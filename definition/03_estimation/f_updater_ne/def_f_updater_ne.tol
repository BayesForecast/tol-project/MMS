
//////////////////////////////////////////////////////////////////////////////
Class @FUpdaterNE : @FUpdater
//////////////////////////////////////////////////////////////////////////////
{
  Static Text _.subclass = "NE"; // no estimativo
  //H: Set _parent_;   // @Forecast
  //H: Set _.settings; // @Settings

  ////////////////////////////////////////////////////////////////////////////
  // Se lanza el mecanismo de actualizaci�n de la previsi�n (no estimativo)
  Real Execute(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Forecast forecast = GetParent(?);
    @Settings settings = GetSettings(?);
    // [1] Se localizan los par�metros indeterminados:
    Set parametersU = forecast::CheckParameters(?);
    // [2] Se clasifican los par�metros por tipo: seg�n si son solventables,
    //     pueden ignorarse o impiden el funcionamiento del actualizador
    Set parametersK = EvalSet(parametersU, Set (@Parameter parameter) {
      Text subclass = parameter::GetSubclass(?);
      Real valid = Case(subclass=="Missing", {
        Real isOutputMissing = And(TextLength(parameter::GetParent(?)
          ::_.GetSubmodelName(?)));
        If(isOutputMissing, 1, -1)
      }, subclass <: [["Hyper", "Sigma2"]], {
        0 // -> se puede ignorar
      }, True, { // "Linear", "NonLinear", "ARIMA"
        -1 // -> no es v�lido
      });    
      [[ parameter, valid ]]
    });
    Set parametersKm1 = Select(parametersK, Real (Set s) { s[2]==(Real -1) });
    Real ignoreCheck = settings::GetSetting("_.ignoreCheck");
    If(Card(parametersKm1)>0 & Not(ignoreCheck), {
      Real MMS.Error("El actualizador encuentra "<<Card(parametersKm1)
        <<" par�metros indeterminados potencialmente incompatibles.", 
        "@FUpdaterNE::Execute");
    0}, {
      Real If(Card(parametersKm1)>0, {
        Real MMS.Warning("El actualizador encuentra "<<Card(parametersKm1)
          <<" par�metros indeterminados potencialmente incompatibles.\n"
          <<"Se continuar� con el proceso de actualizaci�n.", 
          "@FUpdaterNE::Execute")
      });
      Set parametersK1 = Select(parametersK, Real (Set s) { s[2]==1 });
      Set parametersU = EvalSet(parametersK1, @Parameter (Set s) { s[1] });
      If(Card(parametersU), {
        //! Se advierte de que no hay m�s m�todo que OmitFilter implementado
        Text method = settings::GetSetting("_.method");
        Real If(method!="OmitFilter", {
          Real MMS.Warning("M�todo de actualizaci�n '"<<method
            <<"' desconcido. Se usar� OmitFilter.", 
            "@FUpdaterNE::Execute")
        });
        Real _.UpdateOutputParametersMissing(parametersU)
      }, { 
        Real MMS.Info("No se han encontrado par�metros de omitido "
          <<"indeterminados en los outputs.", "@FUpdaterNE::Execute");
      0});
    1})
  };

  ////////////////////////////////////////////////////////////////////////////
  // Actualiza los par�metros de omitido de outputs
  Real _.UpdateOutputParametersMissing(Set parameters)
  ////////////////////////////////////////////////////////////////////////////
  {
    @Forecast forecast = GetParent(?);
    // [0] Se reservan los par�metros existentes:
    Set parameters0 = forecast::GetParameters(?);
    // [1] Se crea una lista auxiliar de par�metros estimados medios:
    Set parametersE = EvalSet(parameters0, Real (Anything p) {
      If(Grammar(p)=="Real", p, {
        WithName(Name(p), p::GetMean(?))
      })
    });
    Real If(Card(parametersE), SetIndexByName(parametersE));
    // [2] Se anulan los par�metros de omitido a actualizar.
    //     Si no exist�an se crean, si exist�an se ponen a cero:
    Set EvalSet(parameters, Real (@ParameterMissing parameter) {
      Real index = FindIndexByName(parametersE, parameter::GetIdentifier(?));
      If(index, {
        Real parametersE[index] := 0;
      0}, {
        Set Append(parametersE, [[
          WithName(parameter::GetIdentifier(?), 0)
        ]]);
      1})
    });
    // [3] Se crean instancias de resultados auxiliares para facilitar el
    //     acceso a resultados:
    //     Para ello se modifican los par�metros del forecast
    //     que luego se restablecer�n junto con los nuevos omitidos.
    Real forecast::SetParameters(parametersE);
    @Model.Results modelR = @Model.Results::FAuxiliar(forecast);
    // [4] Se separan los par�metros por submodelos para tratarlos
    Set bySubmodels = Classify(parameters, 
      Real (@Parameter p1, @Parameter p2) {
      Compare(p1::GetParent(?)::_.GetSubmodelName(?), 
        p2::GetParent(?)::_.GetSubmodelName(?))
    });
    // [5] Se tratan separadamente por submodelos y se encuentran los
    //     nuevos valores para los par�metros de omitido
    //     Se advierte de los par�metros de omitido no f-actualizados.
    Set parametersN = SetConcat(EvalSet(bySubmodels, Set (Set parametersS) {
      Text submodelName = 
        (parametersS[1])::GetParent(?)::_.GetSubmodelName(?);
      @Submodel.Results submodelR = modelR::GetSubmodel(submodelName);
      // (a) Se obtienen los residuos:
      Serie residuals0_ = submodelR::GetResiduals(?);
      // (b) Se define el intervalo de los residuos a utilizar:
      Serie noise0 = submodelR::GetNoise(?);
      Set arima = submodelR::GetARIMA(?);
      Real pdmq = Degree(ARIMAGetARI(arima))-Degree(ARIMAGetMA(arima));
      Serie residuals0 = If(pdmq>0, {
        Date beginR = Succ(First(noise0), Dating(noise0), pdmq);
        SubSer(residuals0_, beginR, Last(residuals0_))
      }, residuals0_);
      // (c) Se crea una serie indicadora de las posiciones de los omitidos:
      Date begin = First(residuals0);
      Date end = Last(residuals0);
      Serie missingsInd = SetSum(EvalSet(parametersS, Serie (@Parameter p) {
        SubSer(Pulse(p::GetPosition(?), Dating(residuals0)), begin, end)
      }));
      // (d) Se corrigen los residuos seg�n OmitFilterTOL:
      Ratio rPi = ARIMAGetARI(arima)/ARIMAGetMA(arima);
      Serie residuals1 = _OmitFilterTOL(rPi, residuals0, missingsInd);
      // (e) Se encuentra el output corregido:
      Ratio rPsi = ARIMAGetMA(arima)/ARIMAGetARI(arima);
      Serie noise1 = DifEq(rPsi, residuals1, noise0);
      Serie filter = submodelR::GetFilter(?);
      Serie output1 = filter + noise1;
      // (f) Se localizan los valores de los par�metros de omitido;
      EvalSet(parametersS, Real (@Parameter p) {
        Text name = p::GetIdentifier(?);
        Real value = SerDat(output1, p::GetPosition(?));
        Real If(IsUnknown(value), {
          Real MMS.Warning("El par�metro '"<<name<<"' no pudo actualizarse.", 
            "@FUpdaterNE::_.UpdateOutputParametersMissing")
        });
        PutName(name, value)
      })
    }));
    // [6] Se destruye el modelR auxiliar y se establecen los par�metros
    //     originales junto con los nuevos omitidos.
    Real modelR::_.Destroy(?);
    NameBlock modelR := NULL(?);
    // Se une el conjunto de nuevos par�metros (parametersN) con el conjunto
    // inicial (parameters0). 
    // En caso de existir duplicidad prevalece el del primer conjunto.
    forecast::SetParameters(Union_Eq(parametersN, parameters0, SameName))
  };

  ////////////////////////////////////////////////////////////////////////////
  // Funciones recuperadas de SADD

  ////////////////////////////////////////////////////////////////////////////
  // Reduce a cero los residuos correspondientes a las fechas de 
  // <impulsos> trasmitiendo el valor del residuo segun la funcion de 
  // respuesta <xpiw>
  Serie _InterRecur(Set impulsos, Serie resi, Polyn xpiw)
  ////////////////////////////////////////////////////////////////////////////
  {
    Date iesima = impulsos[1];
    Real valor  = SerDat(resi, iesima);
    Serie pulEx = xpiw:(valor * Pulse(iesima, Dating(resi)));
    Serie res1  = resi - pulEx; 
    Set imp1    = impulsos - SetOfDate(iesima);
    If(GT(Card(imp1),0), _InterRecur(imp1,res1,xpiw), res1)
  };

  ////////////////////////////////////////////////////////////////////////////
  // Funcion que devuelve la serie de correccion de omitidos en la serie de
  // residuos actualizados
  Serie _OmitFilterTOL(Ration piW, Serie res, Serie ind)
  ////////////////////////////////////////////////////////////////////////////
  {
    Polyn xpiW     = Expand(piW,CountS(ind));
    Set pulsosF    = Dates(SerTms(ind),First(ind),Last(ind));
    Real cardpul   = Card(pulsosF);
    If(cardpul, _InterRecur(pulsosF,res,xpiW), res)
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @FUpdaterNE New(NameBlock args, @Forecast parent)
  ////////////////////////////////////////////////////////////////////////////
  {
    @FUpdaterNE fUpdaterNE = [[
      Set _parent_ = [[ parent ]];
      Set _.settings = [[ getOptArg(args, "_.settings", 
        { @SettingsFUpdaterNE settings }) ]]
    ]];
    fUpdaterNE
  }
};

//////////////////////////////////////////////////////////////////////////////
