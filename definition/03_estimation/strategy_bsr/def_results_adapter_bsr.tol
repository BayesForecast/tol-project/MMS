//////////////////////////////////////////////////////////////////////////////
// FILE:    def_results_adapter_bsr.tol
// PURPOSE: Define la clase para el acceso uniforme a los resultados de
//          una estimaci�n con BSR.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Class @ResultsAdapterBSR : @ResultsAdapter, @ObjectCache
//////////////////////////////////////////////////////////////////////////////
{
  Text _parametersType;
  //H: Set _.nativeResults = Copy(Empty);
  Set _.recoveredResults = Copy(Empty);

  ////////////////////////////////////////////////////////////////////////////
  Anything GetNativeResults(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { _.nativeResults[1] };

  ////////////////////////////////////////////////////////////////////////////
  Anything ResumeParameter(@Parameter parameter, Anything number)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text identifier = parameter::GetIdentifier(?);
    // Se trata el caso de un valor no-muestra en los par�metros:
    Text goc = GrammarOrClass(number);
    Anything numberNew = If(goc=="RandVar::@Real.Sample", {
      Anything number2 = ObtainParameter(parameter);
      Matrix sample1 = If(ObjectExist("Code", "number::GetSample"),
        number::GetSample(?), number::_.GetSample(?));
      Matrix sample2 = If(ObjectExist("Code", "number2::GetSample"),
        number2::GetSample(?), number2::_.GetSample(?));
      Anything Case(_parametersType=="Sample", { 
        Text sampler2 = If(ObjectExist("Code", "number2::GetSampler"),
          number2::GetSampler(?), number2::_.GetSampler(?));
        RandVar::@Real.Sample realSample = 
          RandVar::@Real.Sample::ByColumns(sample1|sample2, sampler2)
      }, _parametersType == "SampleBBM", {
        //! No se sabe ampliar el BBM, o se ampli� en el mismo. Revisar
        number2 
      })
    }, number);
    PutName(identifier, numberNew)
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything ObtainParameter(@Parameter parameter)
  ////////////////////////////////////////////////////////////////////////////
  {
    Case(Card(_.nativeResults), {
      NameBlock bsr = _.nativeResults[1];
      _ObtainParameter(parameter, bsr::cycler::_.name,
        bsr::cycler::_.sampler::_.colNames, False)
    }, Card(_.recoveredResults), {
      _ObtainParameter(parameter, _.recoveredResults::samplerName,
       _.recoveredResults::colNames, True)
    }, True, {
      Real MMS.Error("No hay resultados disponibles.",
        "@ResultsAdapterBSR::ObtainParameter");
      If(False, ?)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Anything _ObtainParameter(@Parameter parameter, Text samplerName,
    Set colNames, Real recover)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text identifier = parameter::GetIdentifier(?);
    // Uso de MEquivalence
    Text bsrName = If(parameter::HasMEquivalence(?), {
      @MEquivalence mEquivalence = parameter::GetMEquivalence(?);
      If(mEquivalence::IsActive(?), {
        mEquivalence::GetIdentifier(?)
      }, {
        MMS.Get.Bsr.Name(parameter)
      })
    }, {
      MMS.Get.Bsr.Name(parameter)
    });
    Real idx = FindIndexByName(colNames, bsrName);    
    If(idx, {
      // Seg�n sea el tipo de par�metros elegido se crea un tipo u otro 
      // de objeto RandVar::@Real.Random
      RandVar::@Real.Random realRandom = Case(recover, {
        //[Recover]
        Matrix sample = SubCol(_.recoveredResults::mcmc[1], [[idx]]); 
        RandVar::@Real.Sample::Sampler(sample, samplerName)
      }, _parametersType=="Sample", { 
        Matrix sample = SubCol(_GetMcmc(?), [[idx]]); 
        RandVar::@Real.Sample::Sampler(sample, samplerName) 
      }, _parametersType=="SampleBBM", {
        RandVar::@Real.SampleBBM::FromBBM(_GetMcmcPath(?), idx, 
          _GetMcmcBurnin(?)+1)
      });     
      PutLocalName(identifier, realRandom)
    }, {
      Case(parameter::IsFixed(?), {
        // Los par�metros Sigma2 fijos no se encuentran.
        PutLocalName(identifier, parameter::GetInitialValue(?))
      }, parameter::GetSubclass(?)=="Sigma2", {
        // A esta situaci�n se llegar�a 
        PutLocalName(identifier, Real 1)
      }, True, {
        Real MMS.Warning("No se ha encontrado el par�metro:\n"<<identifier
          <<"\nusando el nombre: '"<<bsrName<<"'.",
          "@ResultsAdapterBSR::ObtainParameter");
        PutLocalName(identifier, Real ?)
      })
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Text _GetMcmcPath (Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    NameBlock bsr = _.nativeResults[1];
    bsr::cycler::getMcmcPath(?)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _GetMcmcBurnin (Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    NameBlock bsr = _.nativeResults[1];
    bsr::cycler::_.config::mcmc.burnin
  };

  ////////////////////////////////////////////////////////////////////////////
  Matrix _GetMcmc(Real void) { _UseCache(__GetMcmc) };
  ////////////////////////////////////////////////////////////////////////////
  Matrix __GetMcmc(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { 
    NameBlock bsr = _.nativeResults[1];
    bsr::cycler::loadMcmc(bsr::cycler::_.config::mcmc.burnin,
                          bsr::cycler::_.config::mcmc.sampleLength,
                          bsr::cycler::_.config::mcmc.thinning)
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ResultsAdapterBSR Recover(Text parametersType, Text samplerName,
    Set colNames, @Matrix mcmc)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(parametersType<:[["Sample", "SampleBBM"]], {
      @ResultsAdapterBSR resultsAdapterBSR = [[
        Set _.nativeResults = Copy(Empty);
        Set _.recoveredResults = [[
          Text samplerName = samplerName;
          Set colNames = colNames;
          Set mcmc = mcmc
        ]];
        Text _parametersType = parametersType
      ]]
    }, {
      Real MMS.Error("Tipo de par�metros '"<<parametersType<<"' no definido.", 
        "@ResultsAdapterBSR::Recover");
      If(False, ?)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ResultsAdapterBSR Default(BysMcmc::@Estimation bsrResults, 
    Text parametersType)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(parametersType<:[["Sample", "SampleBBM"]], {
      @ResultsAdapterBSR resultsAdapterBSR = [[
        Set _.nativeResults = [[ bsrResults ]];
        Text _parametersType = parametersType
      ]]
    }, {
      Real MMS.Error("Tipo de par�metros '"<<parametersType<<"' no definido.", 
        "@ResultsAdapterBSR::Default");
      If(False, ?)
    })
  }
};
//////////////////////////////////////////////////////////////////////////////
