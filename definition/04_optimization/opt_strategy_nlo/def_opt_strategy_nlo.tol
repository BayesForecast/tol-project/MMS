
//////////////////////////////////////////////////////////////////////////////
Class @OptStrategyNLO : @OptStrategy
//////////////////////////////////////////////////////////////////////////////
{
  Static Text _.subclass = "Opt_NLO";
  //H: Set _parent_;   // @Optimization
  //H: Set _.settings; // @Settings

  Set _.decVariables; // @DecVariable.NLO
  Set _.equations;    // [[ @OptEquation.NLO, Code ]]
  Set _.inequations;  // [[ @OptInequation.NLO, Code ]]
  Set _.pipeLine;

  // Espacio temporal para el almacenamiento de los datos de las variables
  // de decisi�n, en el proceso de optimizaci�n. 
  // Necesita ser un atributo para tener acceso desde el m�todo Execute.
  Set _optData = Copy(Empty);

  //H: Set _.results; // No persistente
  //H: Set _.reports; // No persistente

  ////////////////////////////////////////////////////////////////////////////
  Real _.BuildDecVariables(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set decVariables = MMS.SelectActive((_parent_[1])::GetDecVariables(?));
    Real offset = 1;
    Set _.decVariables := EvalSet(decVariables, 
     @DecVariable.NLO (@DecVariable decVariable) {
      @DecVariable.NLO dvNLO = @DecVariable.NLO::Default(decVariable);
      Real dvNLO::SetOffset(offset);
      Real offset := offset + dvNLO::GetLength(?);
      PutName(dvNLO::GetIdentifier(?), dvNLO)
    });
    Card(_.decVariables)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _.BuildEquations(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set optVariables = MMS.SelectActive((_parent_[1])::GetDecVariables(?)
      <<(_parent_[1])::GetCnsVariables(?));
    Set optEquations = SetConcat(EvalSet(optVariables, 
      Set (@OptVariable optVariable) {
      MMS.SelectActive(optVariable::GetOptEquations(?))
    }));
    Set _.equations := EvalSet(optEquations, Set (@OptEquation equation) {
      @OptEquation.NLO equationNLO = @OptEquation.NLO::Default(equation);
      [[NameBlock equationNLO, Code equationNLO::Check]]
    });
  1};

  ////////////////////////////////////////////////////////////////////////////
  Real _.BuildInequations(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set optVariables = MMS.SelectActive((_parent_[1])::GetDecVariables(?)
      <<(_parent_[1])::GetCnsVariables(?));
    Set optEquations = SetConcat(EvalSet(optVariables, 
      Set (@OptVariable optVariable) {
      MMS.SelectActive(optVariable::GetOptInequations(?))
    }));
    Set _.inequations := EvalSet(optEquations, Set (@OptInequation ineq) {
      @OptInequation.NLO ineqNLO = @OptInequation.NLO::Default(ineq);
      [[NameBlock ineqNLO, Code ineqNLO::Check]]
    });
  1};

  ////////////////////////////////////////////////////////////////////////////
  Real _.BuildPipeline(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    NameBlock If(Card(_.pipeLine), NameBlock _.pipeLine[1] := NULL(?));
    Set _.pipeLine := Copy(Empty);
    Matrix X0 = Group("ConcatRows", EvalSet(_.decVariables, 
      Matrix (@DecVariable.NLO dvNLO) { dvNLO::GetX0(?) }));
    Matrix XL = Group("ConcatRows", EvalSet(_.decVariables, 
      Matrix (@DecVariable.NLO dvNLO) { dvNLO::GetLowerBound(?) }));
    Matrix XU = Group("ConcatRows", EvalSet(_.decVariables, 
      Matrix (@DecVariable.NLO dvNLO) { dvNLO::GetUpperBound(?) }));
/*
    //Matrix g0 = Constant(0,0,?);
    //Real WriteLn("y00 "<< EvalTarget(X0, g0));
*/
    Set _.pipeLine := [[ 
      NonLinGloOpt::@PipeLine pipeLine = [[
        Set problems = [[
          NonLinGloOpt::@Problem problem = [[
            Real n = Rows(X0);
            Matrix lower_bounds = XL;
            Matrix upper_bounds = XU;
            Set equations = _.equations;
            Set inequations = _.inequations;
            Anything target = EvalTarget;
            // Settings
            Real sign = (_.settings[1])::optimal.sign;
            Real neededGlobal = (_.settings[1])::optimal.neededGlobal;
            Real id_analyticalClass = (_.settings[1])::id_analyticalClass;
            Real id_useGradient = (_.settings[1])::id_useGradient;
            Real inequationTolerance = (_.settings[1])::inequationTolerance;
            Real equationTolerance = (_.settings[1])::equationTolerance
          ]]
        ]];
        Matrix x0 = X0;
        // Settings
        Real verboseEach = (_.settings[1])::verboseEach
      ]]
    ]];
    // Settings: algorithm
    Real (_.pipeLine[1])::add_method(NonLinGloOpt::@Method method = [[
      Real id_algorithm = (_.settings[1])::id_algorithm
    ]]);
    Real (_.pipeLine[1])::methods::method::stopCriteria::stopVal :=
      (_.settings[1])::stopCriteria.stopVal;
    Real (_.pipeLine[1])::methods::method::stopCriteria::maxTime :=
      (_.settings[1])::stopCriteria.maxTime;
    Real (_.pipeLine[1])::methods::method::stopCriteria::maxEval :=
      (_.settings[1])::stopCriteria.maxEval;
/*
    Real statusCheck = (_.pipeLine[1])::check(True);
    If(statusCheck, 1, WriteLn( "statusCheck = " << statusCheck));
*/
  1};

  ////////////////////////////////////////////////////////////////////////////
  // M�todos de estrategia
  //  * Real Prepare
  //  * Real SaveIsolationFile
  //  * Real Execute
  //  * @ResultsAdapter GetResults
  //  * Set GetReports
  //  * Real Clear

  ////////////////////////////////////////////////////////////////////////////
  Real Prepare(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real If(Card(_.pipeLine), 0, {
      Real Reporter::Initialize(?);
      Real _.BuildDecVariables(?);
      Real _.BuildEquations(?);
      Real _.BuildInequations(?);
      Real _.BuildOutputVScenarios(?);
      Real _.BuildPipeline(?);
      Real Reporter::Finalize(?);
      Real Reporter::AppendTo(_.reports, "Preparation");
    1})
  };

  ////////////////////////////////////////////////////////////////////////////
  Real Execute(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real Prepare(?);
    // [Execution]
    Set _optData;
    Real Reporter::Initialize(?);
    Real (_.settings[1])::ApplyGlobalSettings(?);
    Real (_.pipeLine[1])::optimize(1);
    Real (_.settings[1])::RestoreGlobalSettings(?);
    Real Reporter::Finalize(?);
    Real ans = If(Reporter::HasErrors(?), -1, {
      // [Results]
      Matrix xIni = (_.pipeLine[1])::x0;
      Real yIni = EvalTarget(xIni, UnknownMatrix);
      Set decIni = DeepCopy(_optData);
      // Se llaman a la funci�n objetivo y a todas las variables de
      // restricci�n o inspecci�n para que dejen cacheados los valores
      // correspondientes al �ptimo.
      Matrix xOpt = (_.pipeLine[1])::x.opt;
      Real yOpt = EvalTarget(xOpt, UnknownMatrix);
      Set cnsVariables = MMS.SelectActive((_parent_[1])::GetCnsVariables(?));
      Set insData = EvalSet(cnsVariables, Set (@CnsVariable cnsVariable) {
        Set [[
          Anything cnsVariable::GetData.Initial(?),
          Anything cnsVariable::GetData.Optimization(?)
        ]]
      });
      Set decOpt = _optData;
      If(Reporter::HasErrors(?), -1, {
        Set _.results := [[
          Matrix xIni;
          Real yIni;
          Set decIni;
          Matrix xOpt;
          Real yOpt;
          Set decOpt;
          Matrix ite = Row(yIni, yOpt) << (xIni|xOpt);
          Set dec = decIni<<decOpt;
          Matrix Group("ConcatColumns", _.iter);
          Set insData
        ]];
      1})
    });
    Real Reporter::AppendTo(_.reports, "Execution");
    1
  };

  Set _.iter = Copy(Empty);
  ////////////////////////////////////////////////////////////////////////////
  Real EvalTarget(Matrix x, Matrix grad)
  ////////////////////////////////////////////////////////////////////////////
  {
    @DataSet dataSet = (_parent_[1])::GetDataSet(?);
    Real dataSet::ClearData_Scenarios([["OPT"]]);
    Set _optData := EvalSet(_.decVariables, Anything (@DecVariable.NLO dvNLO) {
      dvNLO::GetData.Optimization(x)
    });
    Real SetIndexByName(_optData);
    Real (_parent_[1])::Update_Scenarios([["OPT"]]);
    @ObjFunction objFunction = (_parent_[1])::GetObjFunction(?);
    Real y = objFunction::GetData.Optimization(?);
    Set _.iter := _.iter << [[ (Col(y) << x) ]];
    y
  };

/*
  ////////////////////////////////////////////////////////////////////////////
  Serie GetY0Serie(Matrix x)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set _.LocalData := EvalSet(_.decVariables, Serie(@DecVariable.NLO var) {
      PutLocalName(var::GetVarName(?), var::GetData(x))
    });
    ((_parent_[1])::_.objFunction[1])::GetData(1)
  };

  //H: @ResultsAdapter GetResults(Real void);
  //H: Set GetReports(Real void);

  ////////////////////////////////////////////////////////////////////////////
  Real SaveIsolationFile(Text filename)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real Prepare(?);
    1
  };
*/
  
  ////////////////////////////////////////////////////////////////////////////
  Real _.BuildOutputVScenarios(Real void) 
  ////////////////////////////////////////////////////////////////////////////
  {
    // Posible setting: elecci�n del resultado
    //  * Estimation: Observations
    //  * Forecast: Observations.Forecast.Mean
    @MainObject mObj = (_parent_[1])::GetMainObject(?);
    Text mObjSubclass = mObj::GetSubclass(?);
    If(mObjSubclass<:[["Estimation", "Forecast"]], {
      @Model model = mObj::GetModel(?);
      @DataSet dataSet = model::GetDataSet(?);
      Text txtW = If(mObjSubclass=="Estimation", "%E", "%F");
      Text txtR = If(mObjSubclass=="Estimation", "Results", "Forecast"); 
      Real withForecast = If(mObjSubclass=="Forecast",
        (_.settings[1])::_.withForecast, False);
      Text txtV = If(withForecast, {
        Real mObj::InsertScenario("OPT", 1);
        ".WithForecast.Mean(?)"
      }, "_Scenarios([["<<Qt("OPT")<<"]])");
      Set EvalSet(model::GetSubmodels(?), Real (@Submodel submodel) {
        Text submodelName = submodel::GetName(?);
        @Variable outputVar = submodel::GetOutput(?)::GetVariable(?);
        @VariableI variableI = If(outputVar::GetSubclass(?)=="I", outputVar, {
          // Se espera que el output sea una variable independiente.
          // Pero el alg�n caso se crea una variable dependiente de ella
          // de manera trivial
          Set dependences = outputVar::GetDependences(?);
          Real ok = If(Card(dependences)==1, {
            If((dependences[1])::GetSubclass(?)=="I", {
              Text expr = (dependences[1])::GetExpression(?);
              Text exprM = TextTrimRight.(expr, [[" ", "\n", "\t", ";"]]);
              TextMatch(exprM, "*%1")
            }, False)
          }, False);
          If(ok, dependences[1], {
            Real MMS.Warning("No se puede crear el v-escenario del output '"
              <<outputVar::GetName(?)<<"'.", "_.BuildOutputVScenarios");
            If(False, ?)
          })
        });
        Anything variableI::CreateVScenario([[
          Text _.name = "OPT";
          Text _.type = variableI::GetType(?);
          Set _.data. = [[ variableI::GetData(?) ]];
          Real _.dataConcatenation = ?;
          Text _.expression = "Serie "<<txtW<<"::GetModel."<<txtR<<"(?)"
            <<"::GetSubmodel("<<Qt(submodelName)<<")"
            <<"::GetObservations"<<txtV
        ]]);
      1});
    1}, 0)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real Clear(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real MMS.DestroyInstances(_.pipeLine);
    Set EvalSet(_.inequations, Real (Set set) {
      NameBlock set[1] := NULL(?);
    1});
    Set _.inequations := Copy(Empty);
    Set EvalSet(_.equations, Real (Set set) {
      NameBlock set[1] := NULL(?);
    1});
    Set _.equations := Copy(Empty);
    Real MMS.DestroyInstances(_.equations);
    Real MMS.DestroyInstances(_.decVariables);
    Set _.reports := Copy(Empty);
    1
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @OptStrategyNLO New(NameBlock args, @Optimization parent)
  ////////////////////////////////////////////////////////////////////////////
  {
    @OptStrategyNLO optStrategyNLO = [[
      Set _parent_ = [[ parent ]];
      Set _.settings = [[ getOptArg(args, "_.settings",
        { @OptSettingsNLO settings }) ]];
      Set _.decVariables = Copy(Empty);
      Set _.equations = Copy(Empty);
      Set _.inequations = Copy(Empty);
      Set _.pipeLine = Copy(Empty);
      Set _.results = Copy(Empty);
      Set _.reports = Copy(Empty)
    ]];
    optStrategyNLO
  }
};
//////////////////////////////////////////////////////////////////////////////
