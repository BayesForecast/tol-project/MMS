﻿-- MMS 01 VARIABLE
USE `mydb`;

-- Set DataSet. = [[
--   Text MMS.Version        0
--   Text Name               VARCHAR(256)
--   Text Version            VARCHAR(256)
--   Text Description        VARCHAR(256)
--   Date CreationTime       DATETIME
--   Date ModificationTime   DATETIME
--   Set Variables           TABLE
--   Set BaseVariables       TABLE
--   Set Intervals           TABLE
-- ]]

CREATE TABLE mms_d_dataset (
  id_dataset      BIGINT            NOT NULL AUTO_INCREMENT,
  co_name         VARCHAR(256)      NOT NULL,
  co_version      VARCHAR(256)      NOT NULL,
  te_description  TEXT,
  dt_creation     DATETIME NOT NULL,
  dt_modification DATETIME NOT NULL,
  CONSTRAINT pk01_mms_d_dataset PRIMARY KEY (id_dataset),
  CONSTRAINT uk01_mms_d_dataset UNIQUE KEY (co_name, co_version)
);

-- Set Variable. = [[
--   Text Name          VARCHAR(256)
--   Text Scenario      VARCHAR(256)
--   Text Description   VARCHAR(256)
--   Set Tags           TABLE
--   Text Type          VARCHAR(256)
--   Text Expression    VARCHAR(256)
--   Set Dependences    TABLE
--   Anything Data      TABLE
-- ]]

CREATE TABLE mms_d_variable (
  id_variable     BIGINT    NOT NULL AUTO_INCREMENT,
  id_dataset      BIGINT    NOT NULL,
  co_name         VARCHAR(256)      NOT NULL,
  co_scenario     VARCHAR(256)      NOT NULL,
  co_type         VARCHAR(256)      NOT NULL,
  te_description  TEXT,
  te_expression   TEXT      NOT NULL,
  CONSTRAINT pk01_mms_d_variable PRIMARY KEY (id_variable),
  CONSTRAINT uk01_mms_d_variable UNIQUE KEY (id_dataset, co_name, co_scenario),
  CONSTRAINT fk01_mms_d_variable FOREIGN KEY (id_dataset)
    REFERENCES mms_d_dataset (id_dataset)
    ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE mms_f_variable_tag (
  id_variable BIGINT  NOT NULL,
  in_tag      INTEGER NOT NULL,
  te_tag      VARCHAR(256)    NOT NULL,
  CONSTRAINT pk01_mms_f_variable_tag PRIMARY KEY (id_variable, in_tag),
  CONSTRAINT fk01_mms_f_variable_tag FOREIGN KEY (id_variable)
    REFERENCES mms_d_variable (id_variable)
    ON UPDATE CASCADE ON DELETE CASCADE
);

-- Ya que las dependencias no se establecen mediante referencias 
-- sino mediante el identificador, hacemos aqui lo mismo.

CREATE TABLE mms_f_variable_link (
  id_variable  BIGINT  NOT NULL,
  in_link      INTEGER NOT NULL,
  co_link      VARCHAR(256)    NOT NULL,
  CONSTRAINT pk01_mms_f_variable_link PRIMARY KEY (id_variable, in_link),
  CONSTRAINT fk01_mms_f_variable_link FOREIGN KEY (id_variable)
    REFERENCES mms_d_variable (id_variable)
    ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE mms_c_variable_data (
  id_variable    BIGINT    NOT NULL,
  co_grammar     VARCHAR(256)      NOT NULL, -- Serie Matrix...
  co_domain      VARCHAR(256)      NOT NULL, -- Dating ...
  CONSTRAINT pk01_mms_c_variable_data PRIMARY KEY (id_variable),
  CONSTRAINT fk01_mms_c_variable_data FOREIGN KEY (id_variable)
    REFERENCES mms_d_variable (id_variable)
    ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE mms_f_variable_data_serie (
  id_variable BIGINT    NOT NULL,
  dt_date     DATETIME NOT NULL,
  vl_value    DOUBLE,
  CONSTRAINT pk01_mms_c_variable_data_serie PRIMARY KEY (id_variable, dt_date),
  CONSTRAINT fk01_mms_c_variable_data_serie FOREIGN KEY (id_variable)
    REFERENCES mms_c_variable_data (id_variable)
    ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE mms_f_variable_data_matrix (
  id_variable BIGINT  NOT NULL,
  nu_row      INTEGER NOT NULL,
  nu_column   INTEGER NOT NULL,
  vl_value    DOUBLE,
  CONSTRAINT pk01_mms_c_variable_data_matrix PRIMARY KEY (id_variable, nu_row, nu_column),
  CONSTRAINT fk01_mms_c_variable_data_matrix FOREIGN KEY (id_variable)
    REFERENCES mms_c_variable_data (id_variable)
    ON UPDATE CASCADE ON DELETE CASCADE
);

-- Set BaseVariable. = [[
--   Text Name             VARCHAR(256)
--   Text Description      VARCHAR(256)
--   Set Tags              TABLE
--   Text Type             VARCHAR(256)
--   Text Expression       VARCHAR(256)
--   Set Dependences       TABLE
-- ]]

CREATE TABLE mms_d_base_variable (
  id_base_variable BIGINT    NOT NULL AUTO_INCREMENT,
  id_dataset       BIGINT    NOT NULL,
  co_name          VARCHAR(256)      NOT NULL,
  co_type          VARCHAR(256)      NOT NULL,
  te_description   TEXT,
  te_expression    TEXT      NOT NULL,
  CONSTRAINT pk01_mms_d_base_variable PRIMARY KEY (id_base_variable),
  CONSTRAINT uk01_mms_d_base_variable UNIQUE KEY (id_dataset, co_name),
  CONSTRAINT fk01_mms_d_base_variable FOREIGN KEY (id_dataset)
    REFERENCES mms_d_dataset (id_dataset)
    ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE mms_f_base_variable_tag (
  id_base_variable BIGINT  NOT NULL,
  in_tag           INTEGER NOT NULL,
  te_tag           VARCHAR(256)    NOT NULL,
  CONSTRAINT pk01_mms_f_base_variable_tag PRIMARY KEY (id_base_variable, in_tag),
  CONSTRAINT fk01_mms_f_base_variable_tag FOREIGN KEY (id_base_variable)
    REFERENCES mms_d_base_variable (id_base_variable)
    ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE mms_f_base_variable_link (
  id_base_variable  BIGINT  NOT NULL,
  in_link           INTEGER NOT NULL,
  co_link           VARCHAR(256)    NOT NULL,
  CONSTRAINT pk01_mms_f_base_variable_link PRIMARY KEY (id_base_variable, in_link),
  CONSTRAINT fk01_mms_f_base_variable_link FOREIGN KEY (id_base_variable)
    REFERENCES mms_d_base_variable (id_base_variable)
    ON UPDATE CASCADE ON DELETE CASCADE
);

-- Set Interval. = [[
--   Text Domain         VARCHAR(256)
--   Anything First      DATETIME | ...
--   Anything Last       DATETIME | ...
-- ]]

CREATE TABLE mms_d_interval (
  id_interval   BIGINT    NOT NULL AUTO_INCREMENT,
  id_dataset    BIGINT    NOT NULL,
  co_domain     VARCHAR(256)      NOT NULL,
  CONSTRAINT pk01_mms_d_interval PRIMARY KEY (id_interval),
  CONSTRAINT uk01_mms_d_interval UNIQUE KEY (id_dataset, co_domain),
  CONSTRAINT fk01_mms_d_interval FOREIGN KEY (id_dataset)
    REFERENCES mms_d_dataset (id_dataset)
    ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE mms_c_interval_dates (
  id_interval   BIGINT    NOT NULL,
  dt_first      DATETIME NOT NULL,
  dt_last       DATETIME NOT NULL,
  CONSTRAINT pk01_mms_c_interval_dates PRIMARY KEY (id_interval),
  CONSTRAINT fk01_mms_c_interval_dates FOREIGN KEY (id_interval)
    REFERENCES mms_d_interval (id_interval)
    ON UPDATE CASCADE ON DELETE CASCADE
);
