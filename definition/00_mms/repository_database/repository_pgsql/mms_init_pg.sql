--
-- PostgreSQL database dump
--

-- Dumped from database version 8.4.2
-- Dumped by pg_dump version 9.2.2
-- Started on 2013-01-03 17:10:00

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 144 (class 1259 OID 394759)
-- Name: attribute_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE attribute_type (
    id integer NOT NULL,
    name character varying NOT NULL,
    description character varying
);


--
-- TOC entry 143 (class 1259 OID 394757)
-- Name: AttributeType_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE attribute_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1879 (class 0 OID 0)
-- Dependencies: 143
-- Name: attribute_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE attribute_type_id_seq OWNED BY attribute_type.id;


--
-- TOC entry 146 (class 1259 OID 394770)
-- Name: main_object_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE main_object_type (
    id integer NOT NULL,
    name character varying NOT NULL,
    description character varying
);


--
-- TOC entry 145 (class 1259 OID 394768)
-- Name: main_object_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE main_object_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1880 (class 0 OID 0)
-- Dependencies: 145
-- Name: main_object_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE main_object_type_id_seq OWNED BY main_object_type.id;

CREATE TABLE subsection_type (
    id integer NOT NULL,
    name character varying NOT NULL,
    description character varying
);
CREATE SEQUENCE subsection_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE subsection_type_id_seq OWNED BY subsection_type.id;

--
-- TOC entry 150 (class 1259 OID 394799)
-- Name: object_attribute; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE object_attribute (
    id integer NOT NULL,
    id_summary integer NOT NULL,
    id_attribute_type integer NOT NULL,
    name character varying NOT NULL,
    value character varying NOT NULL
);


--
-- TOC entry 149 (class 1259 OID 394797)
-- Name: object_attribute_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE object_attribute_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1881 (class 0 OID 0)
-- Dependencies: 149
-- Name: object_attribute_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE object_attribute_id_seq OWNED BY object_attribute.id;


--
-- TOC entry 152 (class 1259 OID 394820)
-- Name: object_oza; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE object_oza (
    id integer NOT NULL,
    object text NOT NULL,
    id_summary integer
);


--
-- TOC entry 151 (class 1259 OID 394818)
-- Name: object_oza_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE object_oza_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1882 (class 0 OID 0)
-- Dependencies: 151
-- Name: ObjectOza_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE object_oza_id_seq OWNED BY object_oza.id;


--
-- TOC entry 148 (class 1259 OID 394781)
-- Name: ObjectSummary; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE object_summary (
    id integer NOT NULL,
    id_object_type integer NOT NULL,
    name character varying NOT NULL,
    version character varying NOT NULL,
    description character varying,
    tags character varying,
    create_time timestamp without time zone NOT NULL DEFAULT now(),
    modify_time timestamp without time zone NOT NULL DEFAULT now()
);


--
-- TOC entry 147 (class 1259 OID 394779)
-- Name: ObjectSummary_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE object_summary_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1883 (class 0 OID 0)
-- Dependencies: 147
-- Name: ObjectSummary_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE object_summary_id_seq OWNED BY object_summary.id;


--
-- TOC entry 154 (class 1259 OID 394848)
-- Name: SummarySection; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE summary_section (
    id integer NOT NULL,
    id_summary integer NOT NULL,
    id_section_type integer NOT NULL,
    id_attribute_type integer NOT NULL,
    name character varying NOT NULL,
    value character varying NOT NULL
);


--
-- TOC entry 153 (class 1259 OID 394846)
-- Name: SummarySection_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE summary_section_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1884 (class 0 OID 0)
-- Dependencies: 153
-- Name: SummarySection_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE summary_section_id_seq OWNED BY summary_section.id;


--
-- TOC entry 1830 (class 2604 OID 394762)
-- Name: ID; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY attribute_type ALTER COLUMN id SET DEFAULT nextval('attribute_type_id_seq'::regclass);


--
-- TOC entry 1831 (class 2604 OID 394773)
-- Name: ID; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY main_object_type ALTER COLUMN id SET DEFAULT nextval('main_object_type_id_seq'::regclass);

ALTER TABLE ONLY subsection_type ALTER COLUMN id SET DEFAULT nextval('subsection_type_id_seq'::regclass);

--
-- TOC entry 1833 (class 2604 OID 394802)
-- Name: ID; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_attribute ALTER COLUMN id SET DEFAULT nextval('object_attribute_id_seq'::regclass);


--
-- TOC entry 1834 (class 2604 OID 394823)
-- Name: ID; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_oza ALTER COLUMN id SET DEFAULT nextval('object_oza_id_seq'::regclass);


--
-- TOC entry 1832 (class 2604 OID 394784)
-- Name: ID; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_summary ALTER COLUMN id SET DEFAULT nextval('object_summary_id_seq'::regclass);


--
-- TOC entry 1835 (class 2604 OID 394851)
-- Name: ID; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY summary_section ALTER COLUMN id SET DEFAULT nextval('summary_section_id_seq'::regclass);


--
-- TOC entry 1862 (class 0 OID 394759)
-- Dependencies: 144
-- Data for Name: AttributeType; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO attribute_type VALUES (1, 'Real', NULL);
INSERT INTO attribute_type VALUES (2, 'Text', NULL);
INSERT INTO attribute_type VALUES (3, 'Date', NULL);


--
-- TOC entry 1885 (class 0 OID 0)
-- Dependencies: 143
-- Name: AttributeType_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('attribute_type_id_seq', 3, true);


--
-- TOC entry 1864 (class 0 OID 394770)
-- Dependencies: 146
-- Data for Name: main_object_type; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO main_object_type VALUES (1, 'DataSet', NULL);
INSERT INTO main_object_type VALUES (2, 'Model', NULL);
INSERT INTO main_object_type VALUES (3, 'Estimation', NULL);
INSERT INTO main_object_type VALUES (4, 'Forecast', NULL);


--
-- TOC entry 1886 (class 0 OID 0)
-- Dependencies: 145
-- Name: MainObjectType_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('main_object_type_id_seq', 4, true);

INSERT INTO subsection_type VALUES (1, 'dataSet', NULL);
INSERT INTO subsection_type VALUES (2, 'model', NULL);
INSERT INTO subsection_type VALUES (3, 'estimation', NULL);
INSERT INTO subsection_type VALUES (4, 'forecast', NULL);

SELECT pg_catalog.setval('subsection_type_id_seq', 4, true);

--
-- TOC entry 1868 (class 0 OID 394799)
-- Dependencies: 150
-- Data for Name: ObjectAttribute; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 1887 (class 0 OID 0)
-- Dependencies: 149
-- Name: ObjectAttribute_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('object_attribute_id_seq', 1, false);


--
-- TOC entry 1870 (class 0 OID 394820)
-- Dependencies: 152
-- Data for Name: ObjectOza; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 1888 (class 0 OID 0)
-- Dependencies: 151
-- Name: ObjectOza_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('object_oza_id_seq', 1, false);


--
-- TOC entry 1866 (class 0 OID 394781)
-- Dependencies: 148
-- Data for Name: ObjectSummary; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 1889 (class 0 OID 0)
-- Dependencies: 147
-- Name: ObjectSummary_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('object_summary_id_seq', 1, false);


--
-- TOC entry 1872 (class 0 OID 394848)
-- Dependencies: 154
-- Data for Name: SummarySection; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 1890 (class 0 OID 0)
-- Dependencies: 153
-- Name: SummarySection_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('summary_section_id_seq', 1, false);


--
-- TOC entry 1837 (class 2606 OID 394905)
-- Name: AttributeType_Name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY attribute_type
    ADD CONSTRAINT attribute_type_name_key UNIQUE (name);


--
-- TOC entry 1839 (class 2606 OID 394767)
-- Name: AttributeType_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY attribute_type
    ADD CONSTRAINT attribute_type_pkey PRIMARY KEY (id);


--
-- TOC entry 1841 (class 2606 OID 394907)
-- Name: MainObjectType_Name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY main_object_type
    ADD CONSTRAINT main_object_type_name_key UNIQUE (name);

ALTER TABLE ONLY subsection_type
    ADD CONSTRAINT subsection_type_name_key UNIQUE (name);

--
-- TOC entry 1843 (class 2606 OID 394778)
-- Name: MainObjectType_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY main_object_type
    ADD CONSTRAINT main_object_type_pkey PRIMARY KEY (id);

ALTER TABLE ONLY subsection_type
    ADD CONSTRAINT subsection_type_pkey PRIMARY KEY (id);

--
-- TOC entry 1849 (class 2606 OID 394807)
-- Name: object_attribute_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_attribute
    ADD CONSTRAINT object_attribute_pkey PRIMARY KEY (id);


--
-- TOC entry 1851 (class 2606 OID 394828)
-- Name: ObjectOza_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_oza
    ADD CONSTRAINT object_oza_pkey PRIMARY KEY (id);


--
-- TOC entry 1845 (class 2606 OID 394791)
-- Name: ObjectSummary_Name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_summary
    ADD CONSTRAINT object_summary_name_key UNIQUE (id_object_type, name, version);


--
-- TOC entry 1847 (class 2606 OID 394789)
-- Name: ObjectSummary_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_summary
    ADD CONSTRAINT object_summary_pkey PRIMARY KEY (id);


--
-- TOC entry 1853 (class 2606 OID 394856)
-- Name: SummarySection_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY summary_section
    ADD CONSTRAINT summary_section_pkey PRIMARY KEY (id);


--
-- TOC entry 1859 (class 2606 OID 394867)
-- Name: ObjectAttribute_id_AttributeType_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY summary_section
    ADD CONSTRAINT object_attribute_id_attribute_type_fkey FOREIGN KEY (id_attribute_type) REFERENCES attribute_type(id);


--
-- TOC entry 1855 (class 2606 OID 394808)
-- Name: ObjectAttribute_id_summary_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_attribute
    ADD CONSTRAINT object_attribute_id_summary_fkey FOREIGN KEY (id_summary) REFERENCES object_summary(id);


--
-- TOC entry 1856 (class 2606 OID 394813)
-- Name: ObjectAttribute_id_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_attribute
    ADD CONSTRAINT object_attribute_id_type_fkey FOREIGN KEY (id_attribute_type) REFERENCES attribute_type(id);


--
-- TOC entry 1857 (class 2606 OID 394829)
-- Name: ObjectOza_id_Summary_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_oza
    ADD CONSTRAINT object_oza_id_summary_fkey FOREIGN KEY (id_summary) REFERENCES object_summary(id);


--
-- TOC entry 1854 (class 2606 OID 394792)
-- Name: ObjectSummary_id_object_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY object_summary
    ADD CONSTRAINT object_summary_id_object_type_fkey FOREIGN KEY (id_object_type) REFERENCES main_object_type(id);


--
-- TOC entry 1858 (class 2606 OID 394857)
-- Name: SummarySection_id_object_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY summary_section
    ADD CONSTRAINT summary_section_id_object_fkey FOREIGN KEY (id_summary) REFERENCES object_summary(id);


--
-- TOC entry 1860 (class 2606 OID 394872)
-- Name: SummarySection_id_SectionType_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY summary_section
    ADD CONSTRAINT summary_section_id_section_type_fkey FOREIGN KEY (id_section_type) REFERENCES subsection_type(id);


-- Completed on 2013-01-03 17:10:01

--
-- PostgreSQL database dump complete
--

