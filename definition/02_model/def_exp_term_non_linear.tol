
//////////////////////////////////////////////////////////////////////////////
Class @ExpTermNonLinear : @ExpTerm
//////////////////////////////////////////////////////////////////////////////
{
  //H: Set _parent_; //@Submodel
  Static Text _.subclass = "NonLinear";
  //H: Text _.name;
  //H: Text _.description;
  //H: Real _.isActive;
  //H: Real _.isAdditive;
  Set _.nonLinearFilter;
  //H: Set _.input_;
  //H: Set _.parametersLinear;
  Set _.parametersNonLinear;

  #Embed "def_exp_term__uni_input.tol";
  #Embed "def_exp_term__multi_linear.tol";
  #Embed "def_exp_term__regularity.tol"; 
  #Embed "def_exp_term__non_linear.tol";

  ////////////////////////////////////////////////////////////////////////////
  Real SetNonLinearFilter(@NonLinearFilter filter)
  ////////////////////////////////////////////////////////////////////////////
  {
    //!! Hay que revisar el uso de este m�todo para cambiar el filtro
    Real ok = _.SetNonLinearFilter(filter);
    If(ok>0, _.BuildParametersNonLinear(?), ok)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _.SetNonLinearFilter(@NonLinearFilter filter)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real do.set = If(Card(_.nonLinearFilter)==0, True, 
      Not(@NonLinearFilter::Equal(_.nonLinearFilter[1], filter)));
    If(do.set, {
      //!! Hay que revisar el uso de este m�todo para cambiar el filtro
      Set _.nonLinearFilter := [[filter]];
      Real MMS.Trace_OnChange(ClassOf(_this)<<"::_.SetNonLinearFilter");
      Real _OnChange(?)
    }, 0)
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _.BuildParametersNonLinear(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real RemoveParametersNonLinear(?);
    Set parameters = (_.nonLinearFilter[1])::GetParametersDefinition(?);
    Set EvalSet(parameters, @ParameterNonLinear (Set definition) {
      CreateParameterNonLinear([[
        Text _.type = definition->name;
        Real _.degree = definition->degree;
        Real _.initialValue = definition->initialValue;
        NameBlock _.constraint = [[
          Real _.inferiorValue = definition->inferiorValue;
          Real _.superiorValue = definition->superiorValue
        ]]
      ]])
    });
    1
  };

/*
  ////////////////////////////////////////////////////////////////////////////
  Matrix GetInitialNonLinearParameters(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    SetCol(EvalSet(GetParametersNonLinear(?), Real (@Parameter par) {
      par::GetInitialValue(?)
    }))
  };
*/

  //H: Set GetParametersLinear.Degree(Real void)

  ////////////////////////////////////////////////////////////////////////////
  Set GetInitialFilter.Full.(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    // Valores iniciales de los par�metros no lineales
    Set parameters0 = EvalSet(GetParametersNonLinear(?), 
      Real (@ParameterNonLinear parameter) { parameter::GetInitialValue(?) });
    // Conjunto de inputs
    Set inputs = {
      @MVariable input = GetInput(?);
      input::GetData.(100)
    };
    // C�lculo del valor inicial del filtro
    @Anything((_.nonLinearFilter[1])::Filter(parameters0, inputs))
  };

  ////////////////////////////////////////////////////////////////////////////
  Real _OnChangeIdentifier_Childs(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    // Se actualizan los identificadores de los objetos hijos:
    Set EvalSet(_.parametersLinear, Real (@Parameter parameter) {
      parameter::_.UpdateIdentifier(?)
    });
    Set EvalSet(_.parametersNonLinear, Real (@Parameter parameter) {
      parameter::_.UpdateIdentifier(?)
    });
    1
  };

  ////////////////////////////////////////////////////////////////////////////
  Set GetSpecification(Real mode)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set ExpTermNonLinear. = [[
      Text Subclass = _.subclass;
      Text Name = _.name;
      Text Description = _.description;
      Real IsActive = _.isActive;
      Real IsAdditive = _.isAdditive;
      Set NonLinearFilter. = (_.nonLinearFilter[1])::GetSpecification(mode);
      Set Inputs = _GetLinkSet.Spc(_.input_);
      Set ParametersLinear = EvalSet(_.parametersLinear, 
        Set (NameBlock obj) { obj::GetSpecification(mode) });
      Set ParametersNonLinear = EvalSet(_.parametersNonLinear, 
        Set (NameBlock obj) { obj::GetSpecification(mode) })
    ]]
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ExpTermNonLinear Spc(Set expTerm., @Submodel parent)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ExpTermNonLinear expTerm = @ExpTermNonLinear::New([[
      Text _.name = expTerm.::Name;
      Text _.description = expTerm.::Description;
      Real _.isActive = expTerm.::IsActive;
      Real _.isAdditive = If(ObjectExist("Real", "expTerm.::IsAdditive"), 
        expTerm.::IsAdditive, False)
    ]], parent);
    Real expTerm::SetInput(expTerm.::Inputs[1]);
    Set EvalSet(expTerm.::ParametersLinear, 
      @ParameterLinear (Set parameter.) {
      expTerm::CreateParameterLinear_Spc(parameter.)
    });
    Real expTerm::_.SetNonLinearFilter(
      @NonLinearFilter::Spc(expTerm.::NonLinearFilter.));
    Set EvalSet(expTerm.::ParametersNonLinear, 
      @ParameterNonLinear (Set parameter.) {
      expTerm::CreateParameterNonLinear_Spc(parameter.)
    });
    expTerm
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ExpTermNonLinear Advanced(NameBlock args, @Submodel parent)
  ////////////////////////////////////////////////////////////////////////////
  {
    @ExpTermNonLinear expTerm = @ExpTermNonLinear::New(args, parent);
    // Creaci�n de par�metros lineales
//[TF] !! revisar
    Polyn transferF = If(ObjectExist("Polyn", "args::_.transferFunction"),
      args::_.transferFunction, Polyn args::_.coefficient);
    Set expTerm::CreateParametersLinear_Omega(transferF);
    // Asignaci�n del filtro y creaci�n de par�metros no lineales
    Real expTerm::SetNonLinearFilter(args::_.nonLinearFilter);
    // Creaci�n de inputs
    Set inputs = If(ObjectExist("Set", "args::_.inputs"), 
      args::_.inputs, Set [[ args::_.input ]]);
    Real inputSize = args::_.nonLinearFilter::GetInputSize(?);
    Real If(Card(inputs)!=inputSize, {
      Real MMS.Error("El n�mero de inputs no corresponde con el tipo de "
        <<"filtro no lineal utilizado.", "@ExpTermNonLinear::Advanced")
    }, {
      Real expTerm::SetInput_Create(inputs[1]);
    0});
    expTerm
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @ExpTermNonLinear New(NameBlock args, @Submodel parent)
  ////////////////////////////////////////////////////////////////////////////
  {
    Real nerror = Copy(NError);
    @ExpTermNonLinear expTermNonLinear = [[
      Set _parent_ = [[parent]];
      Text _.name = MMS.PrepareName(args::_.name, "@ExpTermNonLinear::New");
      Text _.description = getOptArg(args, "_.description", "");
      Real _.isActive = getOptArg(args, "_.isActive", True);
      Real _.isAdditive = getOptArg(args, "_.isAdditive", False);
      Set _.nonLinearFilter = Copy(Empty);
      Set _.input_ = Copy(Empty);
      Set _.parametersLinear = Copy(Empty);
      Set _.parametersNonLinear = Copy(Empty)
    ]];
    If(NError==nerror, expTermNonLinear)
  }
};
//////////////////////////////////////////////////////////////////////////////
