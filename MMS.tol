
//////////////////////////////////////////////////////////////////////////////
NameBlock MMS = 
//////////////////////////////////////////////////////////////////////////////
[[
  #Require GuiTools;
  #Require BysMcmc;
  #Require NonLinGloOpt;
  #Require QltvRespModel;
  // Privados:
  #Require BabelTool;
  #Require RandVar;
  #Require DecoTools;

  Text _.autodoc.name = "MMS";
  Text _.autodoc.brief = "Model Management System";
  Text _.autodoc.description = "Model Management System";
  Text _.autodoc.url = "https://packages.tol-prj.org"
    <<"/OfficialTolArchiveNetwork/repository.php";
  Set _.autodoc.keys = [["model", "mms"]];
  Set _.autodoc.authors = [["pgea@bayesforecast.com", 
    "jsperez@bayesforecast.com", "decastro@bayesforecast.com",
    "lramirez@bayesforecast.com", "mafernandez@bayesforecast.com", 
    "cfaghloumi@bayesforecast.com"]];
  Text _.autodoc.minTolVersion = "v3.1 p013";
  Real _.autodoc.version.high = 1;
  Real _.autodoc.version.low = 41;
  Set _.autodoc.dependencies = Copy(Empty);
  Text _.autodoc.versionControl = AvoidErr.NonDecAct(OSSvnInfo("."));
  Set _.autodoc.nonTolResources = { [[
    Set CrossPlatform = { [[ 
       Text tcl="gui/tcl",
       Text images="gui/images"
    ]] }
  ]] };

  Text _bbl.es = BabelTool::ExtractBBL_Embed("MMS.tol");
  //Text _bbl.es = BabelTool::ExtractBBL_Path("", "es");
  Text _bbl.en = BabelTool::ExtractBBL_Path("", "en");
  
  #Embed "definition/_dec_definition.tol";
  #Embed "functions/_pck_functions.tol";
  #Embed "definition/_pck_definition.tol";
  #Embed "gui/_pck_gui.tol";

  ////////////////////////////////////////////////////////////////////////////
  Real _isStarted = False;
  Real StartActions(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(_isStarted, 0, {
      Real _isStarted := True;
      Real TolPackage::TolVersion::TestPackageVersion("ExtLib.2.9", "MIN");
      //~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
      // [1] Declaración de las familias de transformaciones
      Real @Transformation.BoxCox::Declare(?);
      // Definción de funciones que pudieran no existir
      Code If(Not(ObjectExist("Code", "IsFiniteDate")), 
        PutName("IsFiniteDate", _IsFiniteDate)); //[OBS]
      //~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
      // [2] Repositorio local por defecto:
      Text path = TolAppDataPath<<"MMS/Repositories/Default";
      // Si no existe se crea:
      Real If(Not(@RepositoryFolder::Exists(path)), {
        @RepositoryFolder::Create([[
          Text _.path = path
        ]])
      });
      // Se crea una conexión al repositorio
      Real If(MMS.FindIndexByInfo(Network::GetConnections(?), "Default")==0, {
        Network::DefineConnection("Default", "folder", path, 
          "Repositorio local por defecto")
      });
      // Se abre la conexión:
      Anything Network::OpenConnection("Default");
      //~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
      // [3] Se carga la GUI (si se está en un entorno Tk)
      Real If(GuiTools::InsideTk(?), {
        // (A) Se declaran los iconos de clases
        Real Icons::Declare(GetAbsolutePath("gui/"));
        // (B) Se declaran los menús contextuales
        Real Menu::Declare(?);
        // (C) Se inicia la GUI específica de MMS
        Text pathTcl = GetAbsolutePath("gui/tcl/");
        Set tclResult = Tcl_EvalEx([[Text "source", pathTcl<<"MMS.tcl"]]);
        If(Not(tclResult["status"]), {
          Error("Fallo cargando MMS.tcl: "<<tclResult["result"])
        });
        // (D) Se añaden botones a la barra de herramientas
        Text TclAddButtonBar([[
          @TclArgSt("-image", "mms_main_container"),
          @TclArgSt("-tooltip", BBr("Abre el contenedor de MMS")),
          @TclArgSt("-tclCmd", "foreach cmd [list "
            <<" {tol::console eval MMS::Container}"
            <<" {after idle ::TolInspector::UpdateConsoleObj}"
            <<"] { eval $cmd }")
        ]]);
        Text TclAddButtonBar([[
          @TclArgSt("-image", "mms_network"),
          @TclArgSt("-tooltip", BBr("Abre la red de trabajo de MMS")),
          @TclArgSt("-tclCmd", "foreach cmd [list "
            <<" {tol::console eval MMS::Network}"
            <<" {after idle ::TolInspector::UpdateConsoleObj}"
            <<"] { eval $cmd }")
        ]]);
        Text TclAddButtonBar([[
          @TclArgSt("-image", "mms_repository_folder"),
          @TclArgSt("-tooltip", BBr("Abre el repositorio por defecto")),
          @TclArgSt("-tclCmd", "foreach cmd [list "
            <<" {tol::console eval MMS::Network::_.repositories::Default}"
            <<" {after idle ::TolInspector::UpdateConsoleObj}"
            <<"] { eval $cmd }")
        ]]);
        Text pathWiz = GetAbsolutePath("wizard/");
        Text pathModel = pathWiz<<"model.prj";

        Text TclAddButtonBar([[
          @TclArgSt("-image", "wizard"),
          @TclArgSt("-tooltip", BBr("Abre la herramienta wizard")),
          @TclArgSt("-tclCmd", "::TolProject::Open "+ Quotes 
          + Replace(pathModel,Quotes,"\\"+Quotes)+Quotes)
        ]]);
      1});
      //~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*
      // [4] Carga de mensajes y sus traducciones
      Real BabelTool::LoadBBL(_bbl.es, True);
      Real BabelTool::LoadBBL(_bbl.en, True);
      Real BabelTool::TranslateGuiTools.Options(?);
      Real BabelTool::PutDescriptions("MMS", 0);
      Real BabelTool::PutDescriptions_Module(MMS::Container, 
        "MMS::@MainContainer", True);
      //Real BabelTool::PutDescriptions(_this, "MMS");
    1})
  }
]];
//////////////////////////////////////////////////////////////////////////////
