
//////////////////////////////////////////////////////////////////////////////
//BBL $OrthogonalizeExpTerms
//BBL %es Ortogonaliza un conjunto de t�rminos explicativos de un modelo.
Real OrthogonalizeExpTerms(@Model model, Text submodelName, 
  Set expTermNames, Text matrixName)
//////////////////////////////////////////////////////////////////////////////
{
  // Se localizan el submodelo y los t�rminos explicativos
  @Submodel submodel = model::GetSubmodel(submodelName);
  Set expTerms = EvalSet(expTermNames, @ExpTerm (Text name) {
    submodel::GetExpTerm(name)
  });
  Real If(Not(_CheckExpTerms(expTerms)), 
    { WriteLn("El chequeo de t�rminos explicativos falla.", "E"); Stop });

  // Se localiza y comprueba la matriz de ortogonalizaci�n
  Real size = Card(expTerms);
  @Variable matrixVar = model::GetDataSet(?)::GetVariable(matrixName);
  Matrix matrix = matrixVar::GetData(?);
  Real If(Rows(matrix)!=size, 
    { WriteLn("N�mero de filas de la matriz err�neo.", "E"); Stop });
  Real If(Columns(matrix)!=size, 
    { WriteLn("N�mero de columnas de la matriz err�neo.", "E"); Stop });

  // Han de localizarse los inputs del bloque a ortogonalizar
  Set variables = EvalSet(expTerms, Anything (@ExpTerm expTerm) {
    expTerm::GetInput(?)::GetVariable(?)
  });
  // Se crean las nuevas variables ortogonalizadas de acuerdo al orden
  // en el que se indicaron los t�rminos explicativos
  Set newVariables = For(1, size, Anything (Real i) {
    @Variable variable = variables[i];
    Text newName = variable::GetName(?)<<"_ORT";
    model::GetDataSet(?)::CreateVariable([[
      Text _.name = newName;
      Text _.type = "Serie";
      Text _.expression = "Serie MMS::Ortho::GetSerie(%A, "<<i<<")";
      Set _.dependences = [[ matrixName ]] << EvalSet(variables, Name)
    ]])
  });

  // Se escogen los nuevos nombres para los t�rminos ortogonalizados:
  Set newNames = EvalSet(expTerms, Text (@ExpTerm expTerm) {
    expTerm::GetName(?)<<"_ORT"
  });

  // Se crean combinaciones equivalentes a los par�metros a ortogonalizar
  Set deoMCombs = SetConcat(For(1, size, Set (Real i) {
    @ExpTerm expTerm = expTerms[i];
    Set EvalSet(expTerm::GetParametersLinear(?), 
      Anything (@Parameter p) {
      @MCombination mc = model::CreateMCombination([[
        Text _.name = Replace(p::GetIdentifier(?), "__", "_._");
        Set _.coefficients = MatSet(matrix)[i];
        Set _.parameters = EvalSet(newNames, Text (Text newName) {
          MMS.NamesToIdentifier([[submodelName, newName, p::GetName(?)]])
        })
      ]]);
      // Se trasladan los priors y las restricciones del par�metro a la comb.
      Real If(p::HasConstraint(?), {
        Real mc::SetConstraint_Spc(p::GetConstraint(?)::GetSpecification(?));
        Real p::RemoveConstraint(?)
      });
      Real If(p::HasPrior(?), {
        Real mc::SetPrior_Spc(p::GetPrior(?)::GetSpecification(?));
        Real p::RemovePrior(?)
      });
      // Se han de reescribir las jerarqu�as y combinaciones de par�metros
      // dependientes de los par�metros ortogonalizados
      Set dependences = p::GetDependences(?);
      // Si hay equivalencias no puede tratarse
      Set deps_equiv = Select(dependences, Real (NameBlock dep) {
        IsInstanceOf(dep, "MMS::@MEquivalence")
      });
      Real If(Card(deps_equiv), 
        { WriteLn("Los par�metros dependen de equivalencias.", "E"); Stop });
      // Se localizan las combinaciones dependientes de los par�metros
      // a alterar y se sustituyen por las combinaciones equivalentes
      Set deps_mcomb = Select(dependences, Real (NameBlock dep) {
        IsInstanceOf(dep, "MMS::@MCombination")
      });
      Set EvalSet(deps_mcomb, Real (@MCombination dep) {
        Real index = dep::FindParameter(p::GetIdentifier(?));
        Real coef = dep::GetCoefficient(index);
        Real dep::RemoveParameter(index);
        Real dep::InsertMCombination(mc::GetIdentifier(?), coef);
        Real dep::ClearParameters(10^(-10));
      1});
      // Se localizan las jerarqu�as dependientes de los par�metros
      // a alterar y se sustituyen por las combinaciones equivalentes
      Set deps_hier = Select(dependences, Real (NameBlock dep) {
        IsInstanceOf(dep, "MMS::@Hierarchy")
      });
      Set EvalSet(deps_hier, Real (@Hierarchy dep) {
        Real dep::ReplaceMElement(p::GetIdentifier(?), mc::GetIdentifier(?));
      1});
      // NO se elimina la combinaci�n auxiliar aunque no sea necesaria
      // pues ser� �til para la valoraci�n de los par�metros originales
      mc
    })
  }));

  // Se renombran adecuadamente los inputs (seg�n newNames)
  Set For(1, Card(expTerms), Real (Real i) {
    @ExpTerm expTerm = expTerms[i];
    @Variable newVariable = newVariables[i];
    @MVariable prevInput = expTerm::GetInput(?);
    Real expTerm::SetInput_Create([[
      Text _.name = newNames[i];
      Text _.variable = newVariable::GetName(?)
    ]]);
    Real If(prevInput::IsActive(?), 0, prevInput::Delete(?));
    Real expTerm::SetName(newNames[i]);
  1});

  // Se enlaza todo el modelo y se renombra todo con los nombres sin "_DEO"
  Real _BuildModelLinks(model);

  // Se eliminan las combinaciones de un par�metro s�lo y que no sean 
  // las informativas de los par�metros originales:
  Set mCombs = Difference_Eq(model::GetMCombinations(?), deoMCombs, 
    SameNameBlock);
  Set EvalSet(mCombs, Real (@MCombination mc) {
    If(mc::GetSize(?)==1, {
      @Parameter p = mc::GetParameter(1);
      // Se trasladan los priors y restricciones de la combin. al par�metro
      Real If(mc::HasConstraint(?), {
        Real p::SetConstraint_Spc(mc::GetConstraint(?)::GetSpecification(?));
        Real mc::RemoveConstraint(?)
      });
      Real If(mc::HasPrior(?), {
        Real p::SetPrior_Spc(mc::GetPrior(?)::GetSpecification(?));
        Real mc::RemovePrior(?)
      });
      mc::Delete(?)
    }, 0)
  });
1};
//////////////////////////////////////////////////////////////////////////////
