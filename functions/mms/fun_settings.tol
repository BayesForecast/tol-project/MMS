
//////////////////////////////////////////////////////////////////////////////
// Funciones que utilizan la configuraci�n de MMS: 
//  * Settings::LocalPath
//  * Settings::OisStore.Mode
//  * Settings::TolName.Mode
//  * Settings::TolName.MaxLength

//////////////////////////////////////////////////////////////////////////////
Text MMS.GetLocalPath(Real void)
//////////////////////////////////////////////////////////////////////////////
{
  //Se podr�a comprobar que la elecci�n es buena..
  If(Settings::LocalPath=="", TolAppDataPath<<"MMS/", Settings::LocalPath)
};

//////////////////////////////////////////////////////////////////////////////
Text MMS.GetSamplesPath(Real void)
//////////////////////////////////////////////////////////////////////////////
{
  //Se podr�a comprobar que la elecci�n es buena..
  If(Settings::SamplesPath=="", MMS.GetLocalPath(?)<<"Samples/", 
    Settings::SamplesPath)
};

//////////////////////////////////////////////////////////////////////////////
// Utilizan el bloque de funciones TolName

//////////////////////////////////////////////////////////////////////////////
Text MMS.PrepareName(Text name, Text context) 
//////////////////////////////////////////////////////////////////////////////
{ 
  // Se evita autom�ticamente la presencia del doble car�cter "__" o de
  // caracteres "_" al principio o al final del nombre.
  Text nameL = TextSub(
    ReplaceTable("_"<<name<<"_", [[ [["__", "_"]] ]], 0), 2, -2);
  // Se evita la presencia del car�cter "@"
  Text nameL := Replace(nameL, "@", ".at.");
  // Se comprueba que:
  //  * la longitud del nombre no es cero
  //  * la longitud del nombre no excede Settings::TolName.MaxLength
  //  * es un nombre tol v�lido de acuerdo al modo elegido en 
  //    la configuraci�n: Settings::TolName.Mode
  Case(TextLength(nameL)==0, {
    Real MMS.Error("El nombre '"<<name<<"' no es v�lido.", context);
    Text ""
  }, TextLength(nameL) > Settings::TolName.MaxLength, {
    Real MMS.Error("El nombre '"<<name<<"' es demasiado largo.", context);
    Text ""
  }, TolName::IsValid_AtMode(nameL, Settings::TolName.Mode), {
    Real If(nameL!=name, {
      Real MMS.Info("El nombre indicado '"<<name<<"' no es v�lido y\n"
        <<"ser� sustituido por '"<<nameL<<"'.", context)
    });
    nameL
  }, True, {
    // Si la limitaci�n es de 7bits intenta transformarse el nombre
    Text nameM = If(TextFind(Settings::TolName.Mode, "7Bits"), {
      Text name7 = TextTo7Bits(nameL);
      If(TolName::IsValid_AtMode(name7, Settings::TolName.Mode), name7, "")
    }, "");
    If(TextLength(nameM), {
      Real MMS.Info("El nombre indicado '"<<name<<"' no es v�lido y\n"
        <<"ser� sustituido por '"<<nameM<<"'.", context);
      nameM
    }, {
      Real MMS.Error("El nombre '"<<name<<"' no es v�lido.", context);
      ""
    })
  })
};

//////////////////////////////////////////////////////////////////////////////
Text MMS.PrepareVersion(Text version, Text context) 
//////////////////////////////////////////////////////////////////////////////
{ 
  // Se evita autom�ticamente la presencia del doble car�cter "__" o de
  // caracteres "_" al principio o al final de la versi�n.
  Text versionL = TextSub(
    ReplaceTable("_"<<version<<"_", [[ [["__", "_"]] ]], 0), 2, -2);
  // Se comprueba que:
  //  * la longitud de la versi�n no es cero
  //  * la longitud de la versi�n no excede Settings::TolName.MaxLength
  //  * son caracteres v�lidos de acuerdo al modo elegido en 
  //    la configuraci�n: Settings::TolName.Mode
  Case(TextLength(versionL)==0, {
    Real MMS.Info("No se admite la versi�n '"<<version<<"'.\n"
      <<"Se sustituir� por '0'.", context);
    Text "0"
  }, TextLength(versionL) > Settings::TolName.MaxLength, {
    Real MMS.Error("La versi�n '"<<version<<"' es demasiado larga.", context);
    Text ""
  }, TolName::IsValid_AtMode("_"<<versionL, Settings::TolName.Mode), {
    Real If(versionL!=version, {
      Real MMS.Info("La versi�n indicada '"<<version<<"' no es v�lida y\n"
        <<"ser� sustituida por '"<<versionL<<"'.", context)
    });
    versionL
  }, True, {
    // Si la limitaci�n es de 7bits intenta transformarse la versi�n
    Text versionM = If(TextFind(Settings::TolName.Mode, "7Bits"), {
      Text version7 = TextTo7Bits(versionL);
      If(TolName::IsValid_AtMode("_"<<version7, Settings::TolName.Mode),
        version7, "")
    }, "");
    If(TextLength(versionM), {
      Real MMS.Info("La versi�n indicada '"<<version<<"' no es v�lida y\n"
        <<"ser� sustituida por '"<<versionM<<"'.", context);
      versionM
    }, {
      Real MMS.Error("La versi�n '"<<version<<"' no es v�lida.", context);
      ""
    })
  })
};

//////////////////////////////////////////////////////////////////////////////
Text MMS.GetOisStore.Mode(Text mode)
//////////////////////////////////////////////////////////////////////////////
{
  // Variantes de mode:
  //  "OIS" se guardar� un directorio ois
  //  "Standard" se guardar� un oza usando mecanismo est�ndar
  //  "ZipArchive" se guardar� un oza usando el mecanismo "ZipArchive"
  //  ""|"Default" se guardar� un oza usando el mecanismo por defecto
  Text modeR = Case(
    ToUpper(mode)=="", "",
    ToUpper(mode)=="DEFAULT", "", 
    ToUpper(mode)=="OIS", "OIS", 
    ToUpper(mode)=="STANDARD", "Standard", 
    ToUpper(mode)=="ZIPARCHIVE", "ZipArachive",  
    True, {
      Real MMS.Warning("Formato '"<<mode<<"' desconocido. "
        <<"Se usar� 'Default'.", "MMS.GetOisStore.Mode");
      Text "" 
    }
  );
  If(TextLength(modeR), modeR, {
    // Se obtiene el mecanismo por defecto
    Text defaultMode = Settings::OisStore.Mode;
    Case(
      ToUpper(defaultMode)=="STANDARD", "Standard", 
      ToUpper(defaultMode)=="ZIPARCHIVE", "ZipArchive",  
      True, {
        Real MMS.Warning("Configuraci�n OisStore.Mode '"<<
          Settings::OisStore.Mode<<"' no v�lida. Se usar� 'Standard'.", 
          "MMS.GetOisStore.Mode");
        Text "Standard" 
      }
    )
  })
};

//////////////////////////////////////////////////////////////////////////////
Real MMS.GetOisStore.Overwrite(Real overwrite)
//////////////////////////////////////////////////////////////////////////////
{
  // Variantes de overwrite
  //  0: no se sobreescribe un archivo existente
  //  1: se sobreescribe un archivo existente
  //  2: se renombra un archivo existente a una versi�n backup
  //  ?: se sobreescribir�n archivos seg�n la configuraci�n por defecto

  Real overwriteR = Case(
    overwrite <: [[0, 1, 2]], overwrite,
    IsUnknown(overwrite), ?,
    True, {
      Real MMS.Warning("Modo de sobreescritura '"<<overwrite
        <<"' desconocido. Se usar� el valor por defecto '?'.", 
        "MMS.GetOisStore.Overwrite");
      Real ?
    }
  );
  If(IsUnknown(overwriteR), {
    // Se obtiene el modo por defecto
    Real defaultOverwrite = Settings::OisStore.Overwrite;
    Case(
      defaultOverwrite <: [[0, 1, 2]], defaultOverwrite,
      True, {
        Real MMS.Warning("Configuraci�n OisStore.Overwrite '"<<
          Settings::OisStore.Overwrite<<"' no v�lida. Se usar� '0'.", 
          "MMS.GetOisStore.Mode");
        Real 0
      }
    )
  }, overwriteR)
};

//////////////////////////////////////////////////////////////////////////////
