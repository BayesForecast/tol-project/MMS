//////////////////////////////////////////////////////////////////////////////
// Tratamiento de opciones
// 
// El conjunto de opciones puede ser:
// + Un Set (Preferido)
// + Un NameBlock
// + Un Real (no hay opciones) == Empty

// * Text _NO_OPTION_
// * Real IsOption(Anything option)
// * Anything GetOption(Anything options, Text name, Anything default)
// * Set FilterOptions(Anything options, Set names)
// * Set GetOptionNames(Anything options)
// * Set ReprefixOptions(Anything options, Text old_prefix, Text new_prefix)
// * Set JoinOptions(Set options1, Set options2)

Text _NO_OPTION_ = "_NO_OPTION_";

//////////////////////////////////////////////////////////////////////////////
Real IsOption(Anything option)
//////////////////////////////////////////////////////////////////////////////
{
  If(Grammar(option)!="Text", True, option!=_NO_OPTION_)
};

//////////////////////////////////////////////////////////////////////////////
Anything GetOption(Anything options, Text name, Anything default)
//////////////////////////////////////////////////////////////////////////////
{
  If(ObjectExist("Anything","options::"<<name),
    Eval("options::"<<name), default)
};

//////////////////////////////////////////////////////////////////////////////
Set FilterOptions(Anything options, Set names)
//////////////////////////////////////////////////////////////////////////////
{
  Text options_type = Grammar(options);
  If(Or(options_type=="Set", options_type=="NameBlock"), {
    Set Select(EvalSet(names, Anything(Text name) {
      GetOption(options, name, _NO_OPTION_)
    }), IsOption)
  }, {
    If(options_type!="Real", WriteLn("[FilterOptions] No se reconoce el tipo "
      <<"'"<<options_type<<"' para represenar unas opciones.", "W"));
    Copy(Empty)
  })
};

//////////////////////////////////////////////////////////////////////////////
Set GetOptionNames(Anything options)
//////////////////////////////////////////////////////////////////////////////
{
  Text options_type = Grammar(options);
  Case(options_type=="Set", {
    EvalSet(options, Text(Anything option) { Name(option) })
  }, options_type=="NameBlock", {
    EvalSet(NameBlockToSet(options), Text(Anything option) { Name(option) })
  }, True, {
    If(options_type!="Real", WriteLn("[FilterOptions] No se reconoce el tipo "
      <<"'"<<options_type<<"' para represenar unas opciones.", "W"));
    Copy(Empty)
  })
};

//////////////////////////////////////////////////////////////////////////////
Set ReprefixOptions(Anything options, Text old_prefix, Text new_prefix)
//////////////////////////////////////////////////////////////////////////////
{
  Text options_type = Grammar(options);
  If(Or(options_type=="Set", options_type=="NameBlock"), {
    Set names = GetOptionNames(options);
    Set Select(EvalSet(names, Anything(Text old_name) {
      Text base_name = Sub(old_name, 1+TextLength(old_prefix), TextLength(old_name));
      Text new_name = new_prefix<<base_name;
      Anything option = GetOption(options, old_name, GetOption(options, new_name, _NO_OPTION_));
      PutName(new_name, option)
    }), IsOption)
  }, {
    If(options_type!="Real", WriteLn("[FilterOptions] No se reconoce el tipo "
      <<"'"<<options_type<<"' para represenar unas opciones.", "W"));
    Copy(Empty)
  })
};

//////////////////////////////////////////////////////////////////////////////
Set JoinOptions(Anything options1, Anything options2)
//////////////////////////////////////////////////////////////////////////////
{
  Text options1_type = Grammar(options1);
  Text options2_type = Grammar(options2);
  Set names = Unique(GetOptionNames(options1)<<GetOptionNames(options2));
  Set Select(EvalSet(names, Anything(Text name) {
    GetOption(options1, name, GetOption(options2, name, _NO_OPTION_))
  }), IsOption)
};


//////////////////////////////////////////////////////////////////////////////
// Ejemplos
/*
Set FilterOptions({Set [[
  Text a = "A";
  Text b = "B"
]]}, [[ "a", "c" ]]);

Anything GetOption({NameBlock [[
  Text a = "A";
  Text b = "B"
]]}, "a", ?);

Set ea = ReprefixOptions(Set ua = {[[
  Text _a = "A";
  Text _b = "B"
]]}, "_", "_.");

Set JoinOptions(ea, ua);
*/
//////////////////////////////////////////////////////////////////////////////


