
//////////////////////////////////////////////////////////////////////////////
Set PutName.(Text name, Set reference)
//////////////////////////////////////////////////////////////////////////////
{
  Anything PutName(name, reference[1]);
  reference
};

//////////////////////////////////////////////////////////////////////////////
Real SameReal(Real a, Real b)
//////////////////////////////////////////////////////////////////////////////
{
  Case(IsUnknown(a) & IsUnknown(b), True,
    IsUnknown(a) | IsUnknown(b), False,
    True, a==b)
};

//////////////////////////////////////////////////////////////////////////////
Real SameDate(Date date1, Date date2)
//////////////////////////////////////////////////////////////////////////////
{
  SameReal(DateToIndex(date1), DateToIndex(date2))
};

//////////////////////////////////////////////////////////////////////////////
Real SamePair(Matrix pair1, Matrix pair2)
//////////////////////////////////////////////////////////////////////////////
{
  If(SameReal(MatDat(pair1, 1, 1), MatDat(pair2, 1, 1)), 
    SameReal(MatDat(pair1, 1, 2), MatDat(pair2, 1, 2)), False)
};

//////////////////////////////////////////////////////////////////////////////
Anything DataAsZeros(Anything data)
//////////////////////////////////////////////////////////////////////////////
{
  Text grammar = Grammar(data);
  Case(grammar=="Serie", {
    EvalSerie(data, Real (Real v) { 0 })
  }, grammar=="Matrix", {
    EvalMat(data, Real (Real v) { 0 })
  })
};

//////////////////////////////////////////////////////////////////////////////
Anything DataSub(Anything data, Anything first, Anything last)
//////////////////////////////////////////////////////////////////////////////
{
  DAtSub(@Anything(data), first, last)[1]
};

//////////////////////////////////////////////////////////////////////////////
Anything DataGetMissings(Anything data, Anything dataInterrupted)
//////////////////////////////////////////////////////////////////////////////
{
  Anything Case(Grammar(data)=="Serie", {
    Serie dataMissings2 = IfSer(IsUnknown(dataInterrupted), data, ?);
    Date last1 = Succ(First(dataInterrupted), Dating(data), -1);
    Date first3 = Succ(Last(dataInterrupted), Dating(data), 1);
    Serie dataMissings1 = SubSer(data, First(data), last1);
    Serie dataMissings3 = SubSer(data, first3, Last(data));
    dataMissings1 << dataMissings2 >> dataMissings3
  }, Grammar(data)=="Matrix", {
    Matrix IfMat(IsUnknown(dataInterrupted), data, ?)
  })
};

//////////////////////////////////////////////////////////////////////////////
Real DataPutValue(Anything data, Anything position, Real value)
//////////////////////////////////////////////////////////////////////////////
{
  Text grammar = Grammar(data);
  Case(grammar=="Serie", {
    Date first = First(data);
    Real checkFirst = If(first==TheBegin, 1, 
      DateToIndex(first)<=DateToIndex(position));
    Date last = Last(data);
    Real checkLast = If(last==TheEnd, 1, 
      DateToIndex(position)<=DateToIndex(last));
    Case(Not(checkFirst), {
      Serie data := SubSer(Pulse(position, Dating(data))*value, position, 
        position) << data;
      ?
    }, Not(checkLast), { 
      Serie data := data >> SubSer(Pulse(position, Dating(data))*value, 
        position, position);
      ?
    }, True, {
      PutSerDat(data, position, value)
    })
  }, grammar=="Matrix", {
    Real row = MatDat(position,1,1);
    Real col = MatDat(position,1,2);
    PutMatDat(data, row, col, value)
  }, True, {
    ?
  })
};

//////////////////////////////////////////////////////////////////////////////
Real DataGetValue(Anything data, Anything position)
//////////////////////////////////////////////////////////////////////////////
{
  Text grammar = Grammar(data);
  Case(grammar=="Serie", {
    SerDat(data, position)
  }, grammar=="Matrix", {
    Real row = MatDat(position,1,1);
    MatDat(data, row, col)
  }, True, {
    ?
  })
};

//////////////////////////////////////////////////////////////////////////////
Anything DataSum(Set dataset)
//////////////////////////////////////////////////////////////////////////////
{
  If(Card(dataset)==0, ?, {
    Text grammar = Grammar(dataset[1]);
    //! SetSum no suma los omitidos de series correctamente!!
    //Eval(grammar<<" SetSum(dataset)")
    Eval(grammar<<" BinGroup(\"+\", dataset)")
  })
};

//////////////////////////////////////////////////////////////////////////////
Real DataLength(Anything data)
//////////////////////////////////////////////////////////////////////////////
{
  Text grammar = Grammar(data);
  Case(grammar=="Serie", {
    CountS(data)
  }, grammar=="Matrix", {
    Rows(data)*Columns(data)
  })
};

//////////////////////////////////////////////////////////////////////////////
Set DataStatistics(Anything data)
//////////////////////////////////////////////////////////////////////////////
{
  Text grammar = Grammar(data);
  Case(grammar=="Serie", {
    Text dating.name = DatingName(data);
    Date begin = First(data);
    Date end = Last(data);
    Text content = dating.name<<"["<<begin<<","<<end<<"]";
    Real card = DateDif(Dating(data), begin, end)+1;
    Real card.unk = SumS(IsUnknown(data));
    Real card.inf = card - SumS(IsFinite(data)) - card.unk;
    Real min = MinS(data);
    Real max = MaxS(data);
    Real sum = SumS(data);
    Real mean = AvrS(data);
    Real sigma = StDsS(data);
    [[ grammar, content, card, card.inf, card.unk, min, max, sum, mean, 
      sigma ]]
  }, grammar=="Matrix", {
    Real rows = Rows(data);
    Real columns = Columns(data);
    Text content = ""<<rows<<"x"<<columns;
    Real card = rows*columns;
    Real card.unk = MatSum(IsUnknown(data));
    Real card.inf = card - MatSum(IsFinite(data)) - card.unk;
    Real min = MatMin(data);
    Real max = MatMax(data);
    Real sum = MatSum(data);
    Real mean = MatAvr(data);
    Real sigma = MatStDs(data);
    [[ grammar, content, card, card.inf, card.unk, min, max, sum, mean, 
      sigma ]]
  }, True, {
    WriteLn("[DataStatistics] Tipo de datos "<<grammar<<" no implementado", 
      "W");
    [[ grammar, "", ?, ?, ?, ?, ?, ?, ?, ? ]]
  })
};

//////////////////////////////////////////////////////////////////////////////
Serie Reduce_PeriodicExtension(Serie serie, Real period)
//////////////////////////////////////////////////////////////////////////////
{
  Date last = Succ(Last(IfSer((1-B^period):serie, 1, ?)), 
    Dating(serie), 1-period);
  SubSer(serie, First(serie), last)
};

//////////////////////////////////////////////////////////////////////////////
Serie Reduce_PeriodicExtension_MinLast(Serie serie, Real period, Date minLast)
//////////////////////////////////////////////////////////////////////////////
{
  Date last = Succ(Last(IfSer((1-B^period):serie, 1, ?)), 
    Dating(serie), 1-period);
  SubSer(serie, First(serie), Max(minLast, last))
};

//////////////////////////////////////////////////////////////////////////////
