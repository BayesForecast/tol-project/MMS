
//////////////////////////////////////////////////////////////////////////////
@Module Joiner = 
//////////////////////////////////////////////////////////////////////////////
[[
  ////////////////////////////////////////////////////////////////////////////
  NameBlock _ObtainObject(Anything object, Text grammar)
  ////////////////////////////////////////////////////////////////////////////
  {
    NameBlock obj = _Obtain(object);
    If(IsInstanceOf(obj, "MMS::@"<<grammar), obj, {
      Real MMS.Error("El objeto indicado no es de tipo '"<<grammar<<"'");
      If(False, ?)
    })
  };
  
  ////////////////////////////////////////////////////////////////////////////
  NameBlock _Obtain(Anything object)
  ////////////////////////////////////////////////////////////////////////////
  {
    // Admite como argumento:
    //  + El objeto MMS (no recomendado, pues se eliminar�)
    //  + La ruta del archivo .oza
    //  + Un nameblock de argumentos indicando el repositorio (_.repository), 
    //    el tipo de objeto (_.grammar) y su identificador (_.identifier)
    Case(Grammar(object)=="NameBlock", {
      If(IsInstanceOf(object, "MMS::@MainObject"), {
        // Se ha indicado el objeto MMS directamente.
        object
      }, {
        // Se admite que de lo contrario es un NameBlock de argumentos
        NameBlock rep = If(Grammar(object::_.repository)=="NameBlock", 
          object::_.repository, Network::GetRepository(object::_.repository));
        rep::LoadObject(object::_.grammar, object::_.identifier)
      })
    }, Grammar(object)=="Text", {
      Container::LoadFile(object)
    }, True, {
      Real MMS.Error("Objeto indicado no v�lido.", "MMS::Joiner::_Obtain");
      If(False, ?)
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  NameBlock JoinForecasts(Set forecasts, NameBlock options)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(Card(forecasts)==0, {
      WriteLn("Indique un par de objetos forecast para fusionar.", "E");
      If(False, ?)
    }, {
      Real firstCall = getOptArg(options, "_.firstCall", True);
      //----------------------------------------------------------------------
      // Se advierte que los objetos indicados como argumentos desaparecer�n
      // en el proceso de fusi�n.
      Real If(firstCall, {
        Real numObjects = SetSum(EvalSet(forecasts, Real (Anything f) {
          If(Grammar(f)=="NameBlock", IsInstanceOf(f, "MMS::@MainObject"), 0)
        }));
        Real If(numObjects>0, {
          Real MMS.Warning("Los objetos indicados en la fusi�n desaparecen "
            <<"en el trascurso del proceso.", "MMS::Joiner::_Obtain");
        0});
      0});
      //----------------------------------------------------------------------
      NameBlock forecast = Case(Card(forecasts)==1, {
        _ObtainObject(forecasts[1], "Forecast")
      }, Card(forecasts)==2, {
        _Join2Forecasts(forecasts[1], forecasts[2])
      }, True, {
        JoinForecasts([[_Join2Forecasts(forecasts[1], forecasts[2])]]
          << Remove(Remove(Copy(forecasts),1),1), [[
          Real _.firstCall = False
        ]])
      });
      //----------------------------------------------------------------------
      // Se renombra el forecast resultante
      Real If(firstCall, {
        Text version = getOptArg(options,"_.version", "Join."<<Card(forecasts));
        Text name = Container::ObtainNewObjectName("Forecast", 
          getOptArg(options, "_.name", "JoinedForecasts"), version);
        Real forecast::SetName(name);
        Real forecast::SetVersion(version);
      1});
      PutName(forecast::GetIdentifier(?), forecast)
    })
  };
  
  ////////////////////////////////////////////////////////////////////////////
  NameBlock _Join2Forecasts(Anything forecast1, Anything forecast2)
  ////////////////////////////////////////////////////////////////////////////
  {
    NameBlock f1 = _ObtainObject(forecast1, "Forecast");
    //------------------------------------------------------------------------
    // Se aprovecha el primer forecast como forecast fusionado
    Real If(f1::GetVersion(?)=="MMS.Joiner", {
    1}, {
      Real f1::SetName(Container::ObtainNewObjectName("Forecast", 
        "JoinedForecasts", "MMS.Joiner"));
      Real f1::SetVersion("MMS.Joiner");
    2});
    //------------------------------------------------------------------------
    // f2 se carga despu�s que f1, por si tiene el mismo identificador que �l
    NameBlock f2 = _ObtainObject(forecast2, "Forecast");
    // Se mantiene la cach� de los forecasts
    WriteLn("[_Join2Forecasts] Obteniendo la cach� de los forecasts");
    Set cache = f1::GetModel.Forecast(?)::_.GetCacheSpecification(?)
      << f2::GetModel.Forecast(?)::_.GetCacheSpecification(?);
    Real f1::Clear(?);
    Real f2::Clear(?);
    //------------------------------------------------------------------------
    // Se fusionan los modelos
    NameBlock m1 = f1::GetModel(?);
    NameBlock m2 = f2::GetModel(?);
    WriteLn("[_Join2Forecasts] Fusionando datasets");
    Real m1::GetDataSet(?)::JoinDataSet_Ignore(m2::GetDataSet(?));
    WriteLn("[_Join2Forecasts] Fusionando m-variables");
    Set EvalSet(m2::GetMVariables(?), Real (NameBlock mVar) {
      NameBlock If(m1::FindMVariable(mVar::GetName(?))==0, 
        m1::CreateMVariable_Spc(mVar::GetSpecification(?)));
    1});
    WriteLn("[_Join2Forecasts] Fusionando submodelos");
    Set EvalSet(m2::GetSubmodels(?), Real (NameBlock sbm) {
      NameBlock m1::CreateSubmodel_Spc(sbm::GetSpecification(?));
    1});
    WriteLn("[_Join2Forecasts] Fusionando jerarqu�as");
    Set EvalSet(m2::GetHierarchies(?), Real (NameBlock hie) {
      NameBlock m1::CreateHierarchy_Spc(hie::GetSpecification(?));
    1});
    WriteLn("[_Join2Forecasts] Fusionando combinaciones de par�metros");
    Set EvalSet(m2::GetMCombinations(?), Real (NameBlock mCom) {
      NameBlock m1::CreateMCombination_Spc(mCom::GetSpecification(?));
    1});
    WriteLn("[_Join2Forecasts] Fusionando combinaciones de par�metros");
    Set EvalSet(m2::GetMEquivalences(?), Real (NameBlock mEqu) {
      NameBlock m1::CreateMEquivalence_Spc(mEqu::GetSpecification(?));
    1});
    //------------------------------------------------------------------------
    // Se fusionan los par�metros
    WriteLn("[_Join2Forecasts] Fusionando par�metros");
    Real f1::SetParameters(f1::GetParameters(?)<<f2::GetParameters(?));
    // Se establece la cach� reservada
    WriteLn("[_Join2Forecasts] Se reestablece la cache");
    Real f1::SetForecast_Spc(cache);
    //------------------------------------------------------------------------
    Real f2::Delete(?);
    f1
  }
]];
//////////////////////////////////////////////////////////////////////////////
