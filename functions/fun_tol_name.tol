
//////////////////////////////////////////////////////////////////////////////
// Caracteres
// + ASCII (0-127) (7bits)
//   > http://en.wikipedia.org/wiki/Ascii
//   + No imprimibles (0-31, 127)
//   + Imprimibles (32-126)
// + ANSI (128-256) (p�gina 1252, semejante a LATIN1: ISO 8859-1)
//   > http://en.wikipedia.org/wiki/Windows-1252
//   > http://en.wikipedia.org/wiki/ISO/IEC_8859-1
//   + No imprimibles (129, 141, 143, 144, 152*, 157)
//     El car�cter 152 es una peque�a tilde que se combina con el car�cter
//     que le sucede: �a (no es �) o �- (sobre un gui�n).
//   + Raros en Tcl/Tk (128-191) (al tabularlos por ejemplo)
//     Se muestra el car�cter de 2Bytes precedido por FF
//   + Correctos en Tcl (192-255)
//   + La interfaz de tol.exe utiliza una p�gina de caracteres (codepage 850)
//     distinta a la que usa internamente provocando errores de codificaci�n.
//     > http://en.wikipedia.org/wiki/Code_page_850
//     Por ejemplo si se indica: "��and�!" interpreta "��and�!"

//[ExtLib] -> Set Characters(Text text) 

//////////////////////////////////////////////////////////////////////////////
Set ASCIISet(Text t)
//////////////////////////////////////////////////////////////////////////////
{
  EvalSet(Characters(t), ASCII)
};

//////////////////////////////////////////////////////////////////////////////
Set CharacterSets = 
//////////////////////////////////////////////////////////////////////////////
{ [[
  Text Digits = "0123456789";
  Text Numericals = "#'.";
  Text Uppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  Text Lowers = "abcdefghijklmnopqrstuvwxyz";
  Text Literals = "_";
  Text Operators = " !\"$%&()*+,-/:;<=>?@[\\]^`{|}~"; // \"
  Text Uppers.Extended = SetSum(EvalSet(Range(192,214,1)<<Range(216,222,1)
    <<[[159,138,140,142]], Char));
  // Text Uppers.Extended = "�����������������������������ޟ���";
  Text Lowers.Extended = SetSum(EvalSet(Range(224,246,1)<<Range(248,255,1)
    <<[[154,156,158]], Char));
  // Text Lowers.Extended = "����������������������������������";
  Text Literals.Extended = SetSum(EvalSet([[131,170,181,186,223]], Char));
  //Text Literals.Extended = "�����";
  Text Operators.1.Extended = SetSum(EvalSet([[128,130]]<<Range(132,137,1)
    <<[[139]]<<Range(145,151,1)<<[[153,155]], Char));
  //Text Operators1.Extended =  "������������������";
  Text Operators.2.Extended = SetSum(EvalSet(Range(160,169,1)
    <<Range(171,180,1)<<Range(182,185,1)<<Range(187,191,1)
    <<[[215,247]], Char));
  //Text Operators2.Extended =  "�������������������������������";
  Text NonPrintable.Extended = SetSum(EvalSet([[129,141,143,144,157]]
    <<[[152]], Char))
]] };

//////////////////////////////////////////////////////////////////////////////
Set CharacterSets.To7Bits = 
//////////////////////////////////////////////////////////////////////////////
{ [[
  Text Uppers.Extended = "AAAAAAACEEEEIIIIDNOOOOOOUUUUYTYSOZ";
  Text Lowers.Extended = "aaaaaaaceeeeiiiidnoooooouuuuytysoz";
  Text Literals.Extended = "fauos";
  Text Operators.1.Extended = "E'\".++^%<''\"\".--*>"; // \";
  Text Operators.2.Extended = " !cLoY|S:C<!-R_0+23'P.,1>///?*/"
]] };

//////////////////////////////////////////////////////////////////////////////
Text TextTo7Bits(Text text)
//////////////////////////////////////////////////////////////////////////////
{
  Set chars = Characters(text); 
  If(Card(chars)==0, "", {
    Real pos = 0;
    SetSum(For(1, Card(chars), Text (Real i) {
    Case(ASCII(chars[i])<128, { chars[i]
    }, Real pos := TextFind(CharacterSets::Uppers.Extended, chars[i]), {
      TextSub(CharacterSets.To7Bits::Uppers.Extended, pos, pos)
    }, Real pos := TextFind(CharacterSets::Lowers.Extended, chars[i]), {
      TextSub(CharacterSets.To7Bits::Lowers.Extended, pos, pos)
    }, Real pos := TextFind(CharacterSets::Literals.Extended, chars[i]), {
      TextSub(CharacterSets.To7Bits::Literals.Extended, pos, pos)
    }, Real pos := TextFind(CharacterSets::Operators.1.Extended, chars[i]), {
      TextSub(CharacterSets.To7Bits::Operators.1.Extended, pos, pos)
    }, Real pos := TextFind(CharacterSets::Operators.2.Extended, chars[i]), {
      TextSub(CharacterSets.To7Bits::Operators.2.Extended, pos, pos)
    }, True, "")
    }))
  })
};

//////////////////////////////////////////////////////////////////////////////
@Module TolName =
//////////////////////////////////////////////////////////////////////////////
[[
  Text DefaultMode = "Compatible7Bits";

  ////////////////////////////////////////////////////////////////////////////
  Set Modes =
  ////////////////////////////////////////////////////////////////////////////
  [[
    // Compatible 7 bits (compatible con BSR)
    NameBlock Compatible7Bits = [[
      Text First = CharacterSets::Uppers << CharacterSets::Lowers 
        << CharacterSets::Literals;
      Text Other = First << CharacterSets::Digits << "."
    ]];
    // Standard 7 bits
    NameBlock Standard7Bits = [[
      Text First = CharacterSets::Uppers << CharacterSets::Lowers 
        << CharacterSets::Literals;
      Text Other = First << CharacterSets::Digits << CharacterSets::Numericals
    ]];
    // Compatible 8 bits (compatible con BSR)
    NameBlock Compatible8Bits = [[
      Text First = CharacterSets::Uppers << CharacterSets::Lowers 
        << CharacterSets::Literals << CharacterSets::Uppers.Extended 
        << CharacterSets::Lowers.Extended << CharacterSets::Literals.Extended;
      Text Other = First << CharacterSets::Digits << "."
    ]];
    // Standard 8 bits
    NameBlock Standard8Bits = [[
      Text First = CharacterSets::Uppers << CharacterSets::Lowers 
        << CharacterSets::Literals << CharacterSets::Uppers.Extended 
        << CharacterSets::Lowers.Extended << CharacterSets::Literals.Extended;
      Text Other = First << CharacterSets::Digits << CharacterSets::Numericals
    ]]
  ]];
  
  Set Modes.Name = EvalSet(Modes, Name);

  ////////////////////////////////////////////////////////////////////////////
  Real IsValid_AtMode(Text text, Text mode)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set chars = Characters(text); 
    If(Card(chars)==0 & mode<:Modes.Name, False, {
      NameBlock modeNB = Modes[mode];
      TextFind(modeNB::First, chars[1]) & If(Card(chars)==1, True, {
        BinGroup("&", For(2, Card(chars), Real (Real i) {
          TextFind(modeNB::Other, chars[i])
        }))  
      })
    })
  };

  ////////////////////////////////////////////////////////////////////////////
  Real IsValid(Text text) { IsValid_AtMode(text, DefaultMode) }
  ////////////////////////////////////////////////////////////////////////////
]];

//////////////////////////////////////////////////////////////////////////////
