#/////////////////////////////////////////////////////////////////////////////
# FILE:    v_scenarios_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDV (module variable definition)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerVScenariosGui {

#///////////////////////////////////////////////////////////////////////////
proc GetInfo {objectid} {
#///////////////////////////////////////////////////////////////////////////
  set objectid_text \"[LayerMMSGui::TolText $objectid]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::VScenariosGui::GetInfo" \
    "Set" $objectid_text
}

#/////////////////////////////////////////////////////////////////////////////
proc GetList {containerid details} {
#/////////////////////////////////////////////////////////////////////////////
  set containerid_text \"$containerid\"
  set details_text \"[LayerMMSGui::TolText $details]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::VScenariosGui::GetList" \
    "Set" $containerid_text $details_text
}

#///////////////////////////////////////////////////////////////////////////
proc Create {object_info} {
#///////////////////////////////////////////////////////////////////////////
  upvar $object_info arrayarg
  set containerid $arrayarg(containerid)
  set containerid_text \"$containerid\"
  LayerMMSGui::EvalTolFunArr "MMS::Layer::VScenariosGui::Create" \
	"Text" arrayarg $containerid_text
}

#///////////////////////////////////////////////////////////////////////////
proc Edit {object_info} {
#///////////////////////////////////////////////////////////////////////////
  upvar $object_info arrayarg
  set containerid $arrayarg(containerid)
  set containerid_text \"$containerid\"
  LayerMMSGui::EvalTolFunArr "MMS::Layer::VScenariosGui::Edit" \
	"Text" arrayarg $containerid_text
}

#///////////////////////////////////////////////////////////////////////////
proc GetType.Possibilities {} {
#///////////////////////////////////////////////////////////////////////////
  set ltypes [LayerMMSGui::EvalTolFunVoid \
    "MMS::Layer::VScenariosGui::GetType.Possibilities" "Set"]
  set _types [list]
  foreach { {} mt } $ltypes {
    lappend _types $mt
  }
  return $_types
}

#/////////////////////////////////////////////////////////////////////////////
proc MmsSyntaxCheck {editor} {
#/////////////////////////////////////////////////////////////////////////////
  # Basado en ::BayesText::TolSyntaxCheck (lib/toltk/bystext.tcl)
  # global toltk_script_path
  # guardamos el archivo en un temporal
  set path [file join $::env(HOME) __check__.tol]
  set texto [::BayesText::GetSelectionOrAll $editor]
  # modificamos la expresi�n de acuerdo al uso de patrones
  set textM [string map {� %} [string map {% _ � _} \
    [string map {%% �} $texto]]]
  set ok [::BayesText::SaveTxt $textM $path]
  if {$ok} {
    # evaluamos el archivo
    set check [::BayesText::TolFileSyntaxCheck $path]
    if {[lindex $check 0]} {
      tk_messageBox -type ok -icon info -title [mc "Syntax Check"] \
        -message [mc "Syntax check done successfully"]
    } else  {
      # hay errores
	  #puts "TolSyntaxCheck (hay errores)"
      ::BayesText::ShowSyntaxErrors $editor [lindex $check 1]
    }
    # borrar el archivo
    catch "file delete $path" error
  } else  {
    tk_messageBox -type ok -icon warning -title [mc Editor] \
                -message [mc "Syntax check cannot be done"]
  }
}

}
