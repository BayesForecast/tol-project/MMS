#/////////////////////////////////////////////////////////////////////////////
# FILE:    dataset_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDV (DataSets)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerDataSetsGui {
 
#/////////////////////////////////////////////////////////////////////////////
proc GetDataSetTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "DataSet 1"
    vers "1.0"
    desc "desc of DataSet 1"
    dCre "2010/10/11"
    dMod "2010/11/28"
    intervals {}
  }
}
  
variable get_dataset_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetDataSet {ident container} {
#///////////////////////////////////////////////////////////////////////////
  variable get_dataset_test
  
  if { $get_dataset_test } {
    return [GetDataSetTest]
  }
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  set container_set [TclLst2TolSet $container]
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::DataSetsGui::GetDataSet" \
    "Set" $ident_text $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetDataSetsListTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "DataSet 1"
	  vers "1.0"
    }
    {}
    {
      name "DataSet 2"
	  vers "2.0"
    }
  }
}
  
variable get_datasets_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetDataSetsList {details} {
#/////////////////////////////////////////////////////////////////////////////
  variable get_datasets_test
  
  if { $get_datasets_test } {
    return [GetDataSetsListTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::DataSetsGui::GetDataSetsList" \
    "Set" $details
}

#///////////////////////////////////////////////////////////////////////////
proc GetDataSetInfo {ident container} {
#///////////////////////////////////////////////////////////////////////////
  
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  set container_set [TclLst2TolSet $container]
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::DataSetsGui::GetDataSetInfo" \
    "Text" $ident_text $container_set
}

#///////////////////////////////////////////////////////////////////////////
proc GetDataSetHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::DataSetsGui::GetDataSetHelp" \
    "Text" "?"
}

#/////////////////////////////////////////////////////////////////////////////
proc GetDataSetSummaryTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      ObjectType "Variable"
      ObjectTotal "4"
      ObjectActive "3"
    }
    {}
    {
      ObjectType "BaseVariable"
      ObjectTotal "12"
      ObjectActive "7"
    }
  }
}

variable summary_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetDataSetSummary {ident container} {
#/////////////////////////////////////////////////////////////////////////////
  variable summary_test

  if { $summary_test } {
    return [GetDataSetSummaryTest]
  }
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  set container_set [TclLst2TolSet $container]
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::DataSetsGui::GetDataSetSummary" \
    "Set" $ident_text $container_set
}

variable create_dataset_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateDataSet {dataset_info intervals attributes} {
#///////////////////////////////////////////////////////////////////////////
  variable create_dataset_test
  upvar $dataset_info arrayarg
  
  set intervals_set [TclLst2TolSet $intervals -level 2]
  #puts "CreateDataSet:intervals_set=$intervals_set"
  
  set attributes_set [TclLst2TolSet $attributes -level 2]
  
  if { $create_dataset_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::DataSetsGui::CreateDataSetTest" \
	  "Real" arrayarg $intervals_set $attributes_set
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::DataSetsGui::CreateDataSet" \
	  "Real" arrayarg $intervals_set $attributes_set
  }
}

variable copy_dataset_test 0
#///////////////////////////////////////////////////////////////////////////
proc CopyDataSet {dataset_info} {
#///////////////////////////////////////////////////////////////////////////
  variable copy_dataset_test
  upvar $dataset_info arrayarg
  
  if { $copy_dataset_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::DataSetsGui::CopyDataSetTest" \
	  "Real" arrayarg
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::DataSetsGui::CopyDataSet" \
	  "Real" arrayarg
  }
}

variable edit_dataset_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditDataSet {dataset_info intervals dsets attributes container} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_dataset_test
  upvar $dataset_info arrayarg
  
  set dsets_set [TclLst2TolSet $dsets]

  set intervals_set [TclLst2TolSet $intervals -level 2]
  #puts "CreateDataSet:intervals_set=$intervals_set"
  
  set attributes_set [TclLst2TolSet $attributes -level 2]
  
  set container_set [TclLst2TolSet $container]
  
  if { $edit_dataset_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::DataSetsGui::EditDataSetTest" \
	  "Real" arrayarg $intervals_set $dsets_set $attributes_set $container_set

  } else {
	LayerMMSGui::EvalTolFunArr "MMS::Layer::DataSetsGui::EditDataSet" \
	  "Real" arrayarg $intervals_set $dsets_set $attributes_set $container_set
  }
}

}
