#/////////////////////////////////////////////////////////////////////////////
# FILE    : dataset_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with MDV layer (DataSets)
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::DataSetsGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateDataSetsListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  
  #puts "CreateDataSetsListDetails"
  set l_details_frame [frame $f.details_DataSets]

  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "DataSet" \
    -swlist "::DataSetsGui::bmmsdsetlist" \
	-fshowitem "::DataSetsGui::_ShowDataSetDetails" \
	-fshowinfo "::DataSetsGui::_ShowDataSetInfo" \
	-fshowlist "::DataSetsGui::_ShowDataSetsListDetails" \
  ]

  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowDataSetsListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreateDataSetsListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details configure -container $container
  $l_details_frame.details Init
  
  return $l_details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowDataSetsListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set container [list "MMS"]

  _ShowDataSetHelp  

  _ShowDataSetsListDetails $container
}

#/////////////////////////////////////////////////////////////////////////////
proc CMenuDataSets {tree} {
#/////////////////////////////////////////////////////////////////////////////

  if {[::MMSGui::IsEditionActive]} {
    set _state "disabled"
  } else {
    set _state "normal"
  }
  $tree.cmenu delete 0 end
  $tree.cmenu add command -label [mc "New DataSet"] \
    -command "::DataSetsGui::NewDataSet" \
	-state $_state
}

#/////////////////////////////////////////////////////////////////////////////
proc NewDataSet {} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details Details "New"
}

#/////////////////////////////////////////////////////////////////////////////
proc ChangeContainerTree {new_ident} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details ChangeActiveItem $new_ident
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowDataSetHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerDataSetsGui::GetDataSetHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateDataSetDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreateDataSetDetails"
  set details_frame [frame $f.details_DataSet]
  set view_frame [frame $f.view_DataSet]

  set _details [bmmsdataset $details_frame.details]
  set _view [bmmsdataset $view_frame.details]

  bind $_details <<Insert>> \
    "event generate $f <<Insert>>"

  bind $_details <<Refresh>> \
    "event generate $f <<Refresh>>"

  bind $_details <<Ok>> \
    "event generate $details_frame <<Ok>>"
  
  bind $_details <<Cancel>> \
    "event generate $details_frame <<Cancel>>"
  
  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowDataSetDetails {container ident state parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateDataSetDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -container $container
  $_frame.details configure -item $ident
  $_frame.details configure -state $state
  $_frame.details configure -parent $parent

  if {$state ne "New"} {
    _ShowDataSetInfo $container $ident  
  }
  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowDataSetInfo {container ident} {
#/////////////////////////////////////////////////////////////////////////////

  array set vinfo [LayerDataSetsGui::GetDataSet $ident $container]
	  
  set objaddr [LayerMMSGui::GetObjectsAddress $vinfo(abs_id)]
  set icon [::ImageManager::getIconForInstance $objaddr]	

  ::MMSGui::ShowObjectInfo $icon "NameBlock" $vinfo(name) \
                           "MMS.@DataSet" "" $vinfo(desc) $objaddr
}

#/////////////////////////////////////////////////////////////////////////////
proc GetDataSetContainer {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set parent_name [$tree item text $parent first]
  if {$parent_name eq [mc "DataSets"]} {
    set id_name [$tree item text $id first]
    set container [list "MMS" $id_name]
  } else {
    set grandparent [$tree item parent $parent]
    set grandparent_name [$tree item text $grandparent first]
    if {$grandparent_name eq [mc "Models"]} {
      set container [list "Model" $parent_name]
    } else {
      set grandgrandparent [$tree item parent $grandparent]
      set grandgrandparent_name [$tree item text $grandgrandparent first]
      if {$grandgrandparent_name eq [mc "Estimations"]} {
        set container [list "Estimation" $grandparent_name]
      } else {
        set container [list "Forecast" $grandparent_name]
      }
    }
  }
  return $container
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowDataSetDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set container [GetDataSetContainer $tree $id]
  set ident [$tree item text $id first]

  _ShowDataSetDetails $container $ident "View" "tree"
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandDataSetsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set _details "no" 

  set _list [LayerDataSetsGui::GetDataSetsList $_details]
  #puts "ExpandDataSetsList:_list=$_list"

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  foreach {{} it} $_list {
    array set vinfo $it
    if {$vinfo(saved)} {
      set icon [::Bitmap::get "mms_dataset"]
	} else {
      set icon [::Bitmap::get "mms_dataset-M"]
	}
    set row [list [list $icon $vinfo(ident)] \
			  [list "DataSet"] \
	          [list "DataSetsGui::ExpandDataSet"] \
			  [list "DataSetsGui::ShowDataSetDetails"] \
			  [list ""] \
			  [list $vinfo(abs_id)] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandDataSet {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set _list {
	"Variables"      "VariablesGui::ExpandList"
                     "VariablesGui::ShowListDetails"
					 "VariablesGui::CMenu"
	"VariablesI"     "VariablesIGui::ExpandList"
                     "VariablesIGui::ShowListDetails"
					 "VariablesIGui::CMenu"
	"VariablesD"     "VariablesDGui::ExpandList"
                     "VariablesDGui::ShowListDetails"
					 "VariablesDGui::CMenu"
  }

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  set icon [::Bitmap::get "Set"]
  foreach {name fexpand fdetails fcmenu} $_list {
    set row [list [list $icon [mc $name]] \
			  [list $name] \
	          [list $fexpand] \
			  [list $fdetails] \
			  [list $fcmenu] \
			  [list ""] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsdsetlist {
# PURPOSE : Defines the snit widget used to
#           list the data sets of MMS
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the container to wich the data sets belongs
  option -container \
    -default "" -configuremethod "_conf-container"  

  option -details \
    -default "no" -configuremethod "_conf-details"  

  variable mms_dsetlist
    # mms_dsetlist(list)  - DataSets list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
    $self FillList
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the data sets of MMS
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
    
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh DataSets List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set ft [frame $f.ft]
    
	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Name"]] \
        [list text -label [mc "Version"]] \
        [list text -label [mc "Creation date"]] \
        [list text -label [mc "Modification date"]] \
        [list text -label [mc "Description"]] \
        [list text -label [mc "Source"]] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns 

	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    focus $tree
    
    set mms_dsetlist(list) ""
  }


  #///////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mms_dsetlist(list) [LayerDataSetsGui::GetDataSetsList \
	  $options(-details)]
    #puts "FillList mms_dsetlist(list)=$mms_dsetlist(list)"

    $tree item delete all

    foreach {{} it} $mms_dsetlist(list) {
      array set vinfo $it
      #puts "FillList vinfo=[array get vinfo]"

      if {$vinfo(saved)} {
        set icon [::Bitmap::get "mms_dataset"]
	  } else {
        set icon [::Bitmap::get "mms_dataset-M"]
	  }
	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(vers)] \
	      [list $vinfo(dCre)] \
	      [list $vinfo(dMod)] \
	      [list $vinfo(desc)] \
	      [list $vinfo(source)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }

      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }


  #///////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }


  #///////////////////////////////////////////////////////////////////////////
  method GetSelectedItems {} {
  #///////////////////////////////////////////////////////////////////////////// 

    set selected {}
    foreach {item} [$tree selection get] {
      set ident [$tree item text $item first]
      lappend selected $ident
    }
    return $selected   
  }

  #///////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
              [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    $tree activate $cur_item
    $tree selection add $cur_item
  }


  #///////////////////////////////////////////////////////////////////////////
  method MakeSelectedItems {items} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    $tree selection clear

    foreach {ident} $items {
      if {$ident != ""} {
        set cur_item 1
        while {$cur_item < $num_item && \
               [$tree item text $cur_item first] != $ident} {
          incr cur_item
        }
        if {$cur_item == $num_item} {
          set cur_item 0
        }
      }
      if {$cur_item != 0} {
        $tree selection add $cur_item
      }
    }
  }


  #///////////////////////////////////////////////////////////////////////////
  method MakeActiveLast {} {
  #/////////////////////////////////////////////////////////////////////////////

    $tree selection clear
    $tree activate last
    $tree selection add last
  }


  #///////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }

}

#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsintervals {
# PURPOSE : Defines the snit widget used to
#           create a list of intervals
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  variable widgets

  # State (disabled, normal)
  option -state \
    -default "disabled" -configuremethod "_conf-state" 

  variable mdv_intervals
  # Intervals to be edited at the tablelist
    
  variable tablelist_options
  
  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor

  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Initialize the variable information
    $self _init
    
    # Paint the window
    $self _create

    # Apply all options passed at creation time.
    $self configurelist $args
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method put_state {s} {
  #///////////////////////////////////////////////////////////////////////////

    $widgets(tbl) configure -state $s
    set lrow [$widgets(tbl) size]
    for {set _r 0} {$_r < $lrow} {incr _r} {
      set btn_minus [$widgets(tbl) windowpath $_r,col_bminus]
      $btn_minus configure -state $s
      set btn_plus [$widgets(tbl) windowpath $_r,col_bplus]
      if {$btn_plus ne ""} {
	    $btn_plus configure -state $s
	  }
    }
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ s } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $s
	$self put_state $s
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #
  # PURPOSE: Initializes the options for the tablelist
  #///////////////////////////////////////////////////////////////////////////
 
    set mdv_intervals [list]

    set tablelist_options(-label) [mc "Intervals"]
    set tablelist_options(-columns) [list \
      0 [mc "Domain"] left \
      0 [mc "Begin"] left \
      0 [mc "End"] left \
      0 "" center \
      0 "" center \
    ]
    set tablelist_options(-listvar) [myvar mdv_intervals]
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          to edit a list of intervals
  #///////////////////////////////////////////////////////////////////////////
   
    set ft $dlg
    # Tablelist frame

    tablelist::tablelist $ft.tbl \
      -columns $tablelist_options(-columns) \
      -listvariable $tablelist_options(-listvar) \
      -xscrollcommand "$ft.xs set" \
      -yscrollcommand "$ft.ys set" \
      -height 4 -width 60
    set widgets(tbl) $ft.tbl

	bind $widgets(tbl) <<TablelistCellUpdated>> \
	  "event generate $win <<OnChanged>>"
	
    scrollbar $ft.ys -orient v -command "$ft.tbl yview"
    scrollbar $ft.xs -orient h -command "$ft.tbl xview"

    grid $ft.tbl  -row 0 -column 0 -sticky nsew
    grid $ft.ys   -row 0 -column 1 -sticky ns
    grid $ft.xs   -row 2 -column 0 -sticky ew

    grid rowconfigure $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
    grid $ft -sticky news
    
    $widgets(tbl) columnconfigure 0 -name col_domain -editable yes
    $widgets(tbl) columnconfigure 1 -name col_begin -editable yes -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 2 -name col_end -editable yes -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 3 -name col_bminus -hide true -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 4 -name col_bplus -labelimage [::Bitmap::get plus] \
	                                -labelcommand "$self AddRow"
    
    foreach {w} {tbl} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method DoNothing {args} {
  #/////////////////////////////////////////////////////////////////////////////
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ConfigureColumns {nrows} {
  #/////////////////////////////////////////////////////////////////////////////

    if {$nrows>0} {
	  $widgets(tbl) columnconfigure col_bminus -hide false
	  $widgets(tbl) columnconfigure col_bplus -labelimage "" \
	    -labelcommand "$self DoNothing"
      $widgets(tbl) cellconfigure end,col_bplus -window [mymethod CreateButtonPlus]
	  if {$nrows>=2} {
        $widgets(tbl) cellconfigure [expr $nrows-2],col_bplus -window ""
	  }

    } else {
	  $widgets(tbl) columnconfigure col_bminus -hide true
	  $widgets(tbl) columnconfigure col_bplus -labelimage [::Bitmap::get plus] \
	    -labelcommand "$self AddRow"
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method DeleteRow {row} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "DeleteRow:row=$row"
    $widgets(tbl) delete [list $row]
    
    set lrow [$widgets(tbl) size]
    for {set _r $row} {$_r < $lrow} {incr _r} {
      #puts "DeleteRow:_r=$_r,windowpath=[$widgets(tbl) windowpath $_r,1]"
      set btn_minus [$widgets(tbl) windowpath $_r,col_bminus]
      $btn_minus configure -command [list $self DeleteRow $_r]
    }
	$self ConfigureColumns $lrow
    
	event generate $win <<OnDeleted>>
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method AddRow {args} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "AddRow:enter"
	$widgets(tbl) finishediting
	update
    $widgets(tbl) insert end [list "" "" ""]

    set lrow [expr [$widgets(tbl) size]-1]
    $widgets(tbl) cellconfigure $lrow,col_bplus -window [mymethod CreateButtonPlus]
    $widgets(tbl) cellconfigure $lrow,col_bminus -window [mymethod CreateButtonMinus]
    $widgets(tbl) editcell $lrow,0

	$self ConfigureColumns [$widgets(tbl) size]
    
	event generate $win <<OnAdded>>
    #puts "AddRow:exit"
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method CreateButtonMinus {tbl row col w} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "createButton:w=$w, row=$row"
    button $w -image [::Bitmap::get minus] -highlightthickness 0 -takefocus 0 \
      -command [list $self DeleteRow $row]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method CreateButtonPlus {tbl row col w} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "createButton:w=$w, row=$row"
    button $w -image [::Bitmap::get plus] -highlightthickness 0 -takefocus 0 \
      -command [list $self AddRow]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnAccept>>
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnCancel>>    
  }

  #/////////////////////////////////////////////////////////////////////////////
  method set_info {iteminfo} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "set_info:iteminfo=$iteminfo"
	set _state $options(-state)
	$self put_state "normal"

    $widgets(tbl) delete 0 end
    set idx_fixed [expr [$widgets(tbl) columncount]-2] 
    set row 0
    foreach {r} $iteminfo {
      $widgets(tbl) insert end [lrange $r 0 $idx_fixed]
      $widgets(tbl) cellconfigure $row,col_bminus -window [mymethod CreateButtonMinus]
      incr row
    }
	$self ConfigureColumns $row
	
	$self put_state $_state
  }

  #/////////////////////////////////////////////////////////////////////////////
  method get_info {what} {
  #/////////////////////////////////////////////////////////////////////////////

    set idx_fixed [expr [$widgets(tbl) columncount]-2] 
    set _intervals [list]
    foreach {r} $mdv_intervals {
      lappend _intervals [lrange $r 0 $idx_fixed]
    }
    #puts "get_info:_intervals=$_intervals"
    return $_intervals
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method finishediting {} {
  #/////////////////////////////////////////////////////////////////////////////

    $widgets(tbl) finishediting
  }
  
}

#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsdataset {
# PURPOSE : Defines the snit widget used to
#           create new data sets or editing existing ones
#/////////////////////////////////////////////////////////////////////////////

  # The container to wich the data set belongs
  option -container \
    -default "" -configuremethod "_conf-container"  
  # If it belongs to MMS - {"MMS" <ident-DataSet>} 
  # If it belongs to a model - {"Model" <ident-Model>} 
  # If it belongs to an estimation - {"Estimation" <ident-Estimation>}
  # If it belongs to a forecast - {"Forecast" <ident-Forecast>}
 
  # Identifier of the specific data set to treat
  option -item \
    -default "" -configuremethod "_conf-item"  

  # Who did call me? (tree (MMS tree), list (DataSets List))
  option -parent \
    -default "tree" -configuremethod "_conf-parent"  

  # Current state (View, Edit, New, Copy)
  option -state \
    -default "Details" -configuremethod "_conf-state"  

  variable label_state

  variable widgets
  
  variable dataset_info
  # DataSet data to be edited
    # (Class @DataSet)
    # dataset_info(name)                - DataSet name (Text _.name)
    # dataset_info(vers)                - Version (Text _.version)
    # dataset_info(dCre)                - (Date _.creationTime)
    # dataset_info(dMod)                - (Date _.modificationTime)
    #  dataset_info(dNow)               - Current system date
	# dataset_info(saved)               - Is saved? (Real _.isSaved)
	# dataset_info(source)              - (Text _.source)
    # dataset_info(intervals)           - List of list of the intervals (Set _.intervals)
	 # (Class @Interval)                - Each list have the following fields
      #                                 - (Text _.domain)
      #                                 - FirstPosition (Set _.begin.)
      #                                 - LastPosition (Set _.end.)
    # dataset_info(desc)                - Description (H: Text _.description)
    # dataset_info(attr)                - Attributes (H: Set _.attributes)
    # dataset_info(tags)                - Tags (H: Set _.tags)
    # dataset_info(st_tags)             - Tags as text
    #                                   - (Set _.variables)
    #                                   - (Set _.baseVariables)
    #                                   - (Set _parent_; // Empty | @Model | @Combination)
    # dataset_info(dsets)               - DataSet Identifiers (Only for Edit at Model)
    
  variable intervals
  variable attributes

  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-item { _ item } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-item) $item
    if {$item eq ""} {
	  return
	}
    $self Details
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent { _ parent } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-parent) $parent 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ state } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $state
    switch -- $state {
      "View"  {$self Details}
      "Edit"  {$self Edit}
      "New"   {$self New}
      "Copy"  {$self Copy}
    }
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates a form to edit a DataSet
  #///////////////////////////////////////////////////////////////////////////
    
    set f $dlg
    
    # Buttons: New, Edit, Copy
    set fbu [frame $f.fbu]
  
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc New] \
      -helptext [mc "New DataSet"] -padx 1 -relief link \
      -compound left -command [list $self New] \
      -state normal
    set widgets(new) $fbu.bNew
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit DataSet"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    Button $fbu.bCopy -image [::Bitmap::get copy] -text [mc "Duplicate"] \
      -helptext "[mc "Duplicate"] [mc "DataSet"]" -padx 1 -relief link \
      -compound left -command [list $self Copy] \
      -state disabled
    set widgets(copy) $fbu.bCopy

    #Button $fbu.bSave -image [ ::Bitmap::get "bsave" ] -text [mc "Save"] \
      -helptext [mc "Save DataSet"] -padx 1 -relief link \
      -compound left -command [list $self Save] \
      -state disabled
    #set widgets(save) $fbu.bSave

    #Button $fbu.bLoad -image [ ::Bitmap::get "bopen" ] -text [mc "Load"] \
      -helptext [mc "Load DataSet"] -padx 1 -relief link \
      -compound left -command [list $self Load] \
      -state normal
    #set widgets(load) $fbu.bLoad
        
    grid $fbu.bNew $fbu.bEdit $fbu.bCopy -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 3 -weight 1

    foreach {w} {new copy} {
      grid remove $widgets($w)
    }

    # Labels and Entries
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]

    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	  
    label $fe.lName -text "[mc "Name"]:" -pady 5 -padx 5
    label $fe.lVers -text "[mc "Version"]:" -pady 5 -padx 5
    label $fe.ldCre -text "[mc "Creation date"]:" -pady 5 -padx 5
    label $fe.ldMod -text "[mc "Modification date"]:" -pady 5 -padx 5
    label $fe.lSour -text "[mc "Source"]:" -pady 5 -padx 5
    label $fe.lInter -text "[mc "Intervals"]:" -pady 5 -padx 5
    label $fe.lDesc -text "[mc "Description"]:" -pady 5 -padx 5
    label $fe.lAttr -text "[mc "Attributes"]:" -pady 5 -padx 5
    label $fe.lDSet -text "[mc "DataSets"]:" -pady 5 -padx 5
    set widgets(ldsets) $fe.lDSet
  
    entry $fe.eName -textvariable [myvar dataset_info(name)] \
      -width 40 -state readonly
    set widgets(name) $fe.eName

    entry $fe.eVers -textvariable [myvar dataset_info(vers)] \
      -width 20 -state readonly
    set widgets(vers) $fe.eVers
    
    ::datefield::datefield $fe.dfCre -textvariable [myvar dataset_info(dCre)] \
      -state normal -format y/m/d
    set dataset_info(dNow) $dataset_info(dCre)
    $fe.dfCre configure -state disabled
    set widgets(dCre) $fe.dfCre
    
    ::datefield::datefield $fe.dfMod -textvariable [myvar dataset_info(dMod)] \
      -state normal -format y/m/d
    set dataset_info(dNow) $dataset_info(dMod)
    $fe.dfMod configure -state disabled
    set widgets(dMod) $fe.dfMod
    
    entry $fe.eSour -textvariable [myvar dataset_info(source)] \
      -width 80 -state readonly
    set widgets(source) $fe.eSour
    
    checkbutton $fe.chkbSaved -variable [myvar dataset_info(saved)] \
      -text [mc "Saved"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(saved) $fe.chkbSaved

	CollapsableFrame $fe.cInter \
	  -text "" -width 460 -height 140
    set widgets(intervals) $fe.cInter
	set ft [$widgets(intervals) getframe]
	::DataSetsGui::bmmsintervals $ft.fInter \
	  -state disabled
    set intervals $ft.fInter
	place $intervals -x 5 -y 15
      
    bind $intervals <<OnCancel>> "$self Cancel"
    #bind $intervals <<OnDeleted>> "$self UpdateIntervals OnDeleted"
    #bind $intervals <<OnAdded>> "$self UpdateIntervals OnAdded"
    #bind $intervals <<OnChanged>> "$self UpdateIntervals OnChanged"
	
    entry $fe.eDesc -textvariable [myvar dataset_info(desc)] \
      -width 60 -state readonly
    set widgets(desc) $fe.eDesc
    
	CollapsableFrame $fe.cAttr \
	  -text "" -width 460 -height 230
    set widgets(attr) $fe.cAttr
	set ft [$widgets(attr) getframe]
	::MMSAttributesGui::bmmsattributes $ft.fAttr \
	  -state disabled
    set attributes $ft.fAttr
	place $attributes -x 5 -y 15
      
    bind $attributes <<OnCancel>> "$self Cancel"
	
    ::MMSSelectorsGui::comboselector $fe.cDSetSel \
      -entry_args "-width 60 -state readonly" \
      -button_args [list -image [::Bitmap::get puntos] \
        -helptext [mc "Select DataSet"] \
        -padx 10 -relief link -compound left \
        -state disabled] \
      -transf_args "" \
	  -type "DataSet" \
	  -swlist "::DataSetsGui::bmmsdsetlist" \
	  -multiple "yes"
    set widgets(dsets) $fe.cDSetSel

	label $fe.lSummary -text [mc "Summary"] -pady 5 -padx 5
    set fs [labelframe $fe.fSummary \
      -labelwidget $fe.lSummary -relief solid -bd 1]
	  
	$self Summary $fs

    grid rowconfigure $fs 0 -weight 1
    grid columnconfigure $fs 0 -weight 1
	
    grid $fe.lName    -row 0 -column 0 -sticky e
    grid $fe.eName    -row 0 -column 1 -sticky w
    grid $fe.lVers    -row 1 -column 0 -sticky e
    grid $fe.eVers    -row 1 -column 1 -sticky w
    grid $fe.ldCre    -row 2 -column 0 -sticky e
    grid $fe.dfCre    -row 2 -column 1 -sticky w
    grid $fe.ldMod    -row 3 -column 0 -sticky e
    grid $fe.dfMod    -row 3 -column 1 -sticky w
    grid $fe.lSour    -row 4 -column 0 -sticky e
    grid $fe.eSour    -row 4 -column 1 -sticky w
    grid $fe.chkbSaved -row 5 -column 1 -sticky w
    grid $fe.lInter   -row 6 -column 0 -sticky ne
    grid $fe.cInter   -row 6 -column 1 -sticky w
    grid $fe.lDesc    -row 7 -column 0 -sticky e
    grid $fe.eDesc    -row 7 -column 1 -sticky w
    grid $fe.lAttr    -row 8 -column 0 -sticky ne
    grid $fe.cAttr    -row 8 -column 1 -sticky w
    grid $fe.lDSet    -row 9 -column 0 -sticky e
    grid $fe.cDSetSel -row 9 -column 1 -sticky w
    grid $fe.fSummary -row 10 -column 0 -sticky news \
                      -columnspan 2 -pady 10 -padx 10

    grid rowconfigure    $fe 11 -weight 1
    grid columnconfigure $fe 2 -weight 1
    
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
    
    foreach {w} {ldsets dsets} {
      grid remove $widgets($w)
    }

    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1
    
    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    foreach {w} {name vers desc} {
      bind $widgets($w) <Return> "$self Ok"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {accept cancel \
	             dsets \
				 intervals attr} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {name vers desc \
                 accept cancel \
				 dsets \
				 intervals attr} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $widgets(name) <Shift-Tab> "focus $widgets(cancel) ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $widgets(accept) ; break"
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetIntervals {} {
  #/////////////////////////////////////////////////////////////////////////////

    set dataset_info(intervals) [$intervals get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetIntervals {} {
  #/////////////////////////////////////////////////////////////////////////////

    $intervals set_info $dataset_info(intervals)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetAttributes {} {
  #/////////////////////////////////////////////////////////////////////////////

    set dataset_info(attr) [$attributes get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetAttributes {} {
  #/////////////////////////////////////////////////////////////////////////////

    $attributes set_info $dataset_info(attr)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetDataSets {} {
  #/////////////////////////////////////////////////////////////////////////////

    set dataset_info(dsets) [$widgets(dsets) get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetDataSets {} {
  #/////////////////////////////////////////////////////////////////////////////

    $widgets(dsets) transient FillList
    $widgets(dsets) set_info $dataset_info(dsets)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set dataset_info(name) ""
    set dataset_info(vers) "1.0"
    set dataset_info(dCre) $dataset_info(dNow)
    set dataset_info(dMod) $dataset_info(dNow)
    set dataset_info(desc) ""
    set dataset_info(source) ""
    set dataset_info(saved) 0

    set dataset_info(intervals) {}
    $self SetIntervals
	
    set dataset_info(attr) {}
    $self SetAttributes
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set ident [$self cget -item]

    array set dataset_info [LayerDataSetsGui::GetDataSet \
      $ident $options(-container)]

    set _intervals [list]
    foreach { {} r } $dataset_info(intervals) {
      set _row [list]
      foreach { {} t } $r {
        lappend _row $t
      }
      lappend _intervals $_row
    }
    set dataset_info(intervals) $_intervals
    #puts "GetInfo:dataset_info(intervals)=$dataset_info(intervals)"
    $self SetIntervals

    set _attributes [list]
    foreach { {} r } $dataset_info(attr) {
      set _row [list]
      foreach { {} t } $r {
        lappend _row $t
      }
      lappend _attributes $_row
    }
    set dataset_info(attr) $_attributes
    $self SetAttributes
	
    set dataset_info(dsets) [list]
    $self SetDataSets
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "View"
    set label_state [mc "Details of the DataSet"]

    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
	if {[lindex $options(-container) 0] eq "MMS"} {
      foreach {w} {new copy} {
        grid $widgets($w)
        $widgets($w) configure -state $_state
      }
	} else {
      foreach {w} {new copy} {
        grid remove $widgets($w)
      }
	}
    $widgets(edit) configure -state $_state
	
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {name vers desc} {
      $widgets($w) configure -state readonly
    }
    $intervals configure -state disabled
    $attributes configure -state disabled
	
    foreach {w} {ldsets dsets} {
      grid remove $widgets($w)
    }
    
	$self GetInfo
	$self FillSummaryList
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Edit"
    set label_state [mc "Edit DataSet"]
    ::MMSGui::ActivateEdition $win

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
	if {[lindex $options(-container) 0] eq "MMS"} {
      foreach {w} {name vers desc} {
        $widgets($w) configure -state normal
      }
      $intervals configure -state normal
      $attributes configure -state normal
	} else {
      foreach {w} {desc} {
        $widgets($w) configure -state disabled
      }
      $intervals configure -state disabled
      $attributes configure -state disabled
      foreach {w} {ldsets dsets} {
        grid $widgets($w)
      }
      $widgets(dsets) button configure -state normal
	}

    focus $widgets(desc)
    bind $widgets(cancel) <Tab> "focus $widgets(desc) ; break"
    bind $widgets(desc) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Create {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name vers desc} {
      $widgets($w) configure -state normal
    }
    if {$options(-state) eq "New"} {
      $intervals configure -state normal
      $attributes configure -state normal
	} else {            ;# Copy
      $intervals configure -state disabled
      $attributes configure -state disabled
	}

    focus $widgets(name)
    bind $widgets(cancel) <Tab> "focus $widgets(name) ; break"
    bind $widgets(desc) <Shift-Tab> "focus $widgets(vers) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method New {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "New"
    set label_state [mc "New DataSet"]
    ::MMSGui::ActivateEdition $win

    $self ClearInfo
	$self FillSummaryList
    
    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Copy {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Copy"
    set label_state "[mc "Duplicate"] [mc "DataSet"]"
    ::MMSGui::ActivateEdition $win

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
    $intervals finishediting
    $self GetIntervals
    $attributes finishediting
    $self GetAttributes
	
    if {$options(-state) eq "Edit"} {
      $self GetDataSets
	  set dataset_info(ident) [$self cget -item]
	  set dataset_info(container) [$self cget -container]
      LayerDataSetsGui::EditDataSet dataset_info \
	    $dataset_info(intervals) $dataset_info(dsets) \
		$dataset_info(attr) $dataset_info(container)

	  if {[lindex $options(-container) 0] eq "MMS"} {
	    set new_ident [LayerMMSGui::NamesToIdentifier \
	      [list $dataset_info(name) $dataset_info(vers)]]
	    if {$dataset_info(ident) ne $new_ident} {
	      if {$options(-parent) eq "tree"} {
            array set new_info [LayerDataSetsGui::GetDataSet \
              $new_ident [list "MMS"]]
	        set new_absid $new_info(abs_id)
            ::MMSGui::ChangeMMSTree $new_ident $new_absid
            #pgea: se a�ade el refresco
            event generate $self <<Refresh>>
	      } else {
            ::DataSetsGui::ChangeContainerTree $new_ident
            event generate $self <<Refresh>>
	      }
	    } elseif {$options(-parent) eq "tree"} {
          ::DataSetsGui::_ShowDataSetInfo $dataset_info(container) $dataset_info(ident)  
	    }
	  }

    } elseif {$options(-state) eq "New"} {
      LayerDataSetsGui::CreateDataSet dataset_info \
	    $dataset_info(intervals) $dataset_info(attr)
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }

    } else {                             ;# Copy
	  set dataset_info(ident) [$self cget -item]
      LayerDataSetsGui::CopyDataSet dataset_info
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }
	}
	  
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Ok>>
	}
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Cancel>>
	}
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Summary {f} {
  #/////////////////////////////////////////////////////////////////////////////
    variable tree
	
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh DataSet Summary"] -padx 1 -relief link -compound left \
      -command [mymethod FillSummaryList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
    
    # Tree
    set ft [frame $f.ft]
    
    set tree [::wtree $ft.tv -table 0 \
      -background white -height 300 -width 0 \
	  -showbuttons no \
      -columns [list \
        [list text -label [mc "Element"]] \
        [list text -label [mc "Total"]] \
        [list text -label [mc "Primitive"]] \
        [list text -label [mc "Trivial"]]
      ]] 

	$tree column configure tail -visible no
	$tree column configure first -expand yes -weight 1

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
 
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    $self FillSummaryList
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillSummaryList {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable tree
  
    $tree item delete all

    if {$dataset_info(name) eq ""} {
	  return
	}
	set summary [LayerDataSetsGui::GetDataSetSummary \
      [$self cget -item] $options(-container)]
    #puts "FillSummaryList summary=$summary"

    foreach {{} t} $summary {
      array set tinfo $t
      set row [list [list [mc $tinfo(ObjectType)]] [list $tinfo(ObjectTotal)] \
	                [list $tinfo(ObjectPrimitive)] [list $tinfo(ObjectTrivial)] \
              ]
      set id [$tree insert $row \
        -at end -relative "root"]

      if {[llength $t] > 6} {
        foreach {{} st} $tinfo(Details) {
          array set sinfo $st
          set row [list [list "[mc $sinfo(ObjectSubtype)]"] [list $sinfo(ObjectTotal)] \
	                    [list $sinfo(ObjectPrimitive)] [list $sinfo(ObjectTrivial)] \
                  ]
          $tree insert $row -at end -relative $id
        }
		$tree item expand $id
	  }
	  
    }
  }
  
}

}
