lappend auto_path [file normalize [file join [file dir [info script]] ".."]]

set ::useTile    1
package require Tk
package require Swaptablelist 1.0

set ::glob_var(local) [list ]

set ::glob_var(masterList) {
  {1  MP1  "one"}
  {2  MP2  "two"}
  {3  MP3  "three"}
  {4  MP4  "four"}
  {5  MP5  "five"}
  {6  MP6  "six"}
  {7  MP7  "seven"}
  {8  MP8  "eight"}
  {9  MP9  "nine"}
  {10 MP10 "ten"}
}

array set ::options {
  -title  "Testing swapTablelist"
  -llabel "Master List"
  -rlabel "Local List"
  -lcolumns {
    {5  "Index"             center }
    {15 "MissionPoint ID"   left   }
    {40 "Description"       left   }
  }
  -rcolumns {
    {5  "Index"             center }
    {15 "MissionPoint ID"   left   }
  }
  -height 20
  -lwidth 0
  -rwidth 0
  -llistvar ::glob_var(masterList)
  -rlistvar ::glob_var(localList)
}

frame .f

set ::swl [eval [list Swaptablelist::create_swap_list .f] [array get ::options]]
pack .f -fill both -expand yes
