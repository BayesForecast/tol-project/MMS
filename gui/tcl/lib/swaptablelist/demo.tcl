 ########################################################################
 #
 # test ...
 #
 ########################################################################

lappend auto_path [file normalize [file join [file dir [info script]] ".."]]

set ::useTile    1

package require Tk
package require BWidget
package require Swaptablelist 1.0

proc main { } {
  interface
  #set_Theme xpnative
}

proc set_Theme { th } {
  if {$::tcl_version <= 8.4} {
    tile::setTheme $th
  } else {
    ttk::style theme use $th
  }
}

proc interface { } {
  
  set w .
  wm withdraw $w
  wm title $w "Test swapTableList"
  wm protocol $w WM_DELETE_WINDOW exit
  
  set descmenu {
    "&File" all file 0 {
      {command "Show Local" {} "Show local list" {}  -command show_local}
      {separator}
      {command "E&xit"      {} "Exit program"    {}  -command exit}
    }
    "&Theme" all theme 0 {
      {radiobutton "Default"   {} "default"     {}  -command {set_Theme default}}
      {radiobutton "Classic"   {} "classic"     {}  -command {set_Theme classic}}
      {radiobutton "Clam"      {} "clam"        {}  -command {set_Theme clam}}
      {radiobutton "Step"      {} "step"        {}  -command {set_Theme step}}
      {radiobutton "alt"       {} "alt"         {}  -command {set_Theme alt}}
      {radiobutton "winnative" {} "Win native"  {}  -command {set_Theme winnative}}
      {radiobutton "xpnative"  {} "XP native"   {}  -command {set_Theme xpnative}}
    }
  }
  
  set mainframe [MainFrame .mainframe \
                     -menu         $descmenu \
                     -textvariable ::glob_var(status) \
                     -progressvar  ::glob_var(progressvar) \
                     -progressmax  100 \
                     -progresstype normal \
                     -progressfg blue ]

  $mainframe showstatusbar status
  $mainframe addindicator -text "version 0.0"
  
  set mf [$mainframe getframe]
  
  build_swtl $mf
  
  pack $mainframe -fill both -expand yes
  
  update idletasks
  
  set x [expr {[winfo screenwidth $w]/2  - [winfo reqwidth $w]/2  - [winfo vrootx $w]}]
  set y [expr {[winfo screenheight $w]/2 - [winfo reqheight $w]/2 - [winfo vrooty $w]}]
  wm geometry $w +$x+$y
  
  raise $w
  wm deiconify $w
}

proc build_swtl { fr } {
  
  set ::glob_var(list,displayed) 0
  
  set ::glob_var(masterList) [list ]
  set ::glob_var(local)      [list ]
  
  set ::glob_var(masterList) {
    {1  MP1  "one"}
    {2  MP2  "two"}
    {3  MP3  "three"}
    {4  MP4  "four"}
    {5  MP5  "five"}
    {6  MP6  "six"}
             {7  MP7  "seven"}
    {8  MP8  "eight"}
    {9  MP9  "nine"}
    {10 MP10 "ten"}
  }
  
  array set ::options {
    -title  "Testing swapTablelist"
    -llabel "Master List"
    -rlabel "Local List"
    -lcolumns {
      {5  "Index"             center }
      {15 "MissionPoint ID"   left   }
      {40 "Description"       left   }
    }
    -rcolumns {
      {5  "Index"             center }
      {15 "MissionPoint ID"   left   }
    }
    -height 20
    -lwidth 0
    -rwidth 0
    -llistvar ::glob_var(masterList)
    -rlistvar ::glob_var(localList)
  }
  
  set f2 [frame $fr.f2]
  set ::swl [eval [list Swaptablelist::create_swap_list $f2] [array get ::options]]
  pack $f2
  
  set tbl [Swaptablelist::getLTable $::swl]
  $tbl columnconfigure 0 -name col_index  -editable no -sortmode integer
  $tbl columnconfigure 1 -name col_mpID   -editable no -sortmode dictionary
  $tbl columnconfigure 2 -name col_desc   -editable no -sortmode dictionary
  
  set tbl [Swaptablelist::getRTable $::swl]
  $tbl columnconfigure 0 -name col_index  -editable no -sortmode integer
  $tbl columnconfigure 1 -name col_mpID   -editable no -sortmode dictionary

}

proc show_local { } {
  puts "Local list : "
  puts $::glob_var(localList)
}

proc fill { } {
  
  # Old code : Not used in the demo any more, but I left
  # it in "just in case"...
  
  if {$::glob_var(list,displayed)} {
    return
  }
  set tbl [Swaptablelist::getLTable $::swl]
  
  set mpList {
    {1  MP1  "one"}
    {2  MP2  "two"}
    {3  MP3  "three"}
    {4  MP4  "four"}
    {5  MP5  "five"}
    {6  MP6  "six"}
    {7  MP7  "seven"}
    {8  MP8  "eight"}
    {9  MP9  "nine"}
    {10 MP10 "ten"}
  }
  
  Swaptablelist::insert $tbl $mpList
  
  set ::glob_var(list,displayed) 1
}

main
