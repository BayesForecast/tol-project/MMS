package require Tk
package require snit

if { ( $tcl_platform(platform) ne "windows" ) ||
   [ catch "package require twapi" ] } {
  puts "default get_desktop_workarea"
  proc _get_desktop_workarea { } {
     list 0 0 [ winfo screenwidth . ] [ winfo screenheight . ]
  } 
} else {
  proc _get_desktop_workarea { } {
    twapi::get_desktop_workarea
  }
}

snit::widgetadaptor wtransient {

  variable prev_focus ""

  option -cuser -readonly yes

  component cuser

  delegate method * to cuser
  delegate option * to cuser
  
  constructor { args } {
    installhull using toplevel
    set master [ winfo parent $win ]
    wm overrideredirect $win 1;
    wm transient $win $master;
    wm group $win $master;
    wm state $win withdraw
    $self configurelist $args
    eval \
        install cuser using [ lindex $options(-cuser) 0 ] $win.cuser \
        [ lrange $options(-cuser) 1 end ]
    grid $win.cuser -row 0 -column 0
    grid rowconfigure $win 0 -weight 1
    grid columnconfigure $win 0 -weight 1
    bind $win <FocusOut> [ mymethod _focusout %W ]
  }

  method _focusout { w } {
    if { $w eq $win } {
      $win unpost
    }
  }

  method post { {xpos -1} {ypos -1} } {
    if { $xpos != -1 && $ypos != -1 } {
      wm geometry $win +${xpos}+${ypos}
      update idletasks
    }
    wm state $win normal 
    update idletask
    wm deiconify $win
    wm state $win normal
    raise $win
    set prev_focus [ focus ]
    after idle "focus $win;"
  }

  method unpost { } {
    focus $prev_focus
    update idletasks
    wm state $win withdraw
  }

  method geometry { w h } {
    wm geometry $win ${w}x${h}
  }
};

snit::widget wcombo {

  option -centry \
      -default "entry" -readonly yes

  # la imagen mac-expand tiene que estar definida, hay que definir un
  # wbutton que sea un widgetadaptor de button.
  option -cbutton \
      -default "cbutton -image mac-expand" -readonly yes

  option -cuser -readonly yes

  # componente que muestra el resumen de la informacion
  component centry -public entry

  # componente para el boton que despliega la ventana de selector
  component cbutton -public button
  
  # componente para la ventana del selector
  component ctransient -public transient

  delegate method get_info to ctransient

  constructor { args } {
    $self configurelist $args
    eval \
        install centry using \
        [ lindex $options(-centry) 0 ] $win.centry \
        [ lrange $options(-centry) 1 end ]
    eval \
        install cbutton using \
        [ lindex $options(-cbutton) 0 ] $win.cbutton \
        [ lrange $options(-cbutton) 1 end ] -takefocus 1
    eval \
        install ctransient using \
        wtransient $win.ctrans -cuser $options(-cuser)
    $self _bind_components
    grid columnconfigure $win 0 -weight 1
    grid $centry -row 0 -column 0 -sticky ew
    grid $cbutton -row 0 -column 1
  }

  method _bind_components { } {
    bind $cbutton <<OnClick>> [ mymethod OnButtonClick ]
    bind $ctransient <<OnAccept>> [ mymethod OnAccept ]
    bind $ctransient <<OnCancel>> [ mymethod OnCancel ]
  }

  method OnButtonClick { } {
    # si la ventana transiente esta abierta debo cerrarla
    if { [ wm state $ctransient ] eq "normal" } {
      $ctransient unpost
    } else {
      # calculo la mejor posicion para desplegar la ventana transiente
      set rx [ winfo rootx $win ]
      set ry [ winfo rooty $win ]
      set h [ winfo height $win ]
      set tw [ winfo width $ctransient ]
      set th [ winfo height $ctransient ]
      # TODO: ticket #112
      foreach { min_x min_y max_x max_y } [ _get_desktop_workarea ] break;
      if { $rx + $tw > $max_x } {
        set x_post [ expr { $max_x - $tw } ]
      } else {
        set x_post $rx
      }
      if { $ry + $h + $th > $max_y } {
        set y_post [ expr { $max_y - $th } ]
      } else {
        set y_post [ expr { $ry + $h } ]
      }
      
      # despliego la ventana transiente
      $ctransient post $x_post $y_post
      after idle focus [ tk_focusNext $ctransient ]
    }
  }

  method OnAccept { } {
    # escondo la ventana transiente
    $ctransient unpost

    # sincronizo con centry
    $centry set_info [ $ctransient get_info resume ]
    after idle event generate $win <<OnAccept>>
  }

  method OnCancel { } {
    # escondo la ventana transiente
    $ctransient unpost
  }

  method set_info { info } {
    $ctransient set_info $info
    $centry set_info [ $ctransient get_info "resume" ]
  }
}

snit::widgetadaptor cEntry {
  
  delegate method * to hull
  delegate option * to hull
  
  constructor { args } {
    eval installhull using Entry $args
  }
  
  method set_info { info } {
    $win delete 0 end
    $win insert 0 $info
  }
}

snit::widgetadaptor cButton {
  
  delegate method * to hull
  delegate option * to hull
  
  constructor { args } {
    eval installhull using Button $args
    $win configure -command [list event generate $win <<OnClick>>]
  }
}

snit::widgetadaptor centry {
  
  delegate method * to hull
  delegate option * to hull
  
  constructor { args } {
    eval installhull using entry $args
  }
  
  method set_info { info } {
    set state [ $win cget -state ]
    $win configure -state normal
    $win delete 0 end
    $win insert 0 $info
    $win configure -state $state
  }
}

snit::widgetadaptor cbutton {
  
  delegate method * to hull
  delegate option * to hull
  
  constructor { args } {
    eval installhull using button $args
    $win configure -command [list event generate $win <<OnClick>>]
  }
}

namespace eval test {
  snit::widget wtest {
    component clabel
    component cbutton1
    component cbutton2
    
    constructor { args } {
      install clabel using \
          label $win.l -text "la mierda pincha en un palo"
      install cbutton1 using \
          button $win.b1 -text "pinchame y veras lo que te hago"
      install cbutton2 using \
          button $win.b2 -text "si me pinchas, me cierro" \
          -command "destroy [winfo parent $win]"
      grid $clabel
      grid $cbutton1
      grid $cbutton2
    }

    proc test01 { } {
      destroy .tr
      destroy .test1
      toplevel .test1
      entry .test1.e1 -width 20
      entry .test1.e2 -width 20
      grid .test1.e1
      grid .test1.e2
      update
      focus .test1.e2
      wtransient .tr -cuser [list test::wtest]
      .tr post 500 500
    }
  }

  snit::widgetadaptor wlist {
    delegate method * to hull
    delegate option * to hull

    constructor { args } {
      eval installhull using listbox $args
      bind $win <Button-1> +[list after idle event generate $win <<OnAccept>>]
      bind $win <Key-Return> +[list after idle event generate $win <<OnAccept>>]
      bind $win <Key-Escape> +[list after idle event generate $win <<OnCancel>>]
    }

    method get_info { what } {
      set cursel [ $win curselection ]
      if {  ![ llength $cursel ] } { return "" }
      if { $what eq "resume" } {
        $win get [ lindex $cursel 0 ]
      } else {
        $win get [ lindex $cursel 0 ]
      }
    }

    # cambia el valor de la ventana transiente
    method set_info { raw } {
      set values [ $win get 0 end ]
      if { [ set idx [ lsearch $values $raw ] ] != -1 } {
        $win see $idx
        $win selection clear 0 end
        $win selection set $idx
      }
    }
  }

  snit::widgetadaptor mycombo {
    delegate option * to hull
    delegate method * to hull

    constructor { args } {
      installhull using wcombo \
          -centry centry -cbutton cButton -cuser ::test::wlist
      $self configurelist $args
    }
  }

  proc test02 { } {
    if { [ lsearch -exact [ image names ] mac-collapse ] == -1 } {
      image create photo mac-collapse  -data {
        R0lGODlhEAAQALIAAAAAAAAAMwAAZgAAmQAAzAAA/wAzAAAzMyH5BAUAAAYA
        LAAAAAAQABAAggAAAGZmzIiIiLu7u5mZ/8zM/////wAAAAMlaLrc/jDKSRm4
        OAMHiv8EIAwcYRKBSD6AmY4S8K4xXNFVru9SAgAh/oBUaGlzIGFuaW1hdGVk
        IEdJRiBmaWxlIHdhcyBjb25zdHJ1Y3RlZCB1c2luZyBVbGVhZCBHSUYgQW5p
        bWF0b3IgTGl0ZSwgdmlzaXQgdXMgYXQgaHR0cDovL3d3dy51bGVhZC5jb20g
        dG8gZmluZCBvdXQgbW9yZS4BVVNTUENNVAAh/wtQSUFOWUdJRjIuMAdJbWFn
        ZQEBADs=
      }
    }
    set w .test02
    destroy $w
    toplevel $w
    label $w.l1 -text "Etiqueta:"
    entry $w.e 
    grid $w.l1 -row 0 -column 0 -sticky "ew"
    grid $w.e -row 0 -column 1 -sticky "ew"
    label $w.l2 -text "Escoje:"
    grid $w.l2 -row 1 -column 0 -sticky "ew"
    mycombo $w.cb
    bind $w.cb <<OnAccept>> {
      puts "<<OnAccept>> capturado: [%W entry get]"
    }
    $w.cb entry configure -state disabled
    $w.cb button configure -image mac-collapse
    $w.cb transient insert end a b c d e f
    $w.cb set_info b
    grid $w.cb -row 1 -column 1 -sticky ew
  }
}

package provide wtransient 1.0

#test::wtest::test01

#::test::test02
