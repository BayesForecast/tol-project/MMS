#/////////////////////////////////////////////////////////////////////////////
# FILE:    fit_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          Fits
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerFitsGui {
 
#/////////////////////////////////////////////////////////////////////////////
proc GetFitTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "Fit 1"
    vers "1.0"
    desc "desc of Fit 1"
    dCre "2010/08/25"
  }
}
  
variable get_fit_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetFit {ident} {
#///////////////////////////////////////////////////////////////////////////
  variable get_fit_test
  
  if { $get_fit_test } {
    return [GetFitTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::FitsGui::GetFit" \
    "Set" $ident
}

#/////////////////////////////////////////////////////////////////////////////
proc GetFitsListTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      ident "Fit 1"
    }
    {}
    {
      ident "Fit 2"
    }
  }
}
  
variable get_fits_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetFitsList {details} {
#/////////////////////////////////////////////////////////////////////////////
  variable get_fits_test
  
  if { $get_fits_test } {
    return [GetFitsListTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::FitsGui::GetFitsList" \
    "Set" $details
}

#///////////////////////////////////////////////////////////////////////////
proc GetFitHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::FitsGui::GetFitHelp" \
    "Text" "?"
}

variable create_fit_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateFit {fit_info attributes} {
#///////////////////////////////////////////////////////////////////////////
  variable create_fit_test
  upvar $fit_info arrayarg
  
  set attributes_set [TclLst2TolSet $attributes -level 2]
  
  if { $create_fit_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::FitsGui::CreateFitTest" \
	  "Real" arrayarg $attributes_set
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::FitsGui::CreateFit" \
	  "Real" arrayarg $attributes_set
  }
}

variable copy_fit_test 0
#///////////////////////////////////////////////////////////////////////////
proc CopyFit {fit_info} {
#///////////////////////////////////////////////////////////////////////////
  variable copy_fit_test
  upvar $fit_info arrayarg
  
  if { $copy_fit_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::FitsGui::CopyFitTest" \
	  "Real" arrayarg
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::FitsGui::CopyFit" \
	  "Real" arrayarg
  }
}

variable edit_fit_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditFit {fit_info attributes} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_fit_test
  upvar $fit_info arrayarg
  
  set attributes_set [TclLst2TolSet $attributes -level 2]
  
  if { $edit_fit_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::FitsGui::EditFitTest" \
	  "Real" arrayarg $attributes_set

  } else {
	LayerMMSGui::EvalTolFunArr "MMS::Layer::FitsGui::EditFit" \
	  "Real" arrayarg $attributes_set
  }
}

}
