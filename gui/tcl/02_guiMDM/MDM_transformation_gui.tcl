#/////////////////////////////////////////////////////////////////////////////
# FILE    : MDM_transformation_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with the transformations of variables (MDM layer)
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::MDMTransformationsGui {

#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmstransformation {
# PURPOSE : Defines the snit widget used to
#           edit the transformations
#/////////////////////////////////////////////////////////////////////////////

  typevariable ListFamilies
  # Families of transformations (None, BoxCox, ...)
  
  # State (readonly, normal)
  option -state \
    -default "readonly" -configuremethod "_conf-state" 

  variable widgets

  variable transf_info
  # Transformation data to be edited
    # (Class @Transformation)
    # transf_info(name)           - Transformation name (Text _.name)
    # transf_info(family)         - Family (None, BoxCox, ...)
    # transf_info(arg_name)       - Value of the argument "arg_name"
	#                                Arguments "arg_name" depends of the family
     # (Class @Transformation.BoxCox)
      # transf_info(first)        - Value for argument First (Real _.first)
      # transf_info(second)       - Value for argument Second (Real _.second)

  component dlg
  
  delegate method * to hull
  delegate option * to hull

  #typeconstructor

  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Initialize the variable information
    $self _init
    
    # Paint the window
    $self _create

    # Apply all options passed at creation time.
    $self configurelist $args
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky news

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ s } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $s
    foreach w {family} {
      if {$s eq "normal"} {
	    $widgets($w) configure -state "normal"
	  } else {
	    $widgets($w) configure -state "disabled"
	  }
    }
	if {[info exists widgets(args)]} {
	  foreach {w} [winfo children $widgets(args)] {
	    if {[winfo class $w] eq "Entry"} {
          $w configure -state $s
		}
	  }
    }	  
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #///////////////////////////////////////////////////////////////////////////

	set ListFamilies [concat \
	  [list [mc "None"]] \
	  [::LayerMDMGui::GetTransformationFamilies]
	]
	$self ClearInfo
  }
 
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          to define a transformation
  #///////////////////////////////////////////////////////////////////////////
   
    set fe $dlg

    label $fe.lFamily -text "[mc "Family"]:" -pady 5 -padx 5

    if {$options(-state) eq "normal"} {
	  set _state "normal"
	} else {
	  set _state "disabled"
	}
    ComboBox $fe.cbFamily -values $ListFamilies \
      -textvariable [myvar transf_info(family)] -editable false \
      -width 15 -state $_state
    set widgets(family) $fe.cbFamily

	set fargs [frame $fe.fargs]
    set widgets(args) $fe.fargs

	foreach family [lrange $ListFamilies 1 end] {
	  set arguments [::LayerMDMGui::GetFamilyArguments $family]
	  #puts "_create:family=$family,arguments=$arguments"
	  set row 0
	  foreach arg $arguments {
        label $fargs.l$family$arg -text "[mc $arg]:" -pady 5 -padx 5
        entry $fargs.e$family$arg -textvariable [myvar transf_info($arg)] \
          -width 20 -state $options(-state)
        set widgets($family$arg) $fargs.e$family$arg
        grid $fargs.l$family$arg  -row $row -column 0 -sticky e
        grid $fargs.e$family$arg  -row $row -column 1 -sticky w
        grid remove $fargs.l$family$arg
        grid remove $fargs.e$family$arg
	    incr row
	  }
	}
	
    grid rowconfigure    $fargs 0 -weight 1
    grid columnconfigure $fargs 2 -weight 1
	
    label $fe.lName -text "[mc "Name"]:" -pady 5 -padx 5

    entry $fe.eName -textvariable [myvar transf_info(name)] \
      -width 30 -state readonly
    set widgets(lname) $fe.lName
    set widgets(name) $fe.eName
    
    grid $fe.lFamily  -row 0 -column 0 -sticky e
    grid $fe.cbFamily -row 0 -column 1 -sticky w
    grid $fe.fargs    -row 1 -column 0 -columnspan 2 -sticky news
    grid $fe.lName    -row 2 -column 0 -sticky e
    grid $fe.eName    -row 2 -column 1 -sticky w

    grid rowconfigure    $fe 0 -weight 1
    grid columnconfigure $fe 2 -weight 1
    grid $fe -sticky news

	foreach w {lname name args} {
      grid remove $widgets($w)
	}
    
    $fe.cbFamily configure \
	  -modifycmd [string map [list %W $fe.cbFamily %L $ListFamilies %M [mymethod ShowArguments]] {
      set idx [%W getvalue]
      if {$idx != -1} {   
        set family [lindex [list %L] $idx]
        %M $family 1
      }
    }]
	  
    foreach {w} {name} {
      bind $widgets($w) <Return> "$self Ok ; break"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {family} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {name \
                 family} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
	if {[info exists widgets(args)]} {
	  foreach {w} [winfo children $widgets(args)] {
        bind $w <Return> "$self Ok ; break"
        bind $w <Escape> "$self Cancel"
        bind $w <Down> {event generate %W <Tab>}
        bind $w <Up> {event generate %W <Shift-Tab>}
	  }
    }	  
  }

  #/////////////////////////////////////////////////////////////////////////////
  method ShowArguments {family show_default_value} {
  #/////////////////////////////////////////////////////////////////////////////

	foreach {w} [winfo children $widgets(args)] {
      grid remove $w
	}
    if {$family ne [mc "None"]} {	
	  if {$show_default_value} {
		set transf_info(name) ""
      }
	  foreach w {lname name args} {
        grid $widgets($w)
	  }
	  set arguments [::LayerMDMGui::GetFamilyArguments $family]
	  foreach arg $arguments {
        grid $widgets(args).l$family$arg
        grid $widgets(args).e$family$arg
		if {$show_default_value} {
		  set transf_info($arg) [::LayerMDMGui::GetArgumentDefaultValue $family $arg]
		}
	  }
    } else {
	  foreach w {lname name args} {
        grid remove $widgets($w)
	  }
    }	
  }

  #/////////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #/////////////////////////////////////////////////////////////////////////////

    set transf_info(family) [mc "None"]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnAccept>>
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnCancel>>    
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Changed {args} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnChanged>>
  }

  #/////////////////////////////////////////////////////////////////////////////
  method set_info {iteminfo} {
  #/////////////////////////////////////////////////////////////////////////////
 
    #puts "iteminfo=$iteminfo"
    if {$iteminfo=={}} {
	  set family [mc "None"]
      $self ClearInfo
    } else {
      array set transf_info $iteminfo
	  set family $transf_info(family)
	  set arguments [lrange $iteminfo 4 end]
	  #puts "set_info:childrens=[winfo children $widgets(args)]"
	  foreach {_ arg _ value} $arguments {
		set transf_info($arg) $value
	  }	  
    }
    $self ShowArguments $family 0
  }

  #/////////////////////////////////////////////////////////////////////////////
  method get_info {what} {
  #/////////////////////////////////////////////////////////////////////////////

    if {$transf_info(family) eq [mc "None"]} {
      set transf_info(family) "None"
    }	  
    return [array get transf_info]
  }

}

}

