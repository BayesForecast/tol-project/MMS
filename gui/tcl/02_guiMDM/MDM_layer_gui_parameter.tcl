#/////////////////////////////////////////////////////////////////////////////
# FILE:    MDM_layer_gui_parameter.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDM (parameters)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMDMGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetParameterTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "Parameter 1"
    type "ARIMA"
    initial "1.0"
    fixed "1"
    active "1"
  }
}
  
variable get_parameter_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetParameter {ident container} {
#///////////////////////////////////////////////////////////////////////////
  variable get_parameter_test
  
  if { $get_parameter_test } {
    return [GetParameterTest]
  }
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetParameter" \
    "Set" $ident_text $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetParametersListTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "Parameter 1"
      type "ARIMA"
    }
    {}
    {
      name "Parameter 2"
      type "ExpTerm"
    }
  }
}

variable parameters_list_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetParametersList {container details} {
#/////////////////////////////////////////////////////////////////////////////
  variable parameters_list_test
  
  if { $parameters_list_test } {
    return [GetParametersListTest]
  }
  set container_set [TclLst2TolSet $container]
  set details_text \"[LayerMMSGui::TolText $details]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetParametersList" \
    "Set" $container_set $details_text
}

#///////////////////////////////////////////////////////////////////////////
proc GetParameterHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetParameterHelp" \
    "Text" "?"
}

variable create_parameter_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateParameter {parameter_info container prior constraint} {
#///////////////////////////////////////////////////////////////////////////
  variable create_parameter_test
  upvar $parameter_info parameter_array

  set container_set [TclLst2TolSet $container]

  set prior_set [TclLst2TolSet $prior]
  set constraint_set [TclLst2TolSet $constraint]
  
  if { $create_parameter_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateParameterTest" \
      "Real" parameter_array $container_set $prior_set $constraint_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateParameter" \
      "Real" parameter_array $container_set $prior_set $constraint_set
  }
}

variable edit_parameter_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditParameter {parameter_info container prior constraint} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_parameter_test
  upvar $parameter_info parameter_array
  
  set container_set [TclLst2TolSet $container]

  set prior_set [TclLst2TolSet $prior]
  set constraint_set [TclLst2TolSet $constraint]
  
  if { $edit_parameter_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditParameterTest" \
      "Real" parameter_array $container_set $prior_set $constraint_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditParameter" \
      "Real" parameter_array $container_set $prior_set $constraint_set
  }
}

}
