#/////////////////////////////////////////////////////////////////////////////
# FILE    : MDM_expterm_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with the Explanatory terms of a model (MDM layer)
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::MDMExpTermsGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateExpTermsListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  
  #puts "CreateExpTermsListDetails"
  set l_details_frame [frame $f.details_ExpTerms]

  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "ExpTerm" \
    -swlist "::MDMExpTermsGui::bmmsexptlist" \
	-fshowitem "::MDMExpTermsGui::_ShowExpTermDetails" \
	-fshowlist "::MDMExpTermsGui::_ShowExpTermsListDetails" \
  ]

  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowExpTermsListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreateExpTermsListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details configure -container $container
  $l_details_frame.details Init

  return $l_details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowExpTermsListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////
  
  set submodel [$tree item parent $id]
  set submodel_name [$tree item text $submodel first]
  set grandparent [$tree item parent $submodel]
  set model [$tree item parent $grandparent]
  set model_name [$tree item text $model first]
  set container [::MDMGui::GetModelContainer $tree $model]
  lappend container $model_name
  lappend container $submodel_name
  
  _ShowExpTermHelp  

  _ShowExpTermsListDetails $container
}

#/////////////////////////////////////////////////////////////////////////////
proc CMenuExpTerms {tree} {
#/////////////////////////////////////////////////////////////////////////////

  if {[::MMSGui::IsEditionActive]} {
    set _state "disabled"
  } else {
    set _state "normal"
  }
  $tree.cmenu delete 0 end
  $tree.cmenu add command -label [mc "New Explanatory Term"] \
    -command "::MDMExpTermsGui::NewExpTerm" \
	-state $_state
}

#/////////////////////////////////////////////////////////////////////////////
proc NewExpTerm {} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details Details "New"
}

#/////////////////////////////////////////////////////////////////////////////
proc ChangeContainerTree {new_ident} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details ChangeActiveItem $new_ident
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowExpTermHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerMDMGui::GetExpTermHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateExpTermDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreateExpTermDetails"
  set details_frame [frame $f.details_ExpTerm]
  set view_frame [frame $f.view_ExpTerm]

  set _details [bmmsexpterm $details_frame.details]
  set _view [bmmsexpterm $view_frame.details]

  bind $_details <<Insert>> \
    "event generate $f <<Insert>>"

  bind $_details <<Refresh>> \
    "event generate $f <<Refresh>>"

  bind $_details <<Ok>> \
    "event generate $details_frame <<Ok>>"
  
  bind $_details <<Cancel>> \
    "event generate $details_frame <<Cancel>>"
  
  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowExpTermDetails {container ident state parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateExpTermDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -container $container
  $_frame.details configure -item $ident
  $_frame.details configure -state $state
  $_frame.details configure -parent $parent
  
  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowExpTermDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set submodel [$tree item parent $parent]
  set submodel_name [$tree item text $submodel first]
  set grandparent [$tree item parent $submodel]
  set model [$tree item parent $grandparent]
  set model_name [$tree item text $model first]
  set container [::MDMGui::GetModelContainer $tree $model]
  lappend container $model_name
  lappend container $submodel_name
 
  set ident [$tree item text $id first]

  _ShowExpTermDetails $container $ident "View" "tree"
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandExpTermsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set submodel [$tree item parent $id]
  set submodel_name [$tree item text $submodel first]
  set grandparent [$tree item parent $submodel]
  set model [$tree item parent $grandparent]
  set model_name [$tree item text $model first]
  set container [::MDMGui::GetModelContainer $tree $model]
  lappend container $model_name
  lappend container $submodel_name
  set _details "no" 
  
  set _list [LayerMDMGui::GetExpTermsList $container $_details]
  #puts "ExpandExpTermsList:_list=$_list"

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  foreach {{} it} $_list {
    array set vinfo $it
    set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
    set row [list [list $icon $vinfo(ident)] \
	          [list "ExplanatoryTerm"] \
	          [list "MDMExpTermsGui::ExpandExpTerm"] \
			  [list "MDMExpTermsGui::ShowExpTermDetails"] \
	          [list ""] \
			  [list $vinfo(abs_id)] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandExpTerm {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  set row [list [list [::Bitmap::get "Set"] [mc "Parameters"]] \
			    [list "Parameters"] \
	            [list "MDMParametersGui::ExpandParametersList"] \
			    [list "MDMParametersGui::ShowParametersListDetails"] \
			    [list ""] \
			    [list ""] \
          ]
  $tree insert $row \
    -at child -relative $id
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsexptlist {
# PURPOSE : Defines the snit widget used to
#           list the Explanatory terms of a model
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the container
  option -container \
    -default "" -configuremethod "_conf-container"  

  option -details \
    -default "no" -configuremethod "_conf-details"  

  variable mdm_expterm
    # mdm_expterm(list)  - Explanatory Terms list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
    $self FillList
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the Explanatory terms of a model
  #///////////////////////////////////////////////////////////////////////////
  
    set f $dlg
 
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Explanatory Terms List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set ft [frame $f.ft]
    
	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Name"]] \
        [list text -label [mc "Type"]] \
        [list text -label [mc "Description"]] \
        [list text -label [mc "Input"]] \
        [list text -label [mc "Grammar"]] \
        [list text -label [mc "Additive"]] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns 

    #$tree configure -contextmenu [$self CreateCMenu]
	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    focus $tree

    set mdm_expterm(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mdm_expterm(list) [LayerMDMGui::GetExpTermsList \
      $options(-container) $options(-details)]
    #puts "FillList mdm_expterm(list)=$mdm_expterm(list)"

    $tree item delete all

    foreach {{} it} $mdm_expterm(list) {
      array set vinfo $it
      set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(type)] \
	      [list $vinfo(desc)] \
	      [list $vinfo(inp)] \
	      [list $vinfo(grammar)] \
	      [list $vinfo(additive)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }
	  
      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
              [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    if {$cur_item != 0} {
      $tree activate $cur_item
      $tree selection add $cur_item
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveLast {} {
  #/////////////////////////////////////////////////////////////////////////////

    $tree selection clear
    $tree activate last
    $tree selection add last
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }
  
  
  #/////////////////////////////////////////////////////////////////////////////
  method CreateCMenu { } {
  #/////////////////////////////////////////////////////////////////////////////

    menu $tree.cmenu -tearoff 0 \
      -postcommand [mymethod PostCMenu $tree.cmenu]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method PostCMenu {cmenu} {
  #/////////////////////////////////////////////////////////////////////////////

    $cmenu delete 0 end
    set selection [$tree selection get]
    set has_selection [llength $selection]
    set state_selection [expr { $has_selection?"normal":"disable" }]
    $cmenu add command -label [mc "Remove"] \
	  -command "[mymethod Remove]" -state $state_selection
    $cmenu add command -label [mc "Enable/Disable"] \
	  -command "[mymethod OnOff]" -state $state_selection
    if {$has_selection} {
      $cmenu add separator
      foreach {idx references} [$self GetRefSelected] break
      set access_info [list "ExpTerm" $options(-model) $options(-version)]
      ::MMSGui::FillOptionsMenu $cmenu $access_info $references $idx
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetRefSelected { } {
  #/////////////////////////////////////////////////////////////////////////////

    set active [$tree index active]
    set i 0
    set idx_active 0
	set selected ""
    foreach item [$tree selection get] {
      if { $i eq $active } {
        set idx_active $i
      }
      set name [$tree item text $item 0]
      lappend selected [list "ExpTerm" $name]
      #set type [$tree item text $item 1]
      #lappend selected [list $type $name]
      incr i
    }
    list $idx_active $selected
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Remove { } {
  #/////////////////////////////////////////////////////////////////////////////
  
    set selected [$tree selection get]
    foreach {item} $selected {
      set name [$tree item text $item 0]
      LayerMDMGui::RemoveExpTerm $name $modInfo(name) $modInfo(vers)
    }
    
    set sibling [$tree item nextsibling $item]
    if {$sibling != ""} {
      set name [$tree item text $sibling 0]
    }
    $self FillList
    if {$sibling == ""} {
      if {[$tree item count] != 1} {
        set name [$tree item text "rnc 0 0" 0]
      } else {
        set name ""
      }
    }
    $self MakeActiveItem $name
  }

  #/////////////////////////////////////////////////////////////////////////////
  method OnOff { } {
  #/////////////////////////////////////////////////////////////////////////////
  
    set selected [$tree selection get]
    foreach {item} $selected {
      set name [$tree item text $item 0]
      LayerMDMGui::OnOffExpTerm $name $modInfo(name) $modInfo(vers)
    }
    $self RefreshList
  }
  
}  


#/////////////////////////////////////////////////////////////////////////////
::snit::widget not_used_bmmsetparameters {
# PURPOSE : Defines the snit widget used to edit
#           the parameters of the Explanatory term
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  variable widgets

  # State (disabled, normal)
  option -state \
    -default "disabled" -configuremethod "_conf-state" 

  variable mdm_parameters
  # Parameters to be edited at the tablelist
    
  variable tablelist_options
  variable par_fixed
  # Used at the option -variable in checkbuttons to fix the parameters
  
  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor

  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Initialize the variable information
    $self _init
    
    # Paint the window
    $self _create

    # Apply all options passed at creation time.
    $self configurelist $args
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method put_state {s} {
  #///////////////////////////////////////////////////////////////////////////

    $widgets(tbl) configure -state $s
    set lrow [$widgets(tbl) size]
    for {set _r 0} {$_r < $lrow} {incr _r} {
      set chkbtn [$widgets(tbl) windowpath $_r,end]
      $chkbtn configure -state $s
    }
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ s } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $s
	$self put_state $s
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #
  # PURPOSE: Initializes the options for the tablelist
  #///////////////////////////////////////////////////////////////////////////
 
    set mdm_parameters [list]

    set tablelist_options(-label) [mc "Parameters"]
    set tablelist_options(-columns) [list \
      0 [mc "Name"] left \
      0 [mc "Type"] left \
      0 [mc "Degree"] right \
      0 [mc "Initial Value"] right \
      0 [mc "Fixed"] center \
    ]
    set tablelist_options(-listvar) [myvar mdm_parameters]   
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          to edit a list of parameters
  #///////////////////////////////////////////////////////////////////////////
   
    set ft $dlg
    # Tablelist frame

    tablelist::tablelist $ft.tbl \
      -columns $tablelist_options(-columns) \
      -listvariable $tablelist_options(-listvar) \
      -xscrollcommand "$ft.xs set" \
      -yscrollcommand "$ft.ys set" \
      -height 5 -width 80
    set widgets(tbl) $ft.tbl

    scrollbar $ft.ys -orient v -command "$ft.tbl yview"
    scrollbar $ft.xs -orient h -command "$ft.tbl xview"

    grid $ft.tbl  -row 0 -column 0 -sticky nsew
    grid $ft.ys   -row 0 -column 1 -sticky ns
    grid $ft.xs   -row 2 -column 0 -sticky ew

    grid columnconfigure $ft 0 -weight 1
    grid rowconfigure $ft 0 -weight 1
	grid $ft -sticky news

    $widgets(tbl) columnconfigure 0 -name col_name -editable no -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 1 -name col_type -editable no -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 2 -name col_degree -editable no -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 3 -name col_ival -editable yes -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 4 -name col_fixed -labelcommand "$self DoNothing"
    
    foreach {w} {tbl} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method DoNothing {args} {
  #/////////////////////////////////////////////////////////////////////////////
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method createCheckButton {tbl row col w} {
  #/////////////////////////////////////////////////////////////////////////////

    checkbutton $w -highlightthickness 0 \
	  -font {{MS Sans Serif} 4} \
	  -onvalue 1 -offvalue 0 \
	  -bd 0 -bg white \
	  -variable [myvar par_fixed($row)]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnAccept>>
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnCancel>>    
  }

  #/////////////////////////////////////////////////////////////////////////////
  method set_info {iteminfo} {
  #/////////////////////////////////////////////////////////////////////////////
	
    #puts "set_info:iteminfo=$iteminfo"
	set _state $options(-state)
	$self put_state "normal"
	
    $widgets(tbl) delete 0 end
    set idx_fixed [expr [$widgets(tbl) columncount]-1] 
    set row 0
    foreach {r} $iteminfo {
      $widgets(tbl) insert end [lrange $r 0 [expr $idx_fixed-1]]
	  set par_fixed($row) [lindex $r $idx_fixed]
      $widgets(tbl) cellconfigure $row,end -window [mymethod createCheckButton]
      #set chkbtn [$widgets(tbl) windowpath $row,$lcol]
      #set [$chkbtn cget -variable] [lindex $r $lcol]
      incr row
    }

	$self put_state $_state
  }

  #/////////////////////////////////////////////////////////////////////////////
  method get_info {what} {
  #/////////////////////////////////////////////////////////////////////////////

    set idx_fixed [expr [$widgets(tbl) columncount]-1] 
    set _parameters [list]
    set row 0
    foreach {r} $mdm_parameters {
	  set par [lrange $r 0 [expr $idx_fixed-1]]
      lappend _parameters [concat $par $par_fixed($row)]
      incr row
    }
    #puts "Ok:_parameters=$_parameters"
    return $_parameters
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method finishediting {} {
  #/////////////////////////////////////////////////////////////////////////////

    $widgets(tbl) finishediting
  }
  
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsexpterm {
# PURPOSE : Defines the snit widget used to
#           create new Explanatory terms or editing existing ones
#/////////////////////////////////////////////////////////////////////////////

  typevariable ListTypes
  # Types of Explanatory terms (Linear, Omega, Ratio, NonLinear)

  # The container to wich the Explanatory term belongs
  option -container \
    -default "" -configuremethod "_conf-container"  
  # Container is a list of the form {<model-container> <ident-submodel>}

  # Name of the specific Explanatory term to treat
  option -item \
    -default "" -configuremethod "_conf-item"  

  # Who did call me? (tree (MMS tree), list (Variables List))
  option -parent \
    -default "tree" -configuremethod "_conf-parent"  

  # Current state (View, Edit, New, Copy)
  option -state \
    -default "Details" -configuremethod "_conf-state"  
	
  variable label_state

  variable widgets
    
  variable expterm_info
  # Explanatory Term data to be edited
   # (Class @ExpTerm)
    # expterm_info(name)            - Explanatory Term name (Text _.name)
    # expterm_info(desc)            - Exp. Term description (Text _.description)
    # expterm_info(g_active)        - Global active (IsActive method)
    # expterm_info(l_active)        - Local active (Real _.isActive)
    # expterm_info(additive)        - (Real _.isAdditive)
    #                               - (Set _.inputs_)
    # expterm_info(input)           - Input Identifier (@MVariable _.input[1])
    # expterm_info(grammar)         - Grammar (Serie, Matrix, Real)
    #                               - (Set _.parametersLinear)
    # expterm_info(type)            - Explanatory Term type (Linear, Omega, Ratio, NonLinear)
     # (Class @ExpTermLinear)       - (uni_input, uni_linear)
      # expterm_info(func)          - Real Coefficient
     # (Class @ExpTermOmega)        - (uni_input, multi_linear)
      # expterm_info(func)          - Polyn TransferFunction
     # (Class @ExpTermRatio)        - (uni_input, multi_linear, non_linear)
      # expterm_info(func)          - Ratio TransferFunction
	  #                             - (Set _.parametersNonLinear)
     # (Class @ExpTermNonLinear)    - (multi_input, multi_linear, non_linear)
	  #                             - (Set _.parametersNonLinear)
	  #                             - (Set _.nonLinearFilter)

  component dlg

  delegate method * to hull
  delegate option * to hull

  typeconstructor {

    set ListTypes {Linear Omega Ratio NonLinear}
  }
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-item { _ item } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-item) $item
    if {$item eq ""} {
	  return
	}
    $self Details
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent { _ parent } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-parent) $parent 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ state } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $state
    switch -- $state {
      "View"  {$self Details}
      "Edit"  {$self Edit}
      "New"   {$self New}
      "Copy"  {$self Copy}
    }
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates a form to edit an Explanatory Term
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
  
    # Buttons: New, Edit, Copy
    set fbu [frame $f.fbu]
  
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc New] \
      -helptext [mc "New Explanatory Term"] -padx 1 -relief link \
      -compound left -command [list $self New] \
      -state normal
    set widgets(new) $fbu.bNew
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit Explanatory Term"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    Button $fbu.bCopy -image [::Bitmap::get copy] -text [mc "Duplicate"] \
      -helptext "[mc "Duplicate"] [mc "Explanatory Term"]" -padx 1 -relief link \
      -compound left -command [list $self Copy] \
      -state disabled
    set widgets(copy) $fbu.bCopy

    grid $fbu.bNew $fbu.bEdit $fbu.bCopy -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 3 -weight 1

    # Labels and Entries
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]

    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	  
    label $fe.lName -text "[mc "Name"]:" -pady 5 -padx 5
    label $fe.lType -text "[mc "Type"]:" -pady 5 -padx 5
    label $fe.lDesc -text "[mc "Description"]:" -pady 5 -padx 5
    label $fe.lActive -text "[mc "Active"]:" -pady 5 -padx 5
    label $fe.lInp  -text "[mc "Input"]:" -pady 5 -padx 5
    label $fe.lGram -text "[mc "Grammar"]:" -pady 5 -padx 5
    
    entry $fe.eName -textvariable [myvar expterm_info(name)] \
      -width 40 -state readonly 
    set widgets(name) $fe.eName

    ComboBox $fe.cbType -values $ListTypes \
      -textvariable [myvar expterm_info(type)] -editable false \
      -width 15 -state disabled \
      -modifycmd [string map [list %W $fe.cbType %L $ListTypes \
	                               %F [mymethod UpdateLabelFunction]] {
        set idx [%W getvalue]
        if {$idx != -1} {   
          set type [lindex [list %L] $idx]
		  %F $type
        }
      }]
    set widgets(type) $fe.cbType
	
    entry $fe.eDesc -textvariable [myvar expterm_info(desc)] \
      -width 60 -state readonly
    set widgets(desc) $fe.eDesc

    set factive [frame $fe.fActive]

    checkbutton $factive.chkbLActive -variable [myvar expterm_info(l_active)] \
      -text [mc "Local"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(l_active) $factive.chkbLActive

    checkbutton $factive.chkbGActive -variable [myvar expterm_info(g_active)] \
      -text [mc "Global"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(g_active) $factive.chkbGActive

    grid $factive.chkbLActive $factive.chkbGActive -sticky w -padx 2 -pady 2
    grid rowconfigure    $factive 0 -weight 1
    grid columnconfigure $factive 2 -weight 1

    ::MMSSelectorsGui::comboselector $fe.cInpSel \
      -entry_args "-width 60 -state readonly" \
      -button_args [list -image [::Bitmap::get puntos] \
        -helptext [mc "Select a Model Variable"] \
        -padx 10 -relief link -compound left \
        -state disabled] \
      -transf_args "" \
	  -type "MVariable" \
	  -swlist "::MDMMVariablesGui::bmmsmvarlist"
    set widgets(inp) $fe.cInpSel

    bind $widgets(inp) <<OnAccept>> "$self UpdateVariableGrammar"

    entry $fe.eGram -textvariable [myvar expterm_info(grammar)] \
      -width 30 -state readonly
    set widgets(grammar) $fe.eGram

    checkbutton $fe.chkbAdditive -variable [myvar expterm_info(additive)] \
      -text [mc "Additive"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(additive) $fe.chkbAdditive

    label $fe.lHType -text [mc "Transfer Function"] -pady 5 -padx 5
    set ftype [labelframe $fe.ftype \
      -labelwidget $fe.lHType -relief solid -bd 1]
    set widgets(ftype) $ftype

    label $ftype.lFunc -image [::Bitmap::get Real] -text "[mc "Coefficient"]:" \
      -compound left -pady 5 -padx 5
    set widgets(lfunc) $ftype.lFunc

    entry $ftype.eFunc -textvariable [myvar expterm_info(func)] \
      -width 30 -state readonly 
    set widgets(func) $ftype.eFunc
  
    grid $ftype.lFunc -row 0 -column 0 -sticky e
    grid $ftype.eFunc -row 0 -column 1 -sticky w
    grid rowconfigure    $ftype 1 -weight 1
    grid columnconfigure $ftype 2 -weight 1

    grid $fe.lName        -row 0 -column 0 -sticky e
    grid $fe.eName        -row 0 -column 1 -sticky w
    grid $fe.lType        -row 1 -column 0 -sticky e
    grid $fe.cbType       -row 1 -column 1 -sticky w
    grid $fe.lDesc        -row 2 -column 0 -sticky e
    grid $fe.eDesc        -row 2 -column 1 -sticky w
    grid $fe.lActive      -row 3 -column 0 -sticky e
    grid $fe.fActive      -row 3 -column 1 -sticky w
    grid $fe.lInp         -row 4 -column 0 -sticky e
    grid $fe.cInpSel      -row 4 -column 1 -sticky w
    grid $fe.lGram        -row 5 -column 0 -sticky e
    grid $fe.eGram        -row 5 -column 1 -sticky w
    grid $fe.chkbAdditive -row 6 -column 1 -sticky w
    grid $fe.ftype        -row 7 -column 0 -sticky news \
	                       -columnspan 2 -pady 10 -padx 10
	
    grid rowconfigure    $fe 8 -weight 1
    grid columnconfigure $fe 2 -weight 1

	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
	
    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1
    
    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    foreach {w} {name desc \
                 func \
				 l_active additive} {
      bind $widgets($w) <Return> "$self Ok"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {accept cancel \
	             type inp} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {name desc \
                 func \
	             inp l_active additive \
                 accept cancel} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $widgets(name) <Shift-Tab> "focus $widgets(cancel) ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $widgets(accept) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method UpdateLabelFunction {_type} {
  #/////////////////////////////////////////////////////////////////////////////

    switch -- $_type {
      "Linear" {
        set icon [::Bitmap::get "Real"]
        set label "Coefficient"
      }
      "Omega"  {
        set icon [::Bitmap::get "Polyn"]
        set label "Polynomial"
      }
      "Ratio"  {
        set icon [::Bitmap::get "Ratio"]
        set label "Ratio"
      }
      "NonLinear"  {
        set icon [::Bitmap::get "Polyn"]
        set label "Polynomial"
      }
    }
    $widgets(ftype).lFunc configure -image $icon
    $widgets(ftype).lFunc configure -text "[mc $label]:"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method UpdateVariableGrammar {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    $self GetInput
    set expterm_info(grammar) [::LayerMDMGui::GetMVariableGrammar \
	  $expterm_info(inp) [lrange [$self cget -container] 0 end-1]]
    if {$expterm_info(name) eq ""} {
      set expterm_info(name) $expterm_info(inp)
	}
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetInput {} {
  #/////////////////////////////////////////////////////////////////////////////

    set expterm_info(inp) [$widgets(inp) get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetInput {} {
  #/////////////////////////////////////////////////////////////////////////////

	$widgets(inp) transient configure -container \
	  [lrange [$self cget -container] 0 end-1]
    $widgets(inp) set_info $expterm_info(inp)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable prev_type
	
    set expterm_info(name) ""
    set expterm_info(desc) ""
	
    set expterm_info(l_active) 1
    set expterm_info(g_active) 0
    set expterm_info(additive) 0
	
    set expterm_info(type) "Linear"
	set prev_type $expterm_info(type)

    set expterm_info(func) ""
	
    set expterm_info(inp) ""
    $self SetInput
	set expterm_info(grammar) ""

	$self UpdateLabelFunction $expterm_info(type)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set ident [$self cget -item]

	array set expterm_info [LayerMDMGui::GetExpTerm $ident \
      $options(-container)]

    $self SetInput

	$self UpdateLabelFunction $expterm_info(type)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable prev_type

    set options(-state) "View"
    set label_state [mc "Details of the Explanatory Term"]
    
    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    foreach {w} {new edit copy} {
      $widgets($w) configure -state $_state
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {name desc func} {
      $widgets($w) configure -state readonly
    }
    foreach {w} {type l_active additive} {
      $widgets($w) configure -state disabled
    }
    $widgets(inp) button configure -state disabled

    $self GetInfo
	
	set prev_type $expterm_info(type)
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Edit"
    set label_state [mc "Edit Explanatory Term"]
    ::MMSGui::ActivateEdition $win

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name desc func \
                 type l_active additive} {
      $widgets($w) configure -state normal
    }
    $widgets(inp) button configure -state normal
	
    focus $widgets(desc)
    bind $widgets(cancel) <Tab> "focus $widgets(desc) ; break"
    bind $widgets(desc) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Create {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name desc func \
                 type l_active additive} {
      $widgets($w) configure -state normal
    }
    $widgets(inp) button configure -state normal
  
    focus $widgets(name)
    bind $widgets(cancel) <Tab> "focus $widgets(name) ; break"
    bind $widgets(desc) <Shift-Tab> "focus $widgets(name) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method New {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "New"
    set label_state [mc "New Explanatory Term"]
    ::MMSGui::ActivateEdition $win

    $self ClearInfo

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Copy {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Copy"
    set label_state "[mc "Duplicate"] [mc "Explanatory Term"]"
    ::MMSGui::ActivateEdition $win

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable prev_type

	::MMSGui::DisactivateEdition
	
    $self GetInput
    
	#set expterm_info(model) [lindex [$self cget -container] 1]
	#set expterm_info(submodel) [lindex [$self cget -container] 0]
	set expterm_info(container) [$self cget -container]

    if {$options(-state) eq "Edit"} {
	  set expterm_info(ident) [$self cget -item]
	  if {$prev_type==$expterm_info(type)} {
        LayerMDMGui::EditExpTerm expterm_info $expterm_info(container)
	  
	    set new_ident $expterm_info(name)
	    if {$expterm_info(ident) ne $new_ident} {
	      if {$options(-parent) eq "tree"} {
            array set new_info [LayerMDMGui::GetExpTerm \
              $new_ident [$self cget -container]]
	        set new_absid $new_info(abs_id)
            ::MMSGui::ChangeMMSTree $new_ident $new_absid
	      } else {
            ::MDMExpTermsGui::ChangeContainerTree $new_ident
            event generate $self <<Refresh>>
	      }
	    }

      } else {
	    LayerMDMGui::RemoveExpTerm $expterm_info(ident) $expterm_info(container)
        LayerMDMGui::CreateExpTerm expterm_info $expterm_info(container)
	    if {$options(-parent) eq "tree"} {
          event generate $self <<Insert>>
	    } else {

	      set new_ident $expterm_info(name)
	      if {$expterm_info(ident) ne $new_ident} {
		    ::MDMExpTermsGui::ChangeContainerTree $new_ident
          }
		  
		  event generate $self <<Refresh>>
	    }
	  }
      
    } else {                                 ;# New, Copy
      LayerMDMGui::CreateExpTerm expterm_info $expterm_info(container)
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }
    }
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Ok>>
	}
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Cancel>>
	}
  }

}

}
