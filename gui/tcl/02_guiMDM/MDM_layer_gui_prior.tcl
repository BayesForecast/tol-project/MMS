#/////////////////////////////////////////////////////////////////////////////
# FILE:    MDM_layer_gui_prior.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDM (priors)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMDMGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetPriorTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "Prior 1"
    elem "Parameter"
    e_name "Element of Prior 1"
    average "0"
    active "1"
  }
}
  
variable get_prior_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetPrior {ident container} {
#///////////////////////////////////////////////////////////////////////////
  variable get_prior_test
  
  if { $get_prior_test } {
    return [GetPriorTest]
  }
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetPrior" \
    "Set" $ident_text $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetPriorsListTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      ident "Prior 1"
    }
    {}
    {
      ident "Prior 2"
    }
  }
}

variable priors_list_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetPriorsList {container details} {
#/////////////////////////////////////////////////////////////////////////////
  variable priors_list_test
  
  if { $priors_list_test } {
    return [GetPriorsListTest]
  }
  set container_set [TclLst2TolSet $container]
  set details_text \"[LayerMMSGui::TolText $details]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetPriorsList" \
    "Set" $container_set $details_text
}

#///////////////////////////////////////////////////////////////////////////
proc GetPriorHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetPriorHelp" \
    "Text" "?"
}

#///////////////////////////////////////////////////////////////////////////
proc GetPriorTerms {comb_name container} {
#///////////////////////////////////////////////////////////////////////////

  set container_set [TclLst2TolSet $container]
  set comb_name_text \"[LayerMMSGui::TolText $comb_name]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetPriorTerms" \
    "Set" $comb_name_text $container_set
}

variable create_prior_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreatePrior {prior_info container} {
#///////////////////////////////////////////////////////////////////////////
  variable create_prior_test
  upvar $prior_info prior_array

  set container_set [TclLst2TolSet $container]

  if { $create_prior_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreatePriorTest" \
      "Real" prior_array $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreatePrior" \
      "Real" prior_array $container_set
  }
}

variable edit_prior_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditPrior {prior_info container} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_prior_test
  upvar $prior_info prior_array
  
  set container_set [TclLst2TolSet $container]

  if { $edit_prior_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditPriorTest" \
      "Real" prior_array $container_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditPrior" \
      "Real" prior_array $container_set
  }
}

#///////////////////////////////////////////////////////////////////////////
proc RemovePrior {ident container} {
#///////////////////////////////////////////////////////////////////////////
  
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::RemovePrior" \
    "Real" $ident_text $container_set
}

}
