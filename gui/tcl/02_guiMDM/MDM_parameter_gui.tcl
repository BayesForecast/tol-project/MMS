#/////////////////////////////////////////////////////////////////////////////
# FILE    : MDM_parameter_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with the parameters of a model (MDM layer)
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::MDMParametersGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateParametersListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  
  #puts "CreateParametersListDetails"
  set l_details_frame [frame $f.details_Parameters]

  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "Parameter" \
    -swlist "::MDMParametersGui::bmmsparlist" \
	-fshowitem "::MDMParametersGui::_ShowParameterDetails" \
	-fshowlist "::MDMParametersGui::_ShowParametersListDetails" \
	-notcreate "yes" \
  ]

  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowParametersListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreateParametersListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details configure -container $container
  $l_details_frame.details Init

  return $l_details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowParametersListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set container [GetParameterContainer $tree $id]

  _ShowParameterHelp  

  _ShowParametersListDetails $container
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateParametersMissingListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_detailsMissing_frame
  
  #puts "CreateParametersMissingListDetails"
  set l_detailsMissing_frame [frame $f.details_MissingParameters]

  set _details [::MMSContainersGui::bmmscontainer $l_detailsMissing_frame.details \
    -type "Parameter" \
    -swlist "::MDMParametersGui::bmmsparlist" \
	-fshowitem "::MDMParametersGui::_ShowParameterDetails" \
	-fshowlist "::MDMParametersGui::_ShowParametersMissingListDetails"
  ]

  grid rowconfigure $l_detailsMissing_frame 0 -weight 1
  grid columnconfigure $l_detailsMissing_frame 0 -weight 1
  grid $l_detailsMissing_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowParametersMissingListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_detailsMissing_frame

  if {![info exists l_detailsMissing_frame] || ![winfo exists $l_detailsMissing_frame]} {
    CreateParametersMissingListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_detailsMissing_frame
  }
  $l_detailsMissing_frame.details configure -container $container
  $l_detailsMissing_frame.details Init

  return $l_detailsMissing_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowParametersMissingListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set container [GetParameterContainer $tree $id]

  _ShowParameterHelp  

  _ShowParametersMissingListDetails $container
}

#/////////////////////////////////////////////////////////////////////////////
proc CMenuParameters {tree} {
#/////////////////////////////////////////////////////////////////////////////

  if {[::MMSGui::IsEditionActive]} {
    set _state "disabled"
  } else {
    set _state "normal"
  }
  $tree.cmenu delete 0 end
  $tree.cmenu add command -label [mc "New Parameter"] \
    -command "::MDMParametersGui::NewParameter" \
	-state $_state
}

#/////////////////////////////////////////////////////////////////////////////
proc NewParameter {} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_detailsMissing_frame

  $l_detailsMissing_frame.details Details "New"
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowParameterHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerMDMGui::GetParameterHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc CreateParameterDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreateParameterDetails"
  set details_frame [frame $f.details_Parameter]
  set view_frame [frame $f.view_Parameter]

  set _details [bmmsparameter $details_frame.details]
  set _view [bmmsparameter $view_frame.details]

  bind $_details <<Insert>> \
    "event generate $f <<Insert>>"

  bind $_details <<Refresh>> \
    "event generate $f <<Refresh>>"

  bind $_details <<Ok>> \
    "event generate $details_frame <<Ok>>"
  
  bind $_details <<Cancel>> \
    "event generate $details_frame <<Cancel>>"
  
  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowParameterDetails {container ident state parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateParameterDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -container $container
  $_frame.details configure -item $ident
  $_frame.details configure -state $state
  $_frame.details configure -parent $parent
  
  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowParameterDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set container [GetParameterContainer $tree $parent]

  set ident [$tree item text $id first]

  _ShowParameterDetails $container $ident "View" "tree"
}

#/////////////////////////////////////////////////////////////////////////////
proc GetParameterContainer {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set grandparent [$tree item parent $parent]
  set grandparent_name [$tree item text $grandparent first]

  if {$grandparent_name eq [mc "Explanatory Terms"] } {
    set submodel [$tree item parent $grandparent]
    set submodel_parent [$tree item parent $submodel]
    set model [$tree item parent $submodel_parent]
    set container [::MDMGui::GetModelContainer $tree $model]
    set model_name [$tree item text $model first]
    lappend container $model_name
    set submodel_name [$tree item text $submodel first]
    lappend container $submodel_name
    set expterm_name [$tree item text $parent first]
    lappend container $expterm_name
    lappend container "ExplanatoryTerm"
  } elseif {$grandparent_name eq [mc "Hierarchy Terms"] } {
    set hierarchy [$tree item parent $grandparent]
    set hierarchy_parent [$tree item parent $hierarchy]
    set model [$tree item parent $hierarchy_parent]
    set container [::MDMGui::GetModelContainer $tree $model]
    set model_name [$tree item text $model first]
    lappend container $model_name
    set hierarchy_name [$tree item text $hierarchy first]
    lappend container $hierarchy_name
    set hierterm_name [$tree item text $parent first]
    lappend container $hierterm_name
    lappend container "HierarchyTerm"
  } elseif {$grandparent_name eq [mc "Submodels"]} {
    set model [$tree item parent $grandparent]
    set container [::MDMGui::GetModelContainer $tree $model]
    set model_name [$tree item text $model first]
    lappend container $model_name
    set submodel_name [$tree item text $parent first]
    lappend container $submodel_name
    lappend container "Submodel"
  } elseif {$grandparent_name eq [mc "Hierarchies"]} {
    set model [$tree item parent $grandparent]
    set container [::MDMGui::GetModelContainer $tree $model]
    set model_name [$tree item text $model first]
    lappend container $model_name
    set hierarchy_name [$tree item text $parent first]
    lappend container $hierarchy_name
    lappend container "Hierarchy"
  } elseif {$grandparent_name eq [mc "Model Variables"]} {
    set model [$tree item parent $grandparent]
    set container [::MDMGui::GetModelContainer $tree $model]
    set model_name [$tree item text $model first]
    lappend container $model_name
    set mvariable_name [$tree item text $parent first]
    lappend container $mvariable_name
    lappend container "MVariable"
  } else {	          ;# Model
    set model $parent
    set container [::MDMGui::GetModelContainer $tree $model]
    set model_name [$tree item text $model first]
    lappend container $model_name
  }
  #puts "GetParameterContainer:container=$container"
  return $container
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandParametersList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set container [GetParameterContainer $tree $id]
  set _details "no" 
	
  set _list [LayerMDMGui::GetParametersList $container $_details]
  #puts "ExpandParametersList:_list=$_list"

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  foreach {{} it} $_list {
    array set vinfo $it
	set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
    set row [list [list $icon $vinfo(ident)] \
	          [list "Parameter"] \
	          [list ""] \
			  [list "MDMParametersGui::ShowParameterDetails"] \
	          [list ""] \
			  [list $vinfo(abs_id)] \
            ]
    $tree insert $row \
      -at child -relative $id -button no
  }
}

      
#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsparlist {
# PURPOSE : Defines the snit widget used to
#           list the parameters of a model
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the model
  option -container \
    -default "" -configuremethod "_conf-container"  

  option -details \
    -default "no" -configuremethod "_conf-details"  

  variable mdm_parlist
    # mdm_parlist(list)  - Parameters list
  
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
    $self FillList
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the parameters of a model
  #///////////////////////////////////////////////////////////////////////////
  
    set f $dlg
 
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Parameters List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set ft [frame $f.ft]
    
	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Name"]] \
        [list text -label [mc "Type"]] \
        [list text -label [mc "Initial Value"]] \
        [list text -label [mc "Fixed"]] \
        [list text -label [mc "Mean"]] \
        [list text -label [mc "Sigma"]] \
        [list text -label [mc "Inferior value"]] \
        [list text -label [mc "Superior value"]] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns
    
    #$tree configure -contextmenu [$self CreateCMenu]
	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    focus $tree

    set mdm_parlist(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mdm_parlist(list) [LayerMDMGui::GetParametersList \
      $options(-container) $options(-details)]
    #puts "FillParametersList mdm_parlist(list)=$mdm_parlist(list)"

    $tree item delete all

    foreach {{} it} $mdm_parlist(list) {
      array set vinfo $it
	  set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(type)] \
	      [list $vinfo(initial)] \
	      [list $vinfo(fixed)] \
	      [list $vinfo(mean)] \
	      [list $vinfo(sigma)] \
	      [list $vinfo(inf)] \
	      [list $vinfo(sup)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }
	  
      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
              [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    if {$cur_item != 0} {
      $tree activate $cur_item
      $tree selection add $cur_item
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveLast {} {
  #/////////////////////////////////////////////////////////////////////////////

    $tree selection clear
    $tree activate last
    $tree selection add last
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }

}  


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsparameter {
# PURPOSE : Defines the snit widget used to
#           create new Parameters (only Missing) or editing existing ones
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # The container to wich the parameter belongs
  option -container \
    -default "" -configuremethod "_conf-container"  

  # Identifier of the specific parameter to treat
  option -item \
    -default "" -configuremethod "_conf-item"  

  # Who did call me? (tree (MMS tree), list (Parameters List))
  option -parent \
    -default "tree" -configuremethod "_conf-parent"  

  # Current state (View, Edit, New, Copy)
  option -state \
    -default "Details" -configuremethod "_conf-state"  
	
  variable label_state

  variable widgets
    
  variable parameter_info
  # Parameter data to be edited
   # (Class @Parameter)            - (: @MElement)
    # parameter_info(name)         - Parameter name (Text _.name)
    # parameter_info(initial)      - Initial value (Real _.initialValue)
    # parameter_info(fixed)        - Is fixed? (Real _.isFixed)
    # parameter_info(g_active)     - Global active (IsActive method)
    # parameter_info(prior)        - Prior {Mean Sigma IsActive} (H: Set _.prior)
	# parameter_info(constraint)   - Constraint {InferiorValue SuperiorValue IsActive} (H: Set _.constraint)
    # parameter_info(type)         - Type (ARIMA, Linear, NonLinear, Hyper, Sigma2, Missing)
     # (Class @ParameterARIMA)
	  #                            - (Set _parent_ // @ARIMABlock)
	  # parameter_info(ptype)      - (Text _.type)
	  # parameter_info(degree)     - (Real _.degree)
     # (Class @ParameterLinear)
	  #                            - (Set _parent_ //@ExpTerm)
	  # parameter_info(degree)     - (Real _.degree)
     # (Class @ParameterNonLinear)
	  #                            - (Set _parent_ //@ExpTerm)
	  # parameter_info(ptype)      - (Text _.type)
	  # parameter_info(degree)     - (Real _.degree)
     # (Class @ParameterHyper)
	  #                            - (Set _parent_ //@HierarchyTerm)
     # (Class @ParameterSigma2)
	  #                            - (Set _parent_ //@Noise)
     # (Class @ParameterMissing)
	  #                            - (Set _parent_ //@MVariable)
	  #                            - (Anything _.position)
      # parameter_info(serpos)     - Position Date for Serie
      # parameter_info(rowpos)     - Position Row for Matrix
      # parameter_info(colpos)     - Position Column for Matrix
      # parameter_info(grammar)    - Grammar of _parent_ (Serie, Matrix)

  variable priorvalues
  variable constraintvalues
  
  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-item { _ item } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-item) $item
    if {$item eq ""} {
	  return
	}
    $self Details
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent { _ parent } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-parent) $parent 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ state } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $state
    switch -- $state {
      "View"  {$self Details}
      "Edit"  {$self Edit}
      "New"   {$self New}
      "Copy"  {$self Copy}
    }
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates a form to edit a Parameter
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
  
    # Buttons: New, Edit, Copy
    set fbu [frame $f.fbu]
  
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc New] \
      -helptext [mc "New Parameter"] -padx 1 -relief link \
      -compound left -command [list $self New] \
      -state normal
    set widgets(new) $fbu.bNew
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit Parameter"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    Button $fbu.bCopy -image [::Bitmap::get copy] -text [mc "Duplicate"] \
      -helptext "[mc "Duplicate"] [mc "Parameter"]" -padx 1 -relief link \
      -compound left -command [list $self Copy] \
      -state disabled
    set widgets(copy) $fbu.bCopy

    grid $fbu.bNew $fbu.bEdit $fbu.bCopy -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 3 -weight 1

    foreach {w} {new copy} {
      grid remove $widgets($w)
    }

    # Labels and Entries
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]
  
    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	  
    label $fe.lName -text "[mc "Name"]:" -pady 5 -padx 5
    label $fe.lType -text "[mc "Type"]:" -pady 5 -padx 5
    label $fe.lActive -text "[mc "Active"]:" -pady 5 -padx 5
    label $fe.lInit -text "[mc "Initial Value"]:" -pady 5 -padx 5
    label $fe.lDegr -text "[mc "Degree"]:" -pady 5 -padx 5
    label $fe.lPtyp -text "[mc "Type"]:" -pady 5 -padx 5
    label $fe.lPos -text "[mc "Position"]:" -pady 5 -padx 5
    set widgets(ldegree) $fe.lDegr
    set widgets(lptype) $fe.lPtyp
    set widgets(lpos) $fe.lPos

    entry $fe.eName -textvariable [myvar parameter_info(name)] \
      -width 40 -state readonly
    set widgets(name) $fe.eName
    
    entry $fe.eType -textvariable [myvar parameter_info(type)] \
      -width 10 -state readonly
    set widgets(type) $fe.cType

    checkbutton $fe.chkbActive -variable [myvar parameter_info(g_active)] \
      -text [mc "Global"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(g_active) $fe.chkbActive

    entry $fe.eInit -textvariable [myvar parameter_info(initial)] \
      -width 10 -state readonly
    set widgets(initial) $fe.eInit

    checkbutton $fe.chkbFixed -variable [myvar parameter_info(fixed)] \
      -text [mc "Fixed"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(fixed) $fe.chkbFixed

    entry $fe.eDegr -textvariable [myvar parameter_info(degree)] \
      -width 10 -state readonly
    set widgets(degree) $fe.eDegr

    entry $fe.ePtyp -textvariable [myvar parameter_info(ptype)] \
      -width 10 -state readonly
    set widgets(ptype) $fe.ePtyp

    # Position
    set fPosSerie [frame $fe.fPosSerie]
    set widgets(fposserie) $fPosSerie
    
    label $fPosSerie.lDate -text "[mc "Date"]:" -padx 5

    ::datefield::datefield $fPosSerie.df \
	  -textvariable [myvar parameter_info(serpos)] \
      -state disabled -format y/m/d
    set widgets(serpos) $fPosSerie.df
      
    grid $fPosSerie.lDate $fPosSerie.df -sticky e
    grid rowconfigure    $fPosSerie 0 -weight 1
    grid columnconfigure $fPosSerie 0 -weight 1

    set fPosMatrix [frame $fe.fPosMatrix]
    set widgets(fposmatrix) $fPosMatrix
    
    label $fPosMatrix.lRow -text "[mc "Row"]:" -padx 5
    label $fPosMatrix.lCol -text "[mc "Column"]:" -padx 5

    entry $fPosMatrix.eRow -textvariable [myvar parameter_info(rowpos)] \
      -width 3 -state readonly
    set widgets(rowpos) $fPosMatrix.eRow
      
    entry $fPosMatrix.eCol -textvariable [myvar parameter_info(colpos)] \
      -width 3 -state readonly
    set widgets(colpos) $fPosMatrix.eCol

    grid $fPosMatrix.lRow $fPosMatrix.eRow $fPosMatrix.lCol $fPosMatrix.eCol -sticky e
    grid rowconfigure    $fPosMatrix 0 -weight 1
    grid columnconfigure $fPosMatrix 0 -weight 1

    grid $fe.lName      -row 0 -column 0 -sticky e
    grid $fe.eName      -row 0 -column 1 -sticky w
    grid $fe.lType      -row 1 -column 0 -sticky e
    grid $fe.eType      -row 1 -column 1 -sticky w
    grid $fe.lActive    -row 2 -column 0 -sticky e
    grid $fe.chkbActive -row 2 -column 1 -sticky w
    grid $fe.lInit      -row 3 -column 0 -sticky e
    grid $fe.eInit      -row 3 -column 1 -sticky w
    grid $fe.chkbFixed  -row 4 -column 1 -sticky w
    grid $fe.lDegr      -row 5 -column 0 -sticky e
    grid $fe.eDegr      -row 5 -column 1 -sticky w
    grid remove $fe.lDegr
    grid remove $fe.eDegr
    grid $fe.lPtyp      -row 6 -column 0 -sticky e
    grid $fe.ePtyp      -row 6 -column 1 -sticky w
    grid remove $fe.lPtyp
    grid remove $fe.ePtyp
    grid $fe.lPos       -row 5 -column 0 -sticky e
    grid $fe.fPosMatrix -row 5 -column 1 -sticky w
    grid remove $fe.fPosMatrix
    grid $fe.fPosSerie  -row 5 -column 1 -sticky w
    grid remove $fe.fPosSerie

    # Prior
    label $fe.lPrior -text [mc "Prior"] -pady 5 -padx 5
    set fprior [labelframe $fe.fPrior \
      -labelwidget $fe.lPrior -relief solid -bd 1]
    set widgets(prior) $fe.fPrior
    
	::MDMPriorsGui::bmmspriorvalues $fprior.fPrior \
	  -state readonly
    set priorvalues $fprior.fPrior
      
    bind $priorvalues <<OnAccept>> "$self Ok"
    bind $priorvalues <<OnCancel>> "$self Cancel"

    grid rowconfigure $fprior 0 -weight 1
    grid columnconfigure $fprior 0 -weight 1
	
    grid $fprior -row 7 -column 0 -columnspan 2 \
	             -sticky news -pady 10 -padx 10

    # Constraint
    label $fe.lCnst -text [mc "Constraint"] -pady 5 -padx 5
    set fcnst [labelframe $fe.fCnst \
      -labelwidget $fe.lCnst -relief solid -bd 1]
    set widgets(constraint) $fe.fCnst

	::MDMConstraintsGui::bmmsconstraintvalues $fcnst.fCnst \
	  -state readonly
    set constraintvalues $fcnst.fCnst
      
    bind $constraintvalues <<OnAccept>> "$self Ok"
    bind $constraintvalues <<OnCancel>> "$self Cancel"

    grid rowconfigure    $fcnst 0 -weight 1
    grid columnconfigure $fcnst 0 -weight 1

    grid $fcnst -row 8 -column 0 -columnspan 2 \
	            -sticky news -pady 10 -padx 10

    grid rowconfigure    $fe 9 -weight 1
    grid columnconfigure $fe 2 -weight 1
  
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1

    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1
    
    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    foreach {w} {initial fixed \
	             serpos rowpos colpos} {
      bind $widgets($w) <Return> "$self Ok"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {accept cancel \
	             prior constraint} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {initial fixed \
	             prior constraint \
                 accept cancel \
				 degree ptype \
				 serpos rowpos colpos} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $widgets(initial) <Shift-Tab> "focus $f.fbd.bCancel ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $f.fbd.bAccept ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method UpdateTypeFields {_type} {
  #/////////////////////////////////////////////////////////////////////////////

    if {$_type ne "Missing"} {
      foreach {w} {lpos fposserie fposmatrix} {
        grid remove $widgets($w)
      }
    }

    if {$_type eq "Missing" || $_type eq "Hyper" || $_type eq "Sigma2"} {
      foreach {w} {degree ldegree ptype lptype} {
        grid remove $widgets($w)
      }
    } else {
      switch -- $_type {
        "ARIMA" {
          foreach {w} {degree ldegree ptype lptype} {
            grid $widgets($w)
          }
        }
        "Linear"  {
          foreach {w} {ptype lptype} {
            grid remove $widgets($w)
          }
          foreach {w} {degree ldegree} {
            grid $widgets($w)
          }
        }
        "NonLinear"  {
          foreach {w} {degree ldegree ptype lptype} {
            grid $widgets($w)
          }
        }
      }
    }
	
    if {$_type eq "Missing"} {
      grid $widgets(lpos)
      if {$parameter_info(grammar) == "Serie"} {
        grid remove $widgets(fposmatrix)
        grid $widgets(fposserie)
      } elseif {$parameter_info(grammar) == "Matrix"} { 
        grid remove $widgets(fposserie)
        grid $widgets(fposmatrix)
      }
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetPriorValues {} {
  #/////////////////////////////////////////////////////////////////////////////

    set parameter_info(prior) [$priorvalues get_info ""]
    #puts "GetPriorValues: parameter_info(prior)=$parameter_info(prior)"
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetPriorValues {} {
  #/////////////////////////////////////////////////////////////////////////////

    $priorvalues set_info $parameter_info(prior)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetConstraintValues {} {
  #/////////////////////////////////////////////////////////////////////////////

    set parameter_info(constraint) [$constraintvalues get_info ""]
    #puts "GetPriorValues: parameter_info(constraint)=$parameter_info(constraint)"
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetConstraintValues {} {
  #/////////////////////////////////////////////////////////////////////////////

    $constraintvalues set_info $parameter_info(constraint)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set parameter_info(name) ""
    set parameter_info(type) "Missing"
    set parameter_info(g_active) 0
    set parameter_info(initial) "?"
    set parameter_info(fixed) 0
	
    set parameter_info(prior) {"" "" 0}
    $self SetPriorValues
	
    set parameter_info(constraint) {"" "" 0}
    $self SetConstraintValues

    set parameter_info(serpos) "2000/01/01"
    set parameter_info(rowpos) "1"
    set parameter_info(colpos) "1"

    set mvariable [lindex $options(-container) end-1]
	set parameter_info(grammar) [LayerMDMGui::GetMVariableGrammar \
      $mvariable $options(-container)]
	  
	$self UpdateTypeFields $parameter_info(type)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set ident [$self cget -item]

	array set parameter_info [LayerMDMGui::GetParameter $ident \
      $options(-container)]
    #puts "GetInfo:parameter_info=[array get parameter_info]"
	
	$self UpdateTypeFields $parameter_info(type)
	
    set _priorvalues [list]
    foreach { {} p } $parameter_info(prior) {
      lappend _priorvalues $p
    }
    set parameter_info(prior) $_priorvalues
	$self SetPriorValues
	
    set _constraintvalues [list]
    foreach { {} p } $parameter_info(constraint) {
      lappend _constraintvalues $p
    }
    set parameter_info(constraint) $_constraintvalues
	$self SetConstraintValues
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "View"
    set label_state [mc "Details of the Parameter"]
    
    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}

	if {$parameter_info(type) eq "Missing"} {
      foreach {w} {new copy} {
        grid $widgets($w)
        $widgets($w) configure -state $_state
      }
	} else {
      foreach {w} {new copy} {
        grid remove $widgets($w)
      }
	}
    $widgets(edit) configure -state $_state
	
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {initial rowpos colpos} {
      $widgets($w) configure -state readonly
    }
    foreach {w} {fixed serpos} {
      $widgets($w) configure -state disabled
    }
    $priorvalues configure -state readonly
    $constraintvalues configure -state readonly

    $self GetInfo
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Edit"
    set label_state [mc "Edit Parameter"]
    ::MMSGui::ActivateEdition $win

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {initial fixed} {
      $widgets($w) configure -state normal
    }
    $priorvalues configure -state normal
    $constraintvalues configure -state normal

    focus $widgets(initial)
    bind $widgets(cancel) <Tab> "focus $widgets(initial) ; break"
    bind $widgets(initial) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Create {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {initial fixed \
	             serpos rowpos colpos} {
      $widgets($w) configure -state normal
    }
    $priorvalues configure -state normal
    $constraintvalues configure -state normal

    focus $widgets(initial)
    bind $widgets(cancel) <Tab> "focus $widgets(initial) ; break"
    bind $widgets(initial) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method New {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "New"
    set label_state [mc "New Parameter"]
    ::MMSGui::ActivateEdition $win

    $self ClearInfo
    
    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Copy {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Copy"
    set label_state "[mc "Duplicate"] [mc "Parameter"]"
    ::MMSGui::ActivateEdition $win

    $self Create
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
    $self GetPriorValues
    $self GetConstraintValues
	
	set parameter_info(container) [$self cget -container]

    if {$options(-state) eq "Edit"} {
      LayerMDMGui::EditParameter parameter_info $parameter_info(container) \
	    $parameter_info(prior) $parameter_info(constraint)
      
    } else {                                 ;# New, Copy
      LayerMDMGui::CreateParameter parameter_info $parameter_info(container) \
	    $parameter_info(prior) $parameter_info(constraint)
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }
    }
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Ok>>
	}
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Cancel>>
	}
  }

}

}
