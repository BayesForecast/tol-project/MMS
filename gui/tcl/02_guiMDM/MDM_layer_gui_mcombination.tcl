#/////////////////////////////////////////////////////////////////////////////
# FILE:    MDM_layer_gui_mcombination.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MDM (Mcombinations)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerMDMGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetMCombinationTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "MCombination 1"
    desc "description of MCombination 1"
    active "1"
    terms {}
  }
}
  
variable get_combination_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetMCombination {ident container} {
#///////////////////////////////////////////////////////////////////////////
  variable get_combination_test
  
  if { $get_combination_test } {
    return [GetMCombinationTest]
  }
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetMCombination" \
    "Set" $ident_text $container_set
}

#/////////////////////////////////////////////////////////////////////////////
proc GetMCombinationsListTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      ident "MCombination 1"
    }
    {}
    {
      ident "MCombination 2"
    }
  }
}

variable combinations_list_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetMCombinationsList {container details} {
#/////////////////////////////////////////////////////////////////////////////
  variable combinations_list_test
  
  if { $combinations_list_test } {
    return [GetMCombinationsListTest]
  }
  set container_set [TclLst2TolSet $container]
  set details_text \"[LayerMMSGui::TolText $details]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetMCombinationsList" \
    "Set" $container_set $details_text
}

#///////////////////////////////////////////////////////////////////////////
proc GetMCombinationHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::GetMCombinationHelp" \
    "Text" "?"
}

variable create_combination_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateMCombination {combination_info terms container prior constraint} {
#///////////////////////////////////////////////////////////////////////////
  variable create_combination_test
  upvar $combination_info combination_array

  set terms_set [TclLst2TolSet $terms -level 2]
  #puts "CreateCombination:terms_set=$terms_set"
  
  set container_set [TclLst2TolSet $container]

  set prior_set [TclLst2TolSet $prior]
  set constraint_set [TclLst2TolSet $constraint]
  
  if { $create_combination_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateMCombinationTest" \
      "Real" combination_array $terms_set $container_set $prior_set $constraint_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::CreateMCombination" \
      "Real" combination_array $terms_set $container_set $prior_set $constraint_set
  }
}

variable edit_combination_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditMCombination {combination_info container prior constraint} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_combination_test
  upvar $combination_info combination_array
  
  set container_set [TclLst2TolSet $container]

  set prior_set [TclLst2TolSet $prior]
  set constraint_set [TclLst2TolSet $constraint]
  
  if { $edit_combination_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditMCombinationTest" \
      "Real" combination_array $container_set $prior_set $constraint_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::MDMGui::EditMCombination" \
      "Real" combination_array $container_set $prior_set $constraint_set
  }
}

#///////////////////////////////////////////////////////////////////////////
proc RemoveMCombination {ident container} {
#///////////////////////////////////////////////////////////////////////////
  
  set container_set [TclLst2TolSet $container]
  set ident_text \"[LayerMMSGui::TolText $ident]\"
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::MDMGui::RemoveMCombination" \
    "Real" $ident_text $container_set
}

}
