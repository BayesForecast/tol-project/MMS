#/////////////////////////////////////////////////////////////////////////////
# FILE    : MDM_hierarchy_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with the hierarchies of a model (MDM layer)
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit
package require Tablelist
package require autoscroll 1.1

namespace eval ::MDMHierarchiesGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateHierarchiesListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  
  #puts "CreateHierarchiesListDetails"
  set l_details_frame [frame $f.details_Hierarchies]

  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "Hierarchy" \
    -swlist "::MDMHierarchiesGui::bmmshierlist" \
	-fshowitem "::MDMHierarchiesGui::_ShowHierarchyDetails" \
	-fshowlist "::MDMHierarchiesGui::_ShowHierarchiesListDetails" \
  ]

  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowHierarchiesListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreateHierarchiesListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details configure -container $container
  $l_details_frame.details Init

  return $l_details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowHierarchiesListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set model [$tree item parent $id]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name
  
  _ShowHierarchyHelp  

  _ShowHierarchiesListDetails $container
}

#/////////////////////////////////////////////////////////////////////////////
proc CMenuHierarchies {tree} {
#/////////////////////////////////////////////////////////////////////////////

  if {[::MMSGui::IsEditionActive]} {
    set _state "disabled"
  } else {
    set _state "normal"
  }
  $tree.cmenu delete 0 end
  $tree.cmenu add command -label [mc "New Hierarchy"] \
    -command "::MDMHierarchiesGui::NewHierarchy" \
	-state $_state
}

#/////////////////////////////////////////////////////////////////////////////
proc NewHierarchy {} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details Details "New"
}

#/////////////////////////////////////////////////////////////////////////////
proc ChangeContainerTree {new_ident} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  $l_details_frame.details ChangeActiveItem $new_ident
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowHierarchyHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerMDMGui::GetHierarchyHelp]

  ::MMSGui::ShowInfo $message
}
#/////////////////////////////////////////////////////////////////////////////
proc CreateHierarchyDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreateHierarchyDetails"
  set details_frame [frame $f.details_Hierarchy]
  set view_frame [frame $f.view_Hierarchy]

  set _details [bmmshierarchy $details_frame.details]
  set _view [bmmshierarchy $view_frame.details]

  bind $_details <<Insert>> \
    "event generate $f <<Insert>>"

  bind $_details <<Refresh>> \
    "event generate $f <<Refresh>>"

  bind $_details <<Ok>> \
    "event generate $details_frame <<Ok>>"
  
  bind $_details <<Cancel>> \
    "event generate $details_frame <<Cancel>>"
  
  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowHierarchyDetails {container ident state parent} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateHierarchyDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -container $container
  $_frame.details configure -item $ident
  $_frame.details configure -state $state
  $_frame.details configure -parent $parent
  
  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowHierarchyDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set model [$tree item parent $parent]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name

  set ident [$tree item text $id first]

  _ShowHierarchyDetails $container $ident "View" "tree"
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandHierarchiesList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set model [$tree item parent $id]
  set container [::MDMGui::GetModelContainer $tree $model]
  set model_name [$tree item text $model first]
  lappend container $model_name
  set _details "no" 
	
  set _list [LayerMDMGui::GetHierarchiesList $container $_details]
  #puts "ExpandHierarchies:_list=$_list"

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  foreach {{} it} $_list {
    array set vinfo $it
    set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
    set row [list [list $icon $vinfo(ident)] \
	          [list "Hierarchy"] \
	          [list "MDMHierarchiesGui::ExpandHierarchy"] \
			  [list "MDMHierarchiesGui::ShowHierarchyDetails"] \
	          [list ""] \
			  [list $vinfo(abs_id)] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandHierarchy {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  set _list [list \
	"Hierarchy Terms"       "MDMHierTermsGui::ExpandHierTermsList" \
        	                "MDMHierTermsGui::ShowHierTermsListDetails" \
        	                "MDMHierTermsGui::CMenuHierTerms" \
	"Parameters"            "MDMParametersGui::ExpandParametersList" \
        	                "MDMParametersGui::ShowParametersListDetails" \
        	                "" \
  ]
  
  foreach {name fexpand fdetails fcmenu} $_list {
    set row [list [list [::Bitmap::get "Set"] [mc $name]] \
			  [list $name] \
	          [list $fexpand] \
			  [list $fdetails] \
			  [list $fcmenu] \
			  [list ""] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

 
#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmshierlist {
# PURPOSE : Defines the snit widget used to
#           list the hierarchies of a model
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the model
  option -container \
    -default "" -configuremethod "_conf-container"  

  option -details \
    -default "no" -configuremethod "_conf-details"  

  variable mdm_hierarchylist
    # mdm_hierarchylist(list)  - Hierarchies list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
    $self FillList
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the hierarchies of a model
  #///////////////////////////////////////////////////////////////////////////
  
    set f $dlg
 
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Hierarchies List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set ft [frame $f.ft]
    
	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Name"]] \
        [list text -label [mc "Child Parameters"]] \
        [list text -label [mc "Hyperparameters"]] \
        [list text -label [mc "Description"]] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns

    #$tree configure -contextmenu [$self CreateCMenu]
	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    focus $tree

    set mdm_hierarchylist(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mdm_hierarchylist(list) [LayerMDMGui::GetHierarchiesList \
      $options(-container) $options(-details)]
    #puts "FillList mdm_hierarchylist(list)=$mdm_hierarchylist(list)"

    $tree item delete all

    foreach {{} it} $mdm_hierarchylist(list) {
      array set vinfo $it
      set icon [LayerMMSGui::GetIcon $vinfo(abs_id)]
	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(name)] \
	      [list $vinfo(nelems)] \
	      [list $vinfo(nterms)] \
	      [list $vinfo(desc)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }
	  
      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
              [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    if {$cur_item != 0} {
      $tree activate $cur_item
      $tree selection add $cur_item
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveLast {} {
  #/////////////////////////////////////////////////////////////////////////////

    $tree selection clear
    $tree activate last
    $tree selection add last
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }

}  


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmselements {
# PURPOSE : Defines the snit widget used to
#           create a list of elements names (Child Parameters)
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  variable widgets
  
  # State (disabled, normal)
  option -state \
    -default "disabled" -configuremethod "_conf-state" 

  # Identifier of the model to wich the hierarchy belongs
  option -model \
    -default "" -configuremethod "_conf-model"  

  variable mdm_elemlist
    # mdm_elemlist(list)         - Elements list (Child Parameters)
    # mdm_elemlist(elem)         - Type of element (Parameter or Combination)
    # mdm_elemlist(left_list)    - Elements left list
    #                            -  (Name,Type for parameters)
    #                            -	(Name,Terms for combinations)
    # mdm_elemlist(right_list)   - Elements right list (Name,Element)
	#                            -  (Element is Parameter or Combination)

  variable swaptablelist_options
  
  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Initialize the variable information
    $self _init
    
    # Paint the window
    $self _create
    
    # Apply all options passed at creation time.
    $self configurelist $args
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-model { _ mod } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-model) $mod 
    $self FillList
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method put_state {s} {
  #///////////////////////////////////////////////////////////////////////////

    set tbl [Swaptablelist::getLTable $widgets(tree)]
    $tbl configure -state $s
    set tbl [Swaptablelist::getRTable $widgets(tree)]
    $tbl configure -state $s
	$widgets(tree).lr.left configure -state $s
	$widgets(tree).lr.right configure -state $s
	
    foreach {w} {bref \
	             elem_par elem_comb} {
      $widgets($w) configure -state $s
    }
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ s } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $s
	$self put_state $s
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #
  # PURPOSE: Initializes the options for the swaptablelist
  #///////////////////////////////////////////////////////////////////////////
 
    set mdm_elemlist(right_list) [list]
	
    set swaptablelist_options(-llabel) [mc "Child Parameters"]
    set swaptablelist_options(-rlabel) [mc "Selected Child Parameters"]
    set swaptablelist_options(-lcolumns) [list \
      [list 0 [mc "Name"] left] \
      [list 0 [mc "Type"] left] \
      [list 0 "Elem" left] \
    ]                         
    set swaptablelist_options(-rcolumns) [list \
      [list 0 [mc "Name"] left] \
      [list 0 [mc "Element"] left] \
    ]
	set swaptablelist_options(-keycolumns) [list \
	  col_name \
    ]
	
    set swaptablelist_options(-height) 15
    set swaptablelist_options(-lwidth) 41
    set swaptablelist_options(-rwidth) 41
    set swaptablelist_options(-llistvar) [list]
    set swaptablelist_options(-rlistvar) [myvar mdm_elemlist(right_list)]
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          select elements of a model (Child Parameters)
  #///////////////////////////////////////////////////////////////////////////

    set fs $dlg

    # radiobuttons for select the kind of element (Parameter or Combination)
    set fe [frame $fs.fe]
	
    label $fe.lElem -text "[mc "Element"]:" -pady 5 -padx 5

    set felem [frame $fe.fElem]
    foreach item {Parameter Combination} {
      radiobutton $felem.rbElem$item -text [mc $item] -value $item \
        -variable [myvar mdm_elemlist(elem)] \
        -state disabled \
        -command "$self UpdateElement"
      pack $felem.rbElem$item -side left
    }
    set widgets(elem_par) $felem.rbElemParameter
    set widgets(elem_comb) $felem.rbElemCombination

	grid $fe.lElem -row 0 -column 0 -sticky e
    grid $fe.fElem -row 0 -column 1 -sticky w
    grid rowconfigure    $fe 1 -weight 1
    grid columnconfigure $fe 2 -weight 1

    # Button: Refresh
    set fb [frame $fs.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Child Parameters List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
    set widgets(bref) $fb.bRef
	
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Swaptablelist frame
    set ft [frame $fs.ft]

    #puts "_create:swaptablelist_options=[array get swaptablelist_options]"
    set widgets(tree) [eval [list Swaptablelist::create_swap_list $ft] [array get swaptablelist_options]]
    #puts "_create:widgets(tree)=$widgets(tree)"    

	bind $widgets(tree) <<SwaptablelistUpdated>> \
	  "event generate $win <<OnChanged>>"
	
    set ltbl [Swaptablelist::getLTable $widgets(tree)]
    $ltbl columnconfigure 0 -name col_name -editable no -sortmode dictionary
    $ltbl columnconfigure 1 -name col_2 -editable no -sortmode dictionary
    $ltbl columnconfigure 2 -name col_elem -editable no -hide 1
	
    set rtbl [Swaptablelist::getRTable $widgets(tree)]
    $rtbl columnconfigure 0 -name col_name -editable no -labelcommand "$self DoNothing"
    $rtbl columnconfigure 1 -name col_elem -editable no -labelcommand "$self DoNothing"
    
    grid $fs.fe -sticky news
    grid $fs.fb -sticky news
    grid $fs.ft -sticky news

    grid rowconfigure $fs 2 -weight 1
    grid columnconfigure $fs 0 -weight 1
    grid $fs -sticky news

    foreach {w} {tree bref \
	             elem_par elem_comb} {
      bind $widgets($w) <Escape> "$self Cancel"
    }

    set mdm_elemlist(elem) "Parameter"
    #$self FillList
  }

  #/////////////////////////////////////////////////////////////////////////////
  method DoNothing {args} {
  #/////////////////////////////////////////////////////////////////////////////
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method UpdateElement {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set tbl [Swaptablelist::getLTable $widgets(tree)]
	
    if {$mdm_elemlist(elem)=="Parameter"} {
      $tbl columnconfigure col_2 -title [mc "Type"]
	  
    } else {                 ;# "Combination"
      $tbl columnconfigure col_2 -title [mc "Terms"]
    }
	
	$self FillList
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    if {$mdm_elemlist(elem)=="Parameter"} {
      set mdm_elemlist(list) [LayerMDMGui::GetParametersList \
        $options(-model) "no"]
		  
    } else {                 ;# "Combination"
      set mdm_elemlist(list) [LayerMDMGui::GetMCombinationsList \
        $options(-model) "no"]
    }
    #puts "FillList mdm_elemlist(list)=$mdm_elemlist(list)"

	set _state $options(-state)
	$self put_state "normal"

    set tbl [Swaptablelist::getLTable $widgets(tree)]
    $tbl delete 0 end

    set mdm_elemlist(left_list) [list]
    foreach {{} it} $mdm_elemlist(list) {
      array set vinfo $it
	  if {$mdm_elemlist(elem)=="Parameter"} {
	    set _col2 $vinfo(type)
	  } else {
	    set _col2 $vinfo(terms)
	  }
      set row [list $vinfo(ident) $_col2 $mdm_elemlist(elem)]
      lappend mdm_elemlist(left_list) $row
    }

    Swaptablelist::insert $tbl $mdm_elemlist(left_list)
      
    $self put_state $_state
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    $self FillList
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnAccept>>
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnCancel>>    
  }

  #/////////////////////////////////////////////////////////////////////////////
  method set_info {iteminfo} {
  #/////////////////////////////////////////////////////////////////////////////
 
    #puts "set_info:iteminfo=$iteminfo"
	set _state $options(-state)
	$self put_state "normal"

    set ltbl [Swaptablelist::getLTable $widgets(tree)]
    $ltbl selection clear 0 end

    set rtbl [Swaptablelist::getRTable $widgets(tree)]
    $rtbl delete 0 end

    Swaptablelist::insert $rtbl $iteminfo

    $self put_state $_state
  }

  #/////////////////////////////////////////////////////////////////////////////
  method get_info {what} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "get_info:mdm_elemlist(right_list)=$mdm_elemlist(right_list)"
    return $mdm_elemlist(right_list)
  }
  
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmshsigmasmatrix {
# PURPOSE : Defines the snit widget used to
#           create a hierarchy matrix
#           in order to edit it's relative sigmas
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  variable widgets

  # State (disabled, normal)
  option -state \
    -default "disabled" -configuremethod "_conf-state" 

  variable mdm_matrix
  # Matrix to be edited at the tablelist
   # Hierarchy matrix
    # Rows
     # one for each Element
    # Columns
     # one for Elements names (Child Parameters)
     # one for Sigmas
    
  variable tablelist_options
  
  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor

  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Initialize the variable information
    $self _init
    
    # Paint the window
    $self _create

    # Apply all options passed at creation time.
    $self configurelist $args
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ s } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $s
    $widgets(tbl) configure -state $s
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #
  # PURPOSE: Initializes the options for the tablelist
  #///////////////////////////////////////////////////////////////////////////
 
    set mdm_matrix [list]

    set tablelist_options(-label) [mc "Hierarchy"]
    set tablelist_options(-columns) [list \
      0 [mc "Child Parameters"] left \
      0 [mc "Sigmas"] right \
    ]
    set tablelist_options(-listvar) [myvar mdm_matrix]
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          to edit the relative sigmas of a hierarchy matrix
  #///////////////////////////////////////////////////////////////////////////
    set fs $dlg
      
    # Tablelist frame
    set ft [frame $fs.ft]

    tablelist::tablelist $ft.tbl \
      -columns $tablelist_options(-columns) \
      -listvariable $tablelist_options(-listvar) \
      -xscrollcommand "$ft.xs set" \
      -yscrollcommand "$ft.ys set" \
      -height 10 -width 93
    set widgets(tbl) $ft.tbl

	bind $widgets(tbl) <<TablelistCellUpdated>> \
	  "event generate $win <<OnChanged>>"
	
    scrollbar $ft.ys -orient v -command "$ft.tbl yview"
    scrollbar $ft.xs -orient h -command "$ft.tbl xview"

    grid $ft.tbl  -row 0 -column 0 -sticky nsew
    grid $ft.ys   -row 0 -column 1 -sticky ns
    grid $ft.xs   -row 2 -column 0 -sticky ew

    grid columnconfigure $ft 0 -weight 1
    grid rowconfigure $ft 0 -weight 1

    $widgets(tbl) columnconfigure 0 -name col_name -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 1 -name col_sigmas -editable yes -labelcommand "$self DoNothing" 
    
    grid $fs.ft -sticky news

    grid rowconfigure $fs 1 -weight 1
    grid columnconfigure $fs 0 -weight 1
    grid $fs -sticky news
    
    foreach {w} {tbl} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method DoNothing {args} {
  #/////////////////////////////////////////////////////////////////////////////
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnAccept>>
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnCancel>>    
  }

  #/////////////////////////////////////////////////////////////////////////////
  method set_info {iteminfo} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "set_info:iteminfo=$iteminfo"
	set _state $options(-state)
    $widgets(tbl) configure -state normal

    $widgets(tbl) delete 0 end

    foreach {r} $iteminfo {
      $widgets(tbl) insert end $r
    }
    
	$widgets(tbl) configure -state $_state
  }

  #/////////////////////////////////////////////////////////////////////////////
  method get_info {what} {
  #/////////////////////////////////////////////////////////////////////////////

    if {$what ne "resume"} {
      return $mdm_matrix
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method finishediting {} {
  #/////////////////////////////////////////////////////////////////////////////

    $widgets(tbl) finishediting
  }

}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmshierarchy {
# PURPOSE : Defines the snit widget used to
#           create new hierarchies or editing existing ones
#/////////////////////////////////////////////////////////////////////////////

  # The container to wich the hierarchy belongs
  option -container \
    -default "" -configuremethod "_conf-container"  

  # Name of the specific hierarchy to treat
  option -item \
    -default "" -configuremethod "_conf-item"  

  # Who did call me? (tree (MMS tree), list (Variables List))
  option -parent \
    -default "tree" -configuremethod "_conf-parent"  

  # Current state (View, Edit, New, Copy)
  option -state \
    -default "Details" -configuremethod "_conf-state"  
	
  variable label_state

  variable widgets
    
  variable hierarchy_info
  # Hierarchy data to be edited
   # (Class @Hierarchy)               - (: @MNode)
    # hierarchy_info(name)            - Hierarchy name (Text _.name)
    # hierarchy_info(desc)            - Description (Text _.description)
    # hierarchy_info(g_active)        - Global active (IsActive method)
    # hierarchy_info(l_active)        - Local active (H: Real _.isActive)
    #                                 - (Set _.mElements_) // @MElement
	 # (Class @MCombination or @Parameter)
      #                               - (Text _.name)
     # hierarchy_info(elems)          - List of list of the hierarchy elements (Child Parameters)
	 #                                - Each list have the name and the type of the element
    #                                 - (Set _.hierarchyTerms) // @HierarchyTerm
    #                                 - (H: Set _.noise)
     # (Class @Noise)
      #                               - (Set _.sigma2 //@ParameterSigma2)
       # hierarchy_info(sigma0)       - Initial Sigma2 value (_.sigma //Real _.initialValue)
       # hierarchy_info(sigmaw)       - Sigma weight, =1->Is Fixed (_.sigmaFixed //Real _.isFixed)
      # (Class @NoiseNormal)
       # hierarchy_info(sigmas)       - (Set _.relativeSigmas)
        # hierarchy_info(matrix)      - Hierarchy matrix of Elements and Relative Sigmas
        #                               One element for each row of the Hierarchy matrix
    #                                 - (H: Set _parent_; //@Model)
	
  variable elems
  variable matrix

  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-item { _ item } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-item) $item
    if {$item eq ""} {
	  return
	}
    $self Details
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-parent { _ parent } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-parent) $parent 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ state } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $state
    switch -- $state {
      "View"  {$self Details}
      "Edit"  {$self Edit}
      "New"   {$self New}
      "Copy"  {$self Copy}
    }
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates a form to edit a Hierarchy
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
  
    # Buttons: New, Edit, Copy
    set fbu [frame $f.fbu]
  
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc New] \
      -helptext [mc "New Hierarchy"] -padx 1 -relief link \
      -compound left -command [list $self New] \
      -state normal
    set widgets(new) $fbu.bNew
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit Hierarchy"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    Button $fbu.bCopy -image [::Bitmap::get copy] -text [mc "Duplicate"] \
      -helptext "[mc "Duplicate"] [mc "Hierarchy"]" -padx 1 -relief link \
      -compound left -command [list $self Copy] \
      -state disabled
    set widgets(copy) $fbu.bCopy

    grid $fbu.bNew $fbu.bEdit $fbu.bCopy -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 3 -weight 1

    # Labels and Entries
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]
  
    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	  
    label $fe.lName -text "[mc "Name"]:" -pady 5 -padx 5
    label $fe.lDesc -text "[mc "Description"]:" -pady 5 -padx 5
    label $fe.lActive -text "[mc "Active"]:" -pady 5 -padx 5
    label $fe.lElem -text "[mc "Child Parameters"]:" -pady 5 -padx 5
    label $fe.lMatrix -text "[mc "Sigmas"]:" -pady 5 -padx 5

    entry $fe.eName -textvariable [myvar hierarchy_info(name)] \
      -width 40 -state readonly
    set widgets(name) $fe.eName
    
    entry $fe.eDesc -textvariable [myvar hierarchy_info(desc)] \
      -width 60 -state readonly
    set widgets(desc) $fe.eDesc

    set factive [frame $fe.fActive]

    checkbutton $factive.chkbLActive -variable [myvar hierarchy_info(l_active)] \
      -text [mc "Local"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(l_active) $factive.chkbLActive

    checkbutton $factive.chkbGActive -variable [myvar hierarchy_info(g_active)] \
      -text [mc "Global"] -onvalue 1 -offvalue 0 -state disabled
    set widgets(g_active) $factive.chkbGActive

    grid $factive.chkbLActive $factive.chkbGActive -sticky w -padx 2 -pady 2
    grid rowconfigure    $factive 0 -weight 1
    grid columnconfigure $factive 2 -weight 1

	CollapsableFrame $fe.cElems \
	  -text "" -width 700 -height 410
    set widgets(elems) $fe.cElems
	set ft [$widgets(elems) getframe]
	::MDMHierarchiesGui::bmmselements $ft.fElems \
	  -state disabled
	  #-model $hierarchy_info(model)
    set elems $ft.fElems
	place $elems -x 5 -y 15
      
    bind $elems <<OnCancel>> "$self Cancel"
    bind $elems <<OnChanged>> "$self UpdateElements"
	
    label $fe.lHError -text [mc "Parameter of Sigma2"] -pady 5 -padx 5
    set ferror [labelframe $fe.ferror \
      -labelwidget $fe.lHError -relief solid -bd 1]
	  
    label $ferror.lSigma0 -text "[mc "Initial Value"]:" -pady 5 -padx 5
    label $ferror.lSigmaW -text "[mc "Weight"]:" -pady 5 -padx 5
	
    entry $ferror.eSigma0 -textvariable [myvar hierarchy_info(sigma0)] \
      -width 10 -state readonly
    set widgets(sigma0) $ferror.eSigma0
	
    entry $ferror.eSigmaW -textvariable [myvar hierarchy_info(sigmaw)] \
      -width 10 -state readonly
    set widgets(sigmaw) $ferror.eSigmaW
	
    grid $ferror.lSigma0 -row 0 -column 0 -sticky e
    grid $ferror.eSigma0 -row 0 -column 1 -sticky w
    grid $ferror.lSigmaW -row 1 -column 0 -sticky e
    grid $ferror.eSigmaW -row 1 -column 1 -sticky w
	
    grid rowconfigure    $ferror 2 -weight 1
    grid columnconfigure $ferror 1 -weight 1
	
	CollapsableFrame $fe.cMatrix \
	  -text "" -width 700 -height 240
    set widgets(matrix) $fe.cMatrix
	set ft [$widgets(matrix) getframe]
	::MDMHierarchiesGui::bmmshsigmasmatrix $ft.fMatrix \
	  -state disabled
    set matrix $ft.fMatrix
	place $matrix -x 5 -y 15
      
    bind $matrix <<OnCancel>> "$self Cancel"
    bind $matrix <<OnChanged>> "$self UpdateMatrix"
    
    grid $fe.lName   -row 0 -column 0 -sticky e
    grid $fe.eName   -row 0 -column 1 -sticky w
    grid $fe.lDesc   -row 1 -column 0 -sticky e
    grid $fe.eDesc   -row 1 -column 1 -sticky w
    grid $fe.lActive -row 2 -column 0 -sticky e
    grid $fe.fActive -row 2 -column 1 -sticky w
    grid $fe.lElem   -row 3 -column 0 -sticky ne
    grid $fe.cElems  -row 3 -column 1 -sticky w
    grid $ferror     -row 4 -column 0 -columnspan 2 -sticky news -pady 10 -padx 10
    grid $fe.lMatrix -row 5 -column 0 -sticky ne
    grid $fe.cMatrix -row 5 -column 1 -sticky w

    grid rowconfigure    $fe 6 -weight 1
    grid columnconfigure $fe 2 -weight 1
  
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
	
    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1
    
    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    foreach {w} {name desc \
	             l_active \
                 sigma0 sigmaw} {
      bind $widgets($w) <Return> "$self Ok"
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {accept cancel \
                 elems matrix} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
    foreach {w} {name desc \
	             l_active \
                 elems matrix \
                 accept cancel \
                 sigma0 sigmaw} {
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $widgets(name) <Shift-Tab> "focus $f.fbd.bCancel ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $f.fbd.bAccept ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method UpdateElements {} {
  #/////////////////////////////////////////////////////////////////////////////

    $matrix finishediting

    set nelem [llength $hierarchy_info(elems)]
    $self GetElems
    
    #puts "UpdateElements:begin:hierarchy_info(matrix)=$hierarchy_info(matrix)"
    set mat_elems [list]
    for {set _r 0} {$_r < $nelem} {incr _r} {
      lappend mat_elems [lindex [lindex $hierarchy_info(matrix) $_r] 0]
    }
    #puts "UpdateElements:mat_elems=$mat_elems"
    set _elems [list]
    foreach e $hierarchy_info(elems) {
      lappend _elems [lindex $e 0]
    }
    #puts "UpdateElements:_elems=$_elems"

    set deleted [list]
    foreach {e} $mat_elems {
      set found [lsearch -exact $_elems $e]
      if {$found < 0} {
        lappend deleted $e
      }
    }
    #puts "UpdateElements:deleted=$deleted"
    if {$deleted != ""} {
      foreach {e} $deleted {
        set idx [lsearch -exact $mat_elems $e]
        set mat_elems [lreplace $mat_elems $idx $idx]
        #puts "UpdateElements:idx=$idx,mat_elems=$mat_elems"
        set hierarchy_info(sigmas) [lreplace $hierarchy_info(sigmas) $idx $idx]
      }
    }

    foreach {e} $_elems {
      set found [lsearch -exact $mat_elems $e]
      if {$found < 0} {
        lappend hierarchy_info(sigmas) 1
      }
    }
    
    $self BuildMatrix
    #puts "UpdateElements:end:hierarchy_info(matrix)=$hierarchy_info(matrix)"
    $self SetMatrix
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method UpdateMatrix {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    $self GetMatrix
    
    set nelem [llength $hierarchy_info(elems)]
    set hierarchy_info(sigmas) [list]
    for {set _r 0} {$_r < $nelem} {incr _r} {
      lappend hierarchy_info(sigmas) [lindex [lindex $hierarchy_info(matrix) $_r] 1]
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method BuildMatrix {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set _matrix [list]
    set nelem [llength $hierarchy_info(elems)]
    for {set _r 0} {$_r < $nelem} {incr _r} {
      set _row [list [lindex [lindex $hierarchy_info(elems) $_r] 0] \
	                 [lindex $hierarchy_info(sigmas) $_r]]
      lappend _matrix $_row
    }
    set hierarchy_info(matrix) $_matrix
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetElems {} {
  #/////////////////////////////////////////////////////////////////////////////

    set hierarchy_info(elems) [$elems get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetElems {} {
  #/////////////////////////////////////////////////////////////////////////////

	$elems configure -model $options(-container)
    $elems set_info $hierarchy_info(elems)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetMatrix {} {
  #/////////////////////////////////////////////////////////////////////////////

    set hierarchy_info(matrix) [$matrix get_info ""]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method SetMatrix {} {
  #/////////////////////////////////////////////////////////////////////////////

    $matrix set_info $hierarchy_info(matrix)
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ClearInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
	
    set hierarchy_info(name) ""
    set hierarchy_info(desc) ""
    set hierarchy_info(l_active) 1
    set hierarchy_info(g_active) 0
    
    set hierarchy_info(elems) {}
    $self SetElems

    set hierarchy_info(sigma0) "?"
    set hierarchy_info(sigmaw) "0"

    set hierarchy_info(sigmas) {}
    $self BuildMatrix
    $self SetMatrix
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetInfo {} {
  #/////////////////////////////////////////////////////////////////////////////
    
    set ident [$self cget -item]

	array set hierarchy_info [LayerMDMGui::GetHierarchy $ident \
        $options(-container)]
    #puts "GetInfo:hierarchy_info=[array get hierarchy_info]"
      
    set _elems [list]
    foreach { {} r } $hierarchy_info(elems) {
      set _row [list]
      foreach { {} t } $r {
        lappend _row $t
      }
      lappend _elems $_row
    }
    set hierarchy_info(elems) $_elems
    #puts "GetInfo:hierarchy_info(elems)=$hierarchy_info(elems)"
      
    set _sigmas [list]
    foreach { {} t } $hierarchy_info(sigmas) {
      lappend _sigmas $t
    }
    set hierarchy_info(sigmas) $_sigmas
    #puts "GetInfo:hierarchy_info(sigmas)=$hierarchy_info(sigmas)"
      
    $self BuildMatrix
    #puts "GetInfo:hierarchy_info(matrix)=$hierarchy_info(matrix)"

    $self SetElems
    $self SetMatrix
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////
	
    set options(-state) "View"
    set label_state [mc "Details of the Hierarchy"]
    
    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    foreach {w} {new edit copy} {
      $widgets($w) configure -state $_state
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {name desc \
	             sigma0 sigmaw} {
      $widgets($w) configure -state readonly
    }
    foreach {w} {l_active} {
      $widgets($w) configure -state disabled
    }
    $elems configure -state disabled
    $matrix configure -state disabled
    
    $self GetInfo
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Edit"
    set label_state [mc "Edit Hierarchy"]
    ::MMSGui::ActivateEdition $win

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name desc \
	             l_active \
                 sigma0 sigmaw} {
      $widgets($w) configure -state normal
    }
    $elems configure -state normal
    $matrix configure -state normal

    focus $widgets(name)
    bind $widgets(cancel) <Tab> "focus $widgets(name) ; break"
    bind $widgets(name) <Shift-Tab> "focus $widgets(cancel) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Create {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} {new edit copy} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
    foreach {w} {name desc \
	             l_active \
                 sigma0 sigmaw} {
      $widgets($w) configure -state normal
    }
    $elems configure -state normal
    $matrix configure -state normal
      
    focus $widgets(name)
    bind $widgets(cancel) <Tab> "focus $widgets(name) ; break"
    bind $widgets(l_active) <Shift-Tab> "focus $widgets(desc) ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method New {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "New"
    set label_state [mc "New Hierarchy"]
    ::MMSGui::ActivateEdition $win

    $self ClearInfo

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Copy {} {
  #/////////////////////////////////////////////////////////////////////////////

    set options(-state) "Copy"
    set label_state "[mc "Duplicate"] [mc "Hierarchy"]"
    ::MMSGui::ActivateEdition $win

    $self Create
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
    $matrix finishediting
	
	set hierarchy_info(container) [$self cget -container]

    if {$options(-state) eq "Edit"} {
	  set hierarchy_info(ident) [$self cget -item]
	  
      LayerMDMGui::EditHierarchy hierarchy_info \
		$hierarchy_info(elems) $hierarchy_info(sigmas) \
		$hierarchy_info(container)

	  set new_ident $hierarchy_info(name)
	  if {$hierarchy_info(ident) ne $new_ident} {
	    if {$options(-parent) eq "tree"} {
        array set new_info [LayerMDMGui::GetHierarchy \
          $new_ident [$self cget -container]]
	    set new_absid $new_info(abs_id)
        ::MMSGui::ChangeMMSTree $new_ident $new_absid
	    } else {
          ::MDMHierarchiesGui::ChangeContainerTree $new_ident
          event generate $self <<Refresh>>
	    }
	  }

    } else {                                 ;# New, Copy
      LayerMDMGui::CreateHierarchy hierarchy_info \
        $hierarchy_info(elems) $hierarchy_info(sigmas) \
		$hierarchy_info(container)
	  if {$options(-parent) eq "tree"} {
        event generate $self <<Insert>>
	  } else {
        event generate $self <<Refresh>>
	  }
    }
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Ok>>
	}
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
	
	if {$options(-parent) eq "tree"} {
      $self Details
	} else {
	  event generate $self <<Cancel>>
	}
  }
  
}

}
