#/////////////////////////////////////////////////////////////////////////////
# FILE    : parameterestim_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with Parameters of an Estimation
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::ParameterEstimGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateParameterEstimDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  
  #puts "CreateParameterEstimDetails"
  set details_frame [frame $f.details_ParameterEstim]

  set _details [::MMSContainersGui::bmmscontainer $details_frame.details \
    -type "ParameterEstim" \
    -swlist "::ParameterEstimGui::bmmsparameterestim" \
	-fshowitem "" \
	-fshowlist "::ParameterEstimGui::_ShowParameterEstimDetails" \
	-nobottons "yes" \
  ]

  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowParameterEstimDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateParameterEstimDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $details_frame
  }
  $details_frame.details configure -container $container
  $details_frame.details Init

  return $details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowParameterEstimDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set estim [$tree item text $parent 0]
  
  _ShowParameterEstimHelp  

  _ShowParameterEstimDetails $estim
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowParameterEstimHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerParameterEstimGui::GetParameterEstimHelp]

  ::MMSGui::ShowInfo $message
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsparameterestim {
# PURPOSE : Defines the snit widget used to
#           list the Parameters of an Estimation
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the Estimation
  option -container \
    -default "" -configuremethod "_conf-container"  

  option -details \
    -default "no"  

  variable mdm_parlist
    # mdm_parlist(list)  - Parameters list
  
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
    $self FillList
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the Parameters of an Estimation
  #///////////////////////////////////////////////////////////////////////////
  
    set f $dlg
 
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Parameters List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set ft [frame $f.ft]
    
	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Name"]] \
        [ list text -label [mc "Mean"] ] \
        [ list text -label [mc "Sigma"] ] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns 

    #$tree configure -contextmenu [$self CreateCMenu]
	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	  $tree column configure "range 2 3" -itemjustify "right"
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    focus $tree

    set mdm_parlist(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mdm_parlist(list) [LayerParameterEstimGui::GetParameterEstim \
      $options(-container)]
    #puts "FillList mdm_parlist(list)=$mdm_parlist(list)"

    $tree item delete all

    foreach {{} it} $mdm_parlist(list) {
      array set vinfo $it
      #set icon [::ImageManager::getIconForInstance \
        [LayerMMSGui::GetObjectsAddress $vinfo(abs_id)]]	
      if {$vinfo(type) eq "RandVar::@Real.Normal"} {
	    set icon [::Bitmap::get "real_distribution"]	
	  } elseif {$vinfo(type) eq "RandVar::@Real.Sample"} {
	    set icon [::Bitmap::get "real_sample"]	
	  } else {
	    set icon [::Bitmap::get "real"]	
	  }

	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(ident)] \
          [list $vinfo(mean)] \
          [list $vinfo(sigma)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }
	  
      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
              [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    if {$cur_item != 0} {
      $tree activate $cur_item
      $tree selection add $cur_item
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }

}  

}
