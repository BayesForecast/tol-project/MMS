#/////////////////////////////////////////////////////////////////////////////
# FILE    : modelresults_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with Model Results of an Estimation
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::ModelResultsGui {

#/////////////////////////////////////////////////////////////////////////////
proc ExpandModelResults {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set ident [$tree item text $id 0]
  
  set _list [list \
	"Submodels"          "SubmodelResultsGui::ExpandSubmodelResultsList" \
	                     "SubmodelResultsGui::ShowSubmodelResultsListDetails" \
						 "SubmodelsResults" \
	"Hierarchies"        "HierarchyResultsGui::ExpandHierarchyResultsList" \
        	             "HierarchyResultsGui::ShowHierarchyResultsListDetails" \
						 "HierarchiesResults" \
	"Parameters"         "ParameterResultsGui::ExpandParameterResultsList" \
        	             "ParameterResultsGui::ShowParameterResultsListDetails" \
						 "ParametersResults" \
	"Combinations"       "MCombinationResultsGui::ExpandMCombinationResultsList" \
        	             "MCombinationResultsGui::ShowMCombinationResultsListDetails" \
						 "MCombinationsResults" \
  ]

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  foreach {name fexpand fdetails type} $_list {
    set row [list [list [::Bitmap::get "Set"] [mc $name]] \
			  [list $type] \
	          [list $fexpand] \
			  [list $fdetails] \
			  [list ""] \
			  [list ""] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

}
