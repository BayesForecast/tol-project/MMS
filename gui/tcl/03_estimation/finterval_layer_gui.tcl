#/////////////////////////////////////////////////////////////////////////////
# FILE:    finterval_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          the FIntervals of a Forecast
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerFIntervalGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetFIntervalTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "FInterval 1"
    }
    {}
    {
      name "FInterval 2"
    }
  }
}

variable finterval_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetFInterval {forecast} {
#/////////////////////////////////////////////////////////////////////////////
  variable finterval_test
  
  if { $finterval_test } {
    return [GetFIntervalTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::ForecastsGui::GetFInterval" \
    "Set" $forecast
}

#///////////////////////////////////////////////////////////////////////////
proc GetFIntervalHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::ForecastsGui::GetFIntervalHelp" \
    "Text" "?"
}

variable edit_finterval_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditFInterval {finterval_info finterval} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_finterval_test
  upvar $finterval_info finterval_array
  
  set finterval_set [TclLst2TolSet $finterval -level 2]
  #puts "EditFInterval:finterval_set=$finterval_set"
  
  if { $edit_finterval_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::ForecastsGui::EditFIntervalTest" \
      "Real" finterval_array $finterval_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::ForecastsGui::EditFInterval" \
      "Real" finterval_array $finterval_set
  }
}

}
