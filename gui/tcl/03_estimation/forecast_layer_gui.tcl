#/////////////////////////////////////////////////////////////////////////////
# FILE:    forecast_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          MED (Forecast)
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerForecastsGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetForecastTest {} {
#/////////////////////////////////////////////////////////////////////////////
  return {
    name "Forecast 1"
    vers "1.0"
    desc "desc of Forecast 1"
    dCre "2010/08/25"
  }
}
  
variable get_forecast_test 0
#///////////////////////////////////////////////////////////////////////////
proc GetForecast {ident} {
#///////////////////////////////////////////////////////////////////////////
  variable get_forecast_test
  
  if { $get_forecast_test } {
    return [GetForecastTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::ForecastsGui::GetForecast" \
    "Set" $ident
}

#/////////////////////////////////////////////////////////////////////////////
proc GetForecastsListTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "Forecast 1"
      vers "1.0"
    }
    {}
    {
      name "Forecast 2"
      vers "2.0"
    }
  }
}

variable get_forecasts_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetForecastsList {details} {
#/////////////////////////////////////////////////////////////////////////////
  variable get_forecasts_test
  
  if { $get_forecasts_test } {
    return [GetForecastsListTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::ForecastsGui::GetForecastsList" \
    "Set" $details
}

#///////////////////////////////////////////////////////////////////////////
proc GetForecastHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::ForecastsGui::GetForecastHelp" \
    "Text" "?"
}

#///////////////////////////////////////////////////////////////////////////
proc GetForecastModel {ident} {
#///////////////////////////////////////////////////////////////////////////

  LayerMMSGui::EvalTolFun "MMS::Layer::ForecastsGui::GetForecastModel" \
    "Set" $ident
}

#///////////////////////////////////////////////////////////////////////////
proc GetForecastSettings {ident} {
#///////////////////////////////////////////////////////////////////////////

  LayerMMSGui::EvalTolFun "MMS::Layer::ForecastsGui::GetForecastSettings" \
    "Set" $ident
}

#///////////////////////////////////////////////////////////////////////////
proc GetModelForecast {ident} {
#///////////////////////////////////////////////////////////////////////////

  LayerMMSGui::EvalTolFun "MMS::Layer::ForecastsGui::GetModelForecast" \
    "Set" $ident
}

#///////////////////////////////////////////////////////////////////////////
proc HasForecast {ident} {
#///////////////////////////////////////////////////////////////////////////

  LayerMMSGui::EvalTolFun "MMS::Layer::ForecastsGui::HasForecast" \
    "Real" $ident
}

variable create_forecast_test 0
#///////////////////////////////////////////////////////////////////////////
proc CreateForecast {forecast_info attributes} {
#///////////////////////////////////////////////////////////////////////////
  variable create_forecast_test
  upvar $forecast_info arrayarg
  
  set attributes_set [TclLst2TolSet $attributes -level 2]
  
  if { $create_forecast_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::ForecastsGui::CreateForecastTest" \
	  "Real" arrayarg $attributes_set
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::ForecastsGui::CreateForecast" \
	  "Real" arrayarg $attributes_set
  }
}

variable copy_fore_test 0
#///////////////////////////////////////////////////////////////////////////
proc CopyForecast {fore_info} {
#///////////////////////////////////////////////////////////////////////////
  variable copy_fore_test
  upvar $fore_info arrayarg
  
  if { $copy_fore_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::ForecastsGui::CopyForecastTest" \
	  "Real" arrayarg
	
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::ForecastsGui::CopyForecast" \
	  "Real" arrayarg
  }
}

variable edit_forecast_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditForecast {forecast_info attributes} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_forecast_test
  upvar $forecast_info arrayarg
  
  set attributes_set [TclLst2TolSet $attributes -level 2]
  
  if { $edit_forecast_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::ForecastsGui::EditForecastTest" \
	  "Real" arrayarg $attributes_set

  } else {
	LayerMMSGui::EvalTolFunArr "MMS::Layer::ForecastsGui::EditForecast" \
	  "Real" arrayarg $attributes_set
  }
}

}
