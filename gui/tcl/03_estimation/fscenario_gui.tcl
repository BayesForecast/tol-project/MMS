#/////////////////////////////////////////////////////////////////////////////
# FILE    : fscenario_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with the FScenarios of a Forecast
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::FScenarioGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateFScenarioDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame
  
  #puts "CreateFScenarioDetails"
  set details_frame [frame $f.details_FScenario]
  set view_frame [frame $f.view_FScenario]

  set _details [bmmsfscenario $details_frame.details]
  set _view [bmmsfscenario $view_frame.details]

  grid rowconfigure $view_frame 0 -weight 1
  grid columnconfigure $view_frame 0 -weight 1
  grid $view_frame -row 0 -column 0 -sticky news
  grid remove $view_frame
  grid rowconfigure $details_frame 0 -weight 1
  grid columnconfigure $details_frame 0 -weight 1
  grid $details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowFScenarioDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////
  variable details_frame
  variable view_frame

  set parent [$tree item parent $id]
  set forecast [$tree item text $parent 0]

  if {![info exists details_frame] || ![winfo exists $details_frame]} {
    CreateFScenarioDetails [::MMSGui::GetMMSDetails]
    set _frame $details_frame
  } else {
    if {[::MMSGui::IsEditionActive]} {
      set _frame $view_frame
    } else {
      set _frame $details_frame
	}
    grid $_frame
  }
  $_frame.details configure -forecast $forecast
  
  _ShowFScenarioHelp  

  return $_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowFScenarioHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerFScenarioGui::GetFScenarioHelp]

  ::MMSGui::ShowInfo $message
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsfscenario {
# PURPOSE : Defines the snit widget used to
#           edit the FScenarios of a Forecast
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the Forecast
  option -forecast \
    -default "" -configuremethod "_conf-forecast"  

  variable label_state

  variable widgets
    
  variable forecast_scenario
    # forecast_scenario(list)         - Inputs list
    # forecast_scenario(left_list)    - Inputs left list (Name, Grammar)
    # forecast_scenario(right_list)   - Scenario right list
    #                                   (Input Name or Pattern, Scenario)

  variable swaptablelist_options
  
  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Initialize the variable information
    $self _init
    
    # Paint the window
    $self _create

    # Apply all options passed at creation time.
    $self configurelist $args
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-forecast { _ forecast } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-forecast) $forecast 
    $self Details
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #
  # PURPOSE: Initializes the options for the swaptablelist
  #///////////////////////////////////////////////////////////////////////////
 
    set forecast_scenario(right_list) [list]
	
    set swaptablelist_options(-llabel) [mc "Inputs"]
    set swaptablelist_options(-rlabel) [mc "Scenarios"]
	
    set swaptablelist_options(-lcolumns) [list \
      [list 0 [mc "Name"] left] \
      [list 0 [mc "Grammar"] left] \
      [list 0 [mc ""] left] \
    ]
    set swaptablelist_options(-rcolumns) [list \
      [list 0 [mc "Name or Pattern"] left] \
      [list 0 [mc ""] left] \
      [list 0 [mc "Scenario"] left] \
    ]
	set swaptablelist_options(-keycolumns) [list \
	  col_name \
    ]

    set swaptablelist_options(-height) 24
    set swaptablelist_options(-lwidth) 35
    set swaptablelist_options(-rwidth) 65
    set swaptablelist_options(-llistvar) [list]
    set swaptablelist_options(-rlistvar) [myvar forecast_scenario(right_list)]
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #///////////////////////////////////////////////////////////////////////////
  
    set f $dlg
 
    # Button: Edit
    set fbu [frame $f.fbu]
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc Edit] \
      -helptext [mc "Edit Scenarios"] -padx 1 -relief link \
      -compound left -command [list $self Edit] \
      -state disabled
    set widgets(edit) $fbu.bEdit

    grid $fbu.bEdit -sticky w -padx 2 -pady 2
    grid rowconfigure    $fbu 0 -weight 1
    grid columnconfigure $fbu 1 -weight 1
	
    # Entry frame
    label $f.lHeader -textvariable [myvar label_state] -pady 5 -padx 5
    set lfe [labelframe $f.lfe \
      -labelwidget $f.lHeader -relief solid -bd 2]
  
    set swe [ScrolledWindow $lfe.swe]
    set sfe [ScrollableFrame $swe.sfe]
    $swe setwidget $sfe
    set fe [$sfe getframe]
	  
    # Button: Refresh
    set fb [frame $fe.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Inputs List"] -padx 1 -relief link \
      -compound left -command [mymethod FillLeftList]
    set widgets(bref) $fb.bRef
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Swaptablelist
    set ft [frame $fe.ft]

    #puts "_create:swaptablelist_options=[array get swaptablelist_options]"
    set widgets(tree) [eval [list Swaptablelist::create_swap_list $ft] [array get swaptablelist_options]]
    #puts "_create:widgets(tree)=$widgets(tree)"    

	bind $widgets(tree) <<SwaptablelistUpdated>> \
	  "$self Changed"
	
    set ltbl [Swaptablelist::getLTable $widgets(tree)]
    $ltbl columnconfigure 0 -name col_name -editable no -sortmode dictionary
    $ltbl columnconfigure 1 -name col_grammar -editable no -sortmode dictionary
    $ltbl columnconfigure 2 -name col_scenario -editable no -hide 1

    set rtbl [Swaptablelist::getRTable $widgets(tree)]
    $rtbl columnconfigure 0 -name col_name -editable yes -labelcommand "$self DoNothing"
    $rtbl columnconfigure 1 -name col_grammar -editable no -hide 1
    $rtbl columnconfigure 2 -name col_scenario -editable yes -labelcommand "$self DoNothing"

	bind $rtbl <<TablelistCellUpdated>> \
	  "$self Changed"
	        
    grid $fe.fb -sticky news
    grid $fe.ft -sticky news -padx 5 -pady 2

    grid rowconfigure    $fe 1 -weight 1
    grid columnconfigure $fe 0 -weight 1
  
	grid $swe -row 0 -column 0 -sticky nsew
    grid columnconfigure $lfe 0 -weight 1
    grid rowconfigure    $lfe 0 -weight 1
	
    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state disabled
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state disabled
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1
    
    grid $fbu -sticky news
    grid $lfe -sticky news
    grid $fbd -sticky news

    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
	
	set forecast_scenario(list) ""
  }

  #///////////////////////////////////////////////////////////////////////////
  method put_state {s} {
  #///////////////////////////////////////////////////////////////////////////

    set tbl [Swaptablelist::getLTable $widgets(tree)]
    $tbl configure -state $s
    set tbl [Swaptablelist::getRTable $widgets(tree)]
    $tbl configure -state $s
	$widgets(tree).lr.left configure -state $s
	$widgets(tree).lr.right configure -state $s
    $widgets(bref) configure -state $s
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method DoNothing {args} {
  #/////////////////////////////////////////////////////////////////////////////
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Changed {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable changed
	
    set changed 1
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method FillLeftList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set model_container [list "Forecast" $options(-forecast)]
    #set forecast_scenario(list) [LayerMDMGui::GetInputsList \
      $model(ident)]
	set details "no"
    set forecast_scenario(list) [LayerMDMGui::GetMVariablesList \
      $model_container $details]
    #puts "FillLeftList forecast_scenario(list)=$forecast_scenario(list)"
	
    set tbl [Swaptablelist::getLTable $widgets(tree)]
    $tbl delete 0 end

    set forecast_scenario(left_list) [list]
    foreach {{} it} $forecast_scenario(list) {
      array set vinfo $it
      set row [list $vinfo(ident) $vinfo(grammar) \
	    ""]
      lappend forecast_scenario(left_list) $row
    }

    Swaptablelist::insert $tbl $forecast_scenario(left_list)
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillRightList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set l_fscenario [LayerFScenarioGui::GetFScenario \
      $options(-forecast)]
    #puts "FillRightList l_fscenario=$l_fscenario"

    set tbl [Swaptablelist::getRTable $widgets(tree)]
    $tbl delete 0 end

	foreach {{} it} $l_fscenario {
      array set vinfo $it
      set row [list $vinfo(pattern) \
        "" $vinfo(scenario)]
      $tbl insert end $row
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillLists {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable changed
	
    set changed 0

    $self FillLeftList
    $self FillRightList
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method finishediting {} {
  #/////////////////////////////////////////////////////////////////////////////

    set tbl [Swaptablelist::getRTable $widgets(tree)]
    $tbl finishediting
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Details {} {
  #/////////////////////////////////////////////////////////////////////////////

    set label_state [mc "Details of the Scenarios"]
    
    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    foreach {w} {edit} {
      $widgets($w) configure -state $_state
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state disabled
    }

	$self put_state "normal"
    $self FillLists
	$self put_state "disabled"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Edit {} {
  #/////////////////////////////////////////////////////////////////////////////

    set label_state [mc "Edit Scenarios"]
    ::MMSGui::ActivateEdition $win

    foreach {w} {edit} {
      $widgets($w) configure -state disabled
    }
    foreach {w} {accept cancel} {
      $widgets($w) configure -state normal
    }
	$self put_state "normal"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////
    variable changed
	
	::MMSGui::DisactivateEdition
  
    $self finishediting
	
	if {$changed == 1} {
	  #puts "Ok:forecast_scenario(right_list)=$forecast_scenario(right_list)"
	  set fscenario_info(forecast) [$self cget -forecast]
      LayerFScenarioGui::EditFScenario fscenario_info \
	    $forecast_scenario(right_list)
	}
    $self Details
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

	::MMSGui::DisactivateEdition
  
    $self finishediting
	
    $self Details
  }

}  

}
