#/////////////////////////////////////////////////////////////////////////////
# FILE    : modelforecast_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with Model Forecast
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::ModelForecastGui {

#/////////////////////////////////////////////////////////////////////////////
proc ExpandModelForecast {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set ident [$tree item text $id 0]
  
  set _list [list \
	"Submodels"          "SubmodelForecastGui::ExpandSubmodelForecastList" \
	                     "SubmodelForecastGui::ShowSubmodelForecastListDetails" \
						 "SubmodelsForecast" \
	"Hierarchies"        "HierarchyForecastGui::ExpandHierarchyForecastList" \
        	             "HierarchyForecastGui::ShowHierarchyForecastListDetails" \
						 "HierarchiesForecast" \
  ]

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  foreach {name fexpand fdetails type} $_list {
    set row [list [list [::Bitmap::get "Set"] [mc $name]] \
			  [list $type] \
	          [list $fexpand] \
			  [list $fdetails] \
			  [list ""] \
			  [list ""] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

}
