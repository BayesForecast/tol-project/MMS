#/////////////////////////////////////////////////////////////////////////////
# FILE    : submodelresults_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           associated with Submodel Results of an Estimation
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::SubmodelResultsGui {

#/////////////////////////////////////////////////////////////////////////////
proc CreateSubmodelResultsListDetails {f} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame
  
  #puts "CreateSubmodelResultsListDetails"
  set l_details_frame [frame $f.details_SubmodelResults]

  set _details [::MMSContainersGui::bmmscontainer $l_details_frame.details \
    -type "SubmodelResult" \
    -swlist "::SubmodelResultsGui::bmmssubrlist" \
	-fshowitem "" \
	-fshowlist "::SubmodelResultsGui::_ShowSubmodelResultsListDetails" \
	-nobottons "yes" \
  ]

  grid rowconfigure $l_details_frame 0 -weight 1
  grid columnconfigure $l_details_frame 0 -weight 1
  grid $l_details_frame -row 0 -column 0 -sticky news
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowSubmodelResultsListDetails {container} {
#/////////////////////////////////////////////////////////////////////////////
  variable l_details_frame

  if {![info exists l_details_frame] || ![winfo exists $l_details_frame]} {
    CreateSubmodelResultsListDetails [::MMSGui::GetMMSDetails]
  } else {
    grid $l_details_frame
  }
  $l_details_frame.details configure -container $container
  $l_details_frame.details Init

  return $l_details_frame
}

#/////////////////////////////////////////////////////////////////////////////
proc ShowSubmodelResultsListDetails {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set grandparent [$tree item parent $parent]
  set estim [$tree item text $grandparent 0]
  
  _ShowSubmodelResultsHelp  

  _ShowSubmodelResultsListDetails $estim
}

#/////////////////////////////////////////////////////////////////////////////
proc _ShowSubmodelResultsHelp {} {
#/////////////////////////////////////////////////////////////////////////////

  set message [LayerSubmodelResultsGui::GetSubmodelResultsHelp]

  ::MMSGui::ShowInfo $message
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandSubmodelResultsList {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  set parent [$tree item parent $id]
  set grandparent [$tree item parent $parent]
  set estim [$tree item text $grandparent 0]
	
  set _list [LayerSubmodelResultsGui::GetSubmodelResultsList $estim]
  #puts "ExpandSubmodelResultsList:_list=$_list"

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  set icon [::Bitmap::get "mms_submodel_results"]
  foreach {{} it} $_list {
    array set vinfo $it
    set row [list [list $icon $vinfo(ident)] \
	          [list "SubmodelResults"] \
	          [list "SubmodelResultsGui::ExpandSubmodelResults"] \
	          [list ""] \
	          [list ""] \
			  [list $vinfo(abs_id)] \
            ]
    $tree insert $row \
      -at child -relative $id
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ExpandSubmodelResults {tree id} {
#/////////////////////////////////////////////////////////////////////////////

  foreach it [$tree item children $id] {
    $tree item delete $it
  }
  
  set row [list [list [::Bitmap::get "Set"] [mc "Explanatory Terms"]] \
			    [list "Explanatory Terms"] \
	            [list "ExpTermResultsGui::ExpandExpTermResultsList"] \
			    [list "ExpTermResultsGui::ShowExpTermResultsListDetails"] \
			    [list ""] \
			    [list ""] \
          ]
  $tree insert $row \
    -at child -relative $id
}


#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmssubrlist {
# PURPOSE : Defines the snit widget used to
#           list the Submodel Results of an Estimation
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Identifier of the Estimation
  option -container \
    -default "" -configuremethod "_conf-container"  

  option -details \
    -default "no"  

  variable mdm_subr
    # mdm_subr(list)  - Submodel Results list
    
  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  component dlg

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _conf-container { _ container } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-container) $container 
    $self FillList
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-details { _ details } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-details) $details 
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          list the Submodel Results of an Estimation
  #///////////////////////////////////////////////////////////////////////////
  
    set f $dlg
 
    # Button: Refresh
    set fb [frame $f.fb]
    
    Button $fb.bRef -image [::Bitmap::get refresh] -text [mc Refresh] \
      -helptext [mc "Refresh Submodel Results List"] -padx 1 -relief link \
      -compound left -command [mymethod RefreshList]
  
    grid $fb.bRef -sticky w -padx 2 -pady 2
    grid rowconfigure    $fb 0 -weight 1
    grid columnconfigure $fb 1 -weight 1
  
    # Tree in form of table
    set ft [frame $f.ft]
    
	if {$options(-details) eq "yes"} {
	  set _columns [ list \
        [list text] \
        [list {image text} -label [mc "Name"]] \
        [list text] \
      ] 
	} else {
	  set _columns [ list \
        [list {image text} -label [mc "Identifier"]]
      ] 
	}
    install tree as ::wtree $ft.tv -table 1 \
      -background white \
      -columns $_columns 

    #$tree configure -contextmenu [$self CreateCMenu]
	$tree column configure tail -visible no
	if {$options(-details) eq "yes"} {
	  $tree column configure first -visible no
	  $tree column configure last -visible no
	  $tree column configure "order 1" -expand yes -weight 1
	} else {
	  $tree column configure first -expand yes -weight 1
	}

    grid $ft.tv -sticky news -padx 2 -pady 2
    grid rowconfigure    $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
  
    grid $f.fb -sticky news
    grid $f.ft -sticky news
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news
    
    focus $tree

    set mdm_subr(list) ""
  }

  #/////////////////////////////////////////////////////////////////////////////
  method FillList {} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set mdm_subr(list) [LayerSubmodelResultsGui::GetSubmodelResultsList \
      $options(-container)]
    #puts "FillList mdm_subr(list)=$mdm_subr(list)"

    $tree item delete all

    foreach {{} it} $mdm_subr(list) {
      array set vinfo $it
      set icon [::ImageManager::getIconForInstance \
        [LayerMMSGui::GetObjectsAddress $vinfo(abs_id)]]	
	  
	  if {$options(-details) eq "yes"} {
	    set row [ list \
          [list $vinfo(ident)] \
	      [list $icon $vinfo(ident)] \
	      [list $vinfo(abs_id)] \
        ] 
	  } else {
	    set row [ list \
          [list $icon $vinfo(ident)]
        ] 
 	  }
	  
      set id [$tree insert $row \
         -at end -relative "root"]
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method MakeActiveItem {ident} {
  #/////////////////////////////////////////////////////////////////////////////

    set num_item [$tree item count]
    set cur_item 0
    
    if {$ident != ""} {
      set cur_item 1
      while {$cur_item < $num_item && \
              [$tree item text $cur_item first] != $ident} {
        incr cur_item
      }
      if {$cur_item == $num_item} {
        set cur_item 0
      }
    }
    
    $tree selection clear
    if {$cur_item != 0} {
      $tree activate $cur_item
      $tree selection add $cur_item
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method RefreshList {} {
  #/////////////////////////////////////////////////////////////////////////////

    set ident [$self GetActiveItem]
    $self FillList
    $self MakeActiveItem $ident
  }
    
}  

}
