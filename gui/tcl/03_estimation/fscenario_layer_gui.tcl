#/////////////////////////////////////////////////////////////////////////////
# FILE:    fscenario_layer_gui.tcl
# PURPOSE: Definition of the communication layer between MMS and the GUI for
#          the FScenarios of a Forecast
#/////////////////////////////////////////////////////////////////////////////

namespace eval LayerFScenarioGui {

#/////////////////////////////////////////////////////////////////////////////
proc GetFScenarioTest { } {
#/////////////////////////////////////////////////////////////////////////////
  return {
    {}
    {
      name "FScenario 1"
    }
    {}
    {
      name "FScenario 2"
    }
  }
}

variable fscenario_test 0
#/////////////////////////////////////////////////////////////////////////////
proc GetFScenario {forecast} {
#/////////////////////////////////////////////////////////////////////////////
  variable fscenario_test
  
  if { $fscenario_test } {
    return [GetFScenarioTest]
  }
  LayerMMSGui::EvalTolFun "MMS::Layer::ForecastsGui::GetFScenario" \
    "Set" $forecast
}

#///////////////////////////////////////////////////////////////////////////
proc GetFScenarioHelp {} {
#///////////////////////////////////////////////////////////////////////////
  
  LayerMMSGui::EvalTolFunArgNoChg "MMS::Layer::ForecastsGui::GetFScenarioHelp" \
    "Text" "?"
}

variable edit_fscenario_test 0
#///////////////////////////////////////////////////////////////////////////
proc EditFScenario {fscenario_info fscenario} {
#///////////////////////////////////////////////////////////////////////////
  variable edit_fscenario_test
  upvar $fscenario_info fscenario_array
  
  set fscenario_set [TclLst2TolSet $fscenario -level 2]
  #puts "EditFScenario:fscenario_set=$fscenario_set"
  
  if { $edit_fscenario_test } {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::ForecastsGui::EditFScenarioTest" \
      "Real" fscenario_array $fscenario_set
  } else {
    LayerMMSGui::EvalTolFunArr "MMS::Layer::ForecastsGui::EditFScenario" \
      "Real" fscenario_array $fscenario_set
  }
}

}
