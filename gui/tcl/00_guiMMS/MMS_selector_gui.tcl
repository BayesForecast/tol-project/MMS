#/////////////////////////////////////////////////////////////////////////////
# FILE    : MMS_selector_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           to manipulate the selectors of objects of MMS
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::MMSSelectorsGui {

#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsselector {
# PURPOSE : Defines the snit widget used to
#           manipulate the selectors of objects
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Type of the items of the selector
  option -type \
    -default "" 
	
  # Snit widget used to list the items of the selector
  option -swlist \
    -default "" 
	
  # Defines if the selection is multiple
  option -multiple \
    -default "no" 
	
  # Defines if the selection is only on available items
  option -available \
    -default "no" 
	
  variable widgets
  
  variable mms_sel
    # Selected item (Identifier) if option -multiple is "no" or
	# Selected items (List of identifiers) if option -multiple is "yes"
    
  component dlg

  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    # Initialize the variable information
    $self _init
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #///////////////////////////////////////////////////////////////////////////

    if {[$self cget -multiple] eq "no"} {
	  set mms_sel ""
	} else {
      set mms_sel {}
	}
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          select an object or a list of multiple objects
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
    $f configure -relief solid -bd 2
    
    # Tree frame
    label $f.lHeader -text "[mc "Selector of"] [mc [$self cget -type]s]" \
	  -pady 5 -padx 5
    set ft [labelframe $f.ft \
      -labelwidget $f.lHeader -relief solid -bd 1]
  
    if {[$self cget -available] eq "yes"} {
      install tree as [$self cget -swlist] $ft.mmstree \
        -details "no" -available "yes"
	} else {
      install tree as [$self cget -swlist] $ft.mmstree \
        -details "no"
    }	  
    
    $tree configure -filter "yes" -columnfilter {0 1}

    $tree notify bind $tree <<ItemSelected>> \
      "$self Ok"

    $tree notify bind $tree <<Escape>> \
      "$self Cancel"

    grid rowconfigure $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1

    # Buttons: Accept, Cancel
    set fbd [frame $f.fbd]
  
    Button $fbd.bAccept -image [::Bitmap::get accept] -text [mc "Accept"] \
      -relief link -compound left -command [list $self Ok] \
      -state normal
    set widgets(accept) $fbd.bAccept
    
    Button $fbd.bCancel -image [::Bitmap::get cancel] -text [mc "Cancel"] \
      -relief link -compound left -command [list $self Cancel] \
      -state normal
    set widgets(cancel) $fbd.bCancel
      
    grid $fbd.bAccept $fbd.bCancel -sticky e -padx 5 -pady 5
    grid rowconfigure    $fbd 0 -weight 1
    grid columnconfigure $fbd 0 -weight 1

    grid $f.ft -sticky news -padx 5
    grid $f.fbd -sticky news
    grid rowconfigure    $f 0 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    foreach {w} {accept cancel} {
      bind $widgets($w) <Escape> "$self Cancel"
      bind $widgets($w) <Down> {event generate %W <Tab>}
      bind $widgets($w) <Up> {event generate %W <Shift-Tab>}
    }
    bind $tree <Shift-Tab> "focus $widgets(cancel) ; break"
    bind $widgets(cancel) <Shift-Tab> "focus $widgets(accept) ; break"
    bind $widgets(cancel) <Tab> "focus $tree ; break"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

    if {[$self cget -multiple] eq "no"} {
      set mms_sel [$tree GetActiveItem]
      if {$mms_sel != ""} {
        event generate $win <<OnAccept>>
      }
	} else {
      set mms_sel [$tree GetSelectedItems]
      if {[llength $mms_sel] != 0} {
        event generate $win <<OnAccept>>
      }
	}
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

    if {[$self cget -multiple] eq "no"} {
      $tree MakeActiveItem $mms_sel
	} else {
      $tree MakeSelectedItems $mms_sel
	}
    event generate $win <<OnCancel>>    
  }

  #/////////////////////////////////////////////////////////////////////////////
  method set_info {iteminfo} {
  #/////////////////////////////////////////////////////////////////////////////

    set mms_sel $iteminfo

    if {[$self cget -multiple] eq "no"} {
      $tree MakeActiveItem $mms_sel
	} else {
      $tree MakeSelectedItems $mms_sel
	}
	#$tree _applyFilter
    focus $tree
  }

  #/////////////////////////////////////////////////////////////////////////////
  method get_info {what} {
  #/////////////////////////////////////////////////////////////////////////////

    return $mms_sel
  }
  
}

::snit::widget comboselector {

  delegate method * to combo
  delegate option * to combo

  option -entry_args -default "" -readonly yes
  option -button_args -default "" -readonly yes
  option -transf_args -default "" -readonly yes
  
  option -swlist -default "" -readonly yes
  option -type -default "" -readonly yes
  option -multiple -default "no" -readonly yes
  option -available -default "no" -readonly yes
  
  component combo
  
  constructor { args } {
    $self configurelist $args
 
    install combo using wcombo $win.combo \
        -centry centry  \
        -cbutton cButton \
        -cuser [list "::MMSSelectorsGui::bmmsselector \
		  -type $options(-type) -swlist $options(-swlist) \
		  -multiple $options(-multiple) \
		  -available $options(-available)" \
		]
		  
    eval $combo entry configure $options(-entry_args)
    eval $combo button configure $options(-button_args)
    eval $combo transient configure $options(-transf_args)

    bind $combo <<OnAccept>> "event generate $win <<OnAccept>>"
    bind $combo <<OnCancel>> "event generate $win <<OnCancel>>"
	
    grid $combo -sticky nsew
    grid $win -sticky nsew
  }  
}

}
