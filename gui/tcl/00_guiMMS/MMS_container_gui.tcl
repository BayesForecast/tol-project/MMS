#/////////////////////////////////////////////////////////////////////////////
# FILE    : MMS_container_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           to show the sets (containers) of MMS
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::MMSContainersGui {

#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmscontainer {
# PURPOSE : Defines the snit widget used to
#           show a detailed list of a container
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  # Type of the items of the container
  option -type \
    -default "" 
	
  # Snit widget used to list the items of the container
  option -swlist \
    -default ""

  # Mode used for the next 3 fshowXXX options
  # There are two modes: "0" (default) and "1" (using absid)
  option -fshowmode \
    -default 0
	
  # Function used to show the frame with the details of one item
  option -fshowitem \
    -default ""
  # (fshowmode=0) fshowitem container ident state parent
  # (fshowmode=1) fshowitem ident objectid containerid state parent
	
  # Function used to show the information of one item
  option -fshowinfo \
    -default ""
  # (fshowmode=0) fshowinfo container ident
  # (fshowmode=1) fshowinfo objectid
	
  # Function used to show the frame with the list of items
  option -fshowlist \
    -default ""  
  # (fshowmode=0) fshowlist container
  # (fshowmode=1) fshowlist containerid

  # Name of the command to invoke on botton 'Delete'
  option -cmddelete \
    -default ""  

  # Commands New and Copy are not allowed
  option -notcreate \
    -default ""  

  # Only command New is allowed
  option -onlycreate \
    -default ""  

  # Don�t show any botton
  option -nobottons \
    -default ""  

  # Use the own context menu instead of the generic
  option -cmenu \
    -default ""  

  variable widgets
  
  variable mms_item
    # mms_item(sel)   - Selected item to show at details frame (Identifier) 
    
  component dlg

  variable tree
  
  delegate method * to tree
  delegate option * to tree
  delegate option -borderwidth to hull
  delegate option -bd to hull
  delegate option -relief to hull

  #typeconstructor
  
  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    # Dialog
    install dlg as frame $win.d 

    # Apply all options passed at creation time.
    $self configurelist $args
    
    # Paint the window
    $self _create

    # Initialize the variable information
    $self _init
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #///////////////////////////////////////////////////////////////////////////

    set mms_item(sel) ""
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          show a detailed list of a container
  #///////////////////////////////////////////////////////////////////////////

    set f $dlg
    
    # Buttons: New, Edit, Copy, ...
    set fbu [frame $f.fbu]
  
    Button $fbu.bNew -image [::Bitmap::get new] -text [mc "New"] \
      -helptext "[mc "Create"] [mc [$self cget -type]]" \
	  -padx 1 -relief link \
      -compound left -command [list $self Details "New"] \
      -state normal
    set widgets(new) $fbu.bNew
  
    Button $fbu.bEdit -image [::Bitmap::get edit] -text [mc "Edit"] \
      -helptext "[mc "Edit"] [mc [$self cget -type]]" \
	  -padx 1 -relief link \
      -compound left -command [list $self Details "Edit"] \
      -state disabled

    Button $fbu.bCopy -image [::Bitmap::get copy] -text [mc "Duplicate"] \
      -helptext "[mc "Duplicate"] [mc [$self cget -type]]" \
	  -padx 1 -relief link \
      -compound left -command [list $self Details "Copy"] \
      -state disabled

    Button $fbu.bDel -image [::Bitmap::get delete] -text [mc "Delete"] \
      -helptext "[mc "Delete"] [mc [$self cget -type]]" \
	  -padx 1 -relief link \
      -compound left -command [list $self Delete] \
      -state disabled

    if {[$self cget -nobottons] ne "yes"} {	

      if {[$self cget -notcreate] eq "yes"} {	
	    set widgets(all) [list $fbu.bEdit]
	  } elseif {[$self cget -onlycreate] eq "yes"} {
	    set widgets(all) [list $fbu.bNew]
	  } else {
 	    set widgets(all) [list $fbu.bNew $fbu.bEdit $fbu.bCopy]
	  }
      if {[$self cget -cmddelete] ne ""} {	
        lappend widgets(all) $fbu.bDel
      }
	
      eval "grid $widgets(all) -sticky w -padx 2 -pady 2"
      grid rowconfigure    $fbu 0 -weight 1
      grid columnconfigure $fbu [llength $widgets(all)] -weight 1
	} else {
      set widgets(all) [list]
	}

    # Tree frame
    label $f.lHeader -text "[mc "List of"] [mc [$self cget -type]s]" \
	  -pady 5 -padx 5
    set ft [labelframe $f.ft \
      -labelwidget $f.lHeader -relief solid -bd 2]
  
	install tree as [$self cget -swlist] $ft.mmstree \
      -details "yes"             

    $tree configure -filter "yes" -columnfilter {0 0 1 1}

    $tree notify bind $tree <ActiveItem> \
      "$self ItemIsActive"
    
    if {[$self cget -cmenu] ne "yes"} {	
      $tree configure -contextmenu [$self CreateCMenu]
	}

    grid rowconfigure $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1

    grid $fbu -sticky news
    grid $f.ft -sticky news -padx 5
    grid rowconfigure    $f 1 -weight 1
    grid columnconfigure $f 0 -weight 1
    grid $f -sticky news

    $tree notify bind $tree <<ItemSelected>> \
      [list $self Details "Edit"]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method CreateCMenu { } {
  #/////////////////////////////////////////////////////////////////////////////

    menu $tree.cmenu -tearoff 0 \
      -postcommand [list ::MMSGui::PostCMenu $tree]
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Init {} {
  #/////////////////////////////////////////////////////////////////////////////

    foreach {w} $widgets(all) {
      $w configure -state disabled
    }
    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    $widgets(new) configure -state $_state
	#$tree _applyFilter
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ItemIsActive {} {
  #/////////////////////////////////////////////////////////////////////////////

    if {[::MMSGui::IsEditionActive]} {
      set _state "disabled"
    } else {
      set _state "normal"
	}
    foreach {w} $widgets(all) {
      $w configure -state $_state
    }

    set finfo [$self cget -fshowinfo]

    puts "fshowinfo"
	if {$finfo ne ""} {
      if {[$self cget -fshowmode] eq 0} {
        puts "fshowinfo fshowmode=0"
	    set ident [$tree GetActiveItem]
	    set container [$self cget -container]
	    $finfo $container $ident
      } {
        set objectid [$tree item text active last]
        puts "fshowinfo fshowmode=1 objectid=$objectid"
	    $finfo $objectid
      }
	}
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method GetActiveItem {} {
  #///////////////////////////////////////////////////////////////////////////// 

    return [$tree item text active first]   
  }
    
  #/////////////////////////////////////////////////////////////////////////////
  method Details {state} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "Details"
    if {$state eq "Edit"} {
      if {[$self cget -onlycreate] eq "yes" ||
	      [$self cget -nobottons] eq "yes" ||
		  [::MMSGui::IsEditionActive]} {
	    return
	  }
	}
	
	set ident [$tree GetActiveItem]
    set mms_item(sel) $ident
	
    ::MMSGui::ClearMMSDetails

    puts "fshowitem"
    if {[$self cget -fshowmode] eq 0} {
      puts "fshowitem fshowmode=0"
      set new_frame [[$self cget -fshowitem] \
	    [$tree cget -container] $ident $state "list"]
    } {
      set objectid [$tree item text active last]
      set containerid [$tree cget -containerid]
      puts "fshowitem fshowmode=1 objectid=$objectid containerid=$containerid"
      set new_frame [[$self cget -fshowitem] \
	    $ident $objectid $containerid $state "list"]
    }

    bind $new_frame <<Ok>> \
      "$self ShowList Ok $state $new_frame"
    	  
    bind $new_frame <<Cancel>> \
      "$self ShowList Cancel $state $new_frame"
  }

  #/////////////////////////////////////////////////////////////////////////////
  method ShowList {result state previous_frame} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "ShowList:result=$result"
    bind $previous_frame <<Ok>> ""
    bind $previous_frame <<Cancel>> ""
	
    ::MMSGui::ClearMMSDetails
    puts "fshowlist"
    if {[$self cget -fshowmode] eq 0} {
      puts "fshowlist fshowmode=0"
      [$self cget -fshowlist] [$tree cget -container]
    } {
      set containerid [$tree cget -containerid]
      puts "fshowlist fshowmode=1 containerid=$containerid"
      [$self cget -fshowlist] $containerid
    }
	  
	if {$result eq "Cancel"} {
	  $tree MakeActiveItem $mms_item(sel)
	} else {       ;# Ok
	  if {$state eq "Edit"} {
	    $tree MakeActiveItem $mms_item(sel)
	  } else {     ;# New, Copy
	    $tree MakeActiveLast
	  }
	}
	$tree _applyFilter
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method Delete {} {
  #/////////////////////////////////////////////////////////////////////////////

    set references [::MMSGui::GetRefSelected $tree]
	set result [::MenuManager::invokeCommand \
	  [$self cget -cmddelete] $references]
	#puts "Delete:result=$result"
	if {$result == 1} {
	  $self Init
	}
  }

  #/////////////////////////////////////////////////////////////////////////////
  method ChangeActiveItem {new_ident} {
  #/////////////////////////////////////////////////////////////////////////////
  
    set id_active [$tree item id active]

    $tree item element configure $id_active first eTXT -text $new_ident
    set mms_item(sel) $new_ident
  }

}

}
