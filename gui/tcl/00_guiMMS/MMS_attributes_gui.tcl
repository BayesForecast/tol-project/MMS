#/////////////////////////////////////////////////////////////////////////////
# FILE    : MMS_attributes_gui.tcl
# PURPOSE : This file implements aspects of the interface
#           to manipulate the attributes of objects of MMS
#/////////////////////////////////////////////////////////////////////////////

package require Tk
package require snit

namespace eval ::MMSAttributesGui {

#/////////////////////////////////////////////////////////////////////////////
::snit::widget bmmsattributes {
# PURPOSE : Defines the snit widget used to
#           create a list of attributes
#/////////////////////////////////////////////////////////////////////////////

  #typevariable

  variable widgets

  # State (disabled, normal)
  option -state \
    -default "disabled" -configuremethod "_conf-state"

  variable _attributes
  # Attributes to be edited at the tablelist
    
  variable tablelist_options
  
  component dlg

  delegate method * to hull
  delegate option * to hull

  #typeconstructor

  #///////////////////////////////////////////////////////////////////////////
  constructor { args } {
  #///////////////////////////////////////////////////////////////////////////
 
    install dlg as frame $win.d 

    $self _init
    
    $self _create

    # Apply all options passed at creation time.
    $self configurelist $args
    
    grid columnconfigure $win 0 -weight 1
    grid rowconfigure    $win 0 -weight 1
    grid $win -sticky nsew

    return $win
  }

  #///////////////////////////////////////////////////////////////////////////
  method put_state {s} {
  #///////////////////////////////////////////////////////////////////////////

    $widgets(tbl) configure -state $s
    set lrow [$widgets(tbl) size]
    for {set _r 0} {$_r < $lrow} {incr _r} {
      set btn_minus [$widgets(tbl) windowpath $_r,col_bminus]
      $btn_minus configure -state $s
      set btn_plus [$widgets(tbl) windowpath $_r,col_bplus]
      if {$btn_plus ne ""} {
	    $btn_plus configure -state $s
	  }
    }
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _conf-state { _ s } {
  #///////////////////////////////////////////////////////////////////////////

    set options(-state) $s
	$self put_state $s
  }
  
  #///////////////////////////////////////////////////////////////////////////
  method _init {} {
  #
  # PURPOSE: Initializes the options for the tablelist
  #///////////////////////////////////////////////////////////////////////////
 
    set _attributes [list]

    set tablelist_options(-label) [mc "Attributes"]
    set tablelist_options(-columns) [list \
      0 [mc "Name"] left \
      0 [mc "Value"] left \
      0 "" center \
      0 "" center \
    ]
    set tablelist_options(-listvar) [myvar _attributes]
  }

  #///////////////////////////////////////////////////////////////////////////
  method _create { } {
  #
  # PURPOSE: Creates the contents of a frame in order to
  #          to edit a list of attributes
  #///////////////////////////////////////////////////////////////////////////
   
    set ft $dlg
    # Tablelist frame

    tablelist::tablelist $ft.tbl \
      -columns $tablelist_options(-columns) \
      -listvariable $tablelist_options(-listvar) \
      -xscrollcommand "$ft.xs set" \
      -yscrollcommand "$ft.ys set" \
      -height 10 -width 60
    set widgets(tbl) $ft.tbl

	bind $widgets(tbl) <<TablelistCellUpdated>> \
	  "event generate $win <<OnChanged>>"
	
    scrollbar $ft.ys -orient v -command "$ft.tbl yview"
    scrollbar $ft.xs -orient h -command "$ft.tbl xview"

    grid $ft.tbl  -row 0 -column 0 -sticky nsew
    grid $ft.ys   -row 0 -column 1 -sticky ns
    grid $ft.xs   -row 2 -column 0 -sticky ew

    grid rowconfigure $ft 0 -weight 1
    grid columnconfigure $ft 0 -weight 1
    grid $ft -sticky news
    
    $widgets(tbl) columnconfigure 0 -name col_name -editable yes
    $widgets(tbl) columnconfigure 1 -name col_scen -editable yes -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 2 -name col_bminus -hide true -labelcommand "$self DoNothing"
    $widgets(tbl) columnconfigure 3 -name col_bplus -labelimage [::Bitmap::get plus] \
	                                -labelcommand "$self AddRow"
    
    foreach {w} {tbl} {
      bind $widgets($w) <Escape> "$self Cancel"
    }
  }

  #/////////////////////////////////////////////////////////////////////////////
  method DoNothing {args} {
  #/////////////////////////////////////////////////////////////////////////////
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method ConfigureColumns {nrows} {
  #/////////////////////////////////////////////////////////////////////////////

    if {$nrows>0} {
	  $widgets(tbl) columnconfigure col_bminus -hide false
	  $widgets(tbl) columnconfigure col_bplus -labelimage "" \
	    -labelcommand "$self DoNothing"
      $widgets(tbl) cellconfigure end,col_bplus -window [mymethod CreateButtonPlus]
	  if {$nrows>=2} {
        $widgets(tbl) cellconfigure [expr $nrows-2],col_bplus -window ""
	  }

    } else {
	  $widgets(tbl) columnconfigure col_bminus -hide true
	  $widgets(tbl) columnconfigure col_bplus -labelimage [::Bitmap::get plus] \
	    -labelcommand "$self AddRow"
    }
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method DeleteRow {row} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "DeleteRow:row=$row"
    $widgets(tbl) delete [list $row]
    
    set lrow [$widgets(tbl) size]
    for {set _r $row} {$_r < $lrow} {incr _r} {
      #puts "DeleteRow:_r=$_r,windowpath=[$widgets(tbl) windowpath $_r,1]"
      set btn_minus [$widgets(tbl) windowpath $_r,col_bminus]
      $btn_minus configure -command [list $self DeleteRow $_r]
    }
	$self ConfigureColumns $lrow
    
	event generate $win <<OnDeleted>>
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method AddRow {args} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "AddRow:enter"
	$widgets(tbl) finishediting
	update
    $widgets(tbl) insert end [list "" "" ""]

    set lrow [expr [$widgets(tbl) size]-1]
    $widgets(tbl) cellconfigure $lrow,col_bplus -window [mymethod CreateButtonPlus]
    $widgets(tbl) cellconfigure $lrow,col_bminus -window [mymethod CreateButtonMinus]
    $widgets(tbl) editcell $lrow,0

	$self ConfigureColumns [$widgets(tbl) size]
    
	event generate $win <<OnAdded>>
    #puts "AddRow:exit"
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method CreateButtonMinus {tbl row col w} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "createButton:w=$w, row=$row"
    button $w -image [::Bitmap::get minus] -highlightthickness 0 -takefocus 0 \
      -command [list $self DeleteRow $row]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method CreateButtonPlus {tbl row col w} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "createButton:w=$w, row=$row"
    button $w -image [::Bitmap::get plus] -highlightthickness 0 -takefocus 0 \
      -command [list $self AddRow]
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Ok {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnAccept>>
  }

  #/////////////////////////////////////////////////////////////////////////////
  method Cancel {} {
  #/////////////////////////////////////////////////////////////////////////////

    event generate $win <<OnCancel>>    
  }

  #/////////////////////////////////////////////////////////////////////////////
  method set_info {iteminfo} {
  #/////////////////////////////////////////////////////////////////////////////

    #puts "set_info:iteminfo=$iteminfo"
	set _state [$self cget -state]
	$self put_state "normal"

    $widgets(tbl) delete 0 end
    set idx_fixed [expr [$widgets(tbl) columncount]-2] 
    set row 0
    foreach {r} $iteminfo {
      $widgets(tbl) insert end [lrange $r 0 $idx_fixed]
      $widgets(tbl) cellconfigure $row,col_bminus -window [mymethod CreateButtonMinus]
      incr row
    }
	$self ConfigureColumns $row
	
	$self put_state $_state
  }

  #/////////////////////////////////////////////////////////////////////////////
  method get_info {what} {
  #/////////////////////////////////////////////////////////////////////////////

    set idx_fixed [expr [$widgets(tbl) columncount]-2] 
    set _info [list]
    foreach {r} $_attributes {
      lappend _info [lrange $r 0 [expr $idx_fixed-1]]
    }
    #puts "get_info:_info=$_info"
    return $_info
  }
  
  #/////////////////////////////////////////////////////////////////////////////
  method finishediting {} {
  #/////////////////////////////////////////////////////////////////////////////

    $widgets(tbl) finishediting
  }
  
}

}
