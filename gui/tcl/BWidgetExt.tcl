package require BWidget

Bitmap::use

namespace eval Bitmap {
  # registra una imagen ya creada
  proc register { name { img  ""}} {
    variable _bmp
    
    if { [ info exists _bmp($name) ] } {
      error "image '$name' is already registered"
    }
    if { $img eq "" } {
      set img $name
    }
    if { [ lsearch [ image names ] $img ] == -1 } {
      error "'$img' is not a valid image"
    } else {
      set _bmp($name) $img
    }
    return $_bmp($name)
  }
}
