#/////////////////////////////////////////////////////////////////////////////
# FILE    : MMS.tcl
# PURPOSE : Main file of MMS
#/////////////////////////////////////////////////////////////////////////////


if { [ catch {package present Tk} ] || ![namespace exists ::TolInspector] } {
  # solo evaluo este archivo si estoy dentro de tolbase
  return
}

namespace eval ::MMS {
  variable mydir [ file dir [ file normalize [ info script ] ] ]

  proc loadTcl { filePath } {
    variable _filePath $filePath
    namespace eval :: source [list [file join $MMS::mydir $MMS::_filePath]]
  }
}

namespace eval :: {
  
  lappend auto_path [ file join $MMS::mydir lib ]

  # el paquete autoscroll se carga de una forma rara en
  # tolbase/lib/byswidget/bfunctions/
  package forget autoscroll

  package require Swaptablelist
  package require CollapsableFrame
  
  package require wtree
  package require wtransient

  #package require renderpane
  #package require mimetex
  #package require textutil::expander
  package require nbdbmanager
  package require markupviewer
  
  if { ![ namespace exists MarkupHelper ] } {
    MMS::loadTcl "markuphelper.tcl"
  }
   
  MMS::loadTcl "datefield.tcl"
  #package require datefield
  
  MMS::loadTcl "BWidgetExt.tcl"
  
  MMS::loadTcl "00_guiMMS/MMS_gui.tcl"
  MMS::loadTcl "00_guiMMS/MMS_layer_gui.tcl"

  MMS::loadTcl "00_guiMMS/MMS_selector_gui.tcl"
  MMS::loadTcl "00_guiMMS/MMS_container_gui.tcl"
  MMS::loadTcl "00_guiMMS/MMS_attributes_gui.tcl"
  
  MMS::loadTcl "00_guiMMS/MMS_connection_gui.tcl"
  MMS::loadTcl "00_guiMMS/MMS_layer_gui_connection.tcl"

  MMS::loadTcl "00_guiMMS/MMS_repository_gui.tcl"
  MMS::loadTcl "00_guiMMS/MMS_layer_gui_repository.tcl"

  MMS::loadTcl "00_guiMMS/MMS_layer_gui_wizard.tcl"

  MMS::loadTcl "01_variable/dataset_gui.tcl"
  MMS::loadTcl "01_variable/dataset_layer_gui.tcl"

  MMS::loadTcl "01_variable/variable_gui.tcl"
  MMS::loadTcl "01_variable/variable_layer_gui.tcl"

  MMS::loadTcl "01_variable/variable_i_gui.tcl"
  MMS::loadTcl "01_variable/variable_i_layer_gui.tcl"

  MMS::loadTcl "01_variable/variable_d_gui.tcl"
  MMS::loadTcl "01_variable/variable_d_layer_gui.tcl"

  MMS::loadTcl "01_variable/v_scenarios_gui.tcl"
  MMS::loadTcl "01_variable/v_scenarios_layer_gui.tcl"
  
  MMS::loadTcl "02_guiMDM/MDM_gui.tcl"
  MMS::loadTcl "02_guiMDM/MDM_layer_gui.tcl"
  
  MMS::loadTcl "02_guiMDM/MDM_mvariable_gui.tcl"
  MMS::loadTcl "02_guiMDM/MDM_layer_gui_mvariable.tcl"
  
  MMS::loadTcl "02_guiMDM/MDM_submodel_gui.tcl"
  MMS::loadTcl "02_guiMDM/MDM_layer_gui_submodel.tcl"
  
  MMS::loadTcl "02_guiMDM/MDM_transformation_gui.tcl"
  MMS::loadTcl "02_guiMDM/MDM_layer_gui_transformation.tcl"
  
  MMS::loadTcl "02_guiMDM/MDM_expterm_gui.tcl"
  MMS::loadTcl "02_guiMDM/MDM_layer_gui_expterm.tcl"

  MMS::loadTcl "02_guiMDM/MDM_parameter_gui.tcl"
  MMS::loadTcl "02_guiMDM/MDM_layer_gui_parameter.tcl"
  
  MMS::loadTcl "02_guiMDM/MDM_mcombination_gui.tcl"
  MMS::loadTcl "02_guiMDM/MDM_layer_gui_mcombination.tcl"
  
  MMS::loadTcl "02_guiMDM/MDM_constraint_gui.tcl"
  MMS::loadTcl "02_guiMDM/MDM_layer_gui_constraint.tcl"

  MMS::loadTcl "02_guiMDM/MDM_prior_gui.tcl"
  MMS::loadTcl "02_guiMDM/MDM_layer_gui_prior.tcl"
  
  MMS::loadTcl "02_guiMDM/MDM_hierarchy_gui.tcl"
  MMS::loadTcl "02_guiMDM/MDM_layer_gui_hierarchy.tcl"

  MMS::loadTcl "02_guiMDM/MDM_hierterm_gui.tcl"
  MMS::loadTcl "02_guiMDM/MDM_layer_gui_hierterm.tcl"

  MMS::loadTcl "02_guiMDM/MDM_mequivalence_gui.tcl"
  MMS::loadTcl "02_guiMDM/MDM_layer_gui_mequivalence.tcl"

  MMS::loadTcl "03_estimation/estimation_gui.tcl"
  MMS::loadTcl "03_estimation/estimation_layer_gui.tcl"
  
  MMS::loadTcl "03_estimation/modelresults_gui.tcl"
  MMS::loadTcl "03_estimation/modelresults_layer_gui.tcl"
  
  MMS::loadTcl "03_estimation/submodelresults_gui.tcl"
  MMS::loadTcl "03_estimation/submodelresults_layer_gui.tcl"
  
  MMS::loadTcl "03_estimation/exptermresults_gui.tcl"
  MMS::loadTcl "03_estimation/exptermresults_layer_gui.tcl"
  
  MMS::loadTcl "03_estimation/hierarchyresults_gui.tcl"
  MMS::loadTcl "03_estimation/hierarchyresults_layer_gui.tcl"

  MMS::loadTcl "03_estimation/parameterresults_gui.tcl"
  MMS::loadTcl "03_estimation/parameterresults_layer_gui.tcl"
  
  MMS::loadTcl "03_estimation/mcombinationresults_gui.tcl"
  MMS::loadTcl "03_estimation/mcombinationresults_layer_gui.tcl"
  
  MMS::loadTcl "03_estimation/settings_gui.tcl"
  MMS::loadTcl "03_estimation/settings_layer_gui.tcl"
  
  MMS::loadTcl "03_estimation/conditioning_gui.tcl"
  MMS::loadTcl "03_estimation/conditioning_layer_gui.tcl"
  
  MMS::loadTcl "03_estimation/parameterestim_gui.tcl"
  MMS::loadTcl "03_estimation/parameterestim_layer_gui.tcl"
  
  MMS::loadTcl "03_estimation/forecast_gui.tcl"
  MMS::loadTcl "03_estimation/forecast_layer_gui.tcl"
  
  MMS::loadTcl "03_estimation/modelforecast_gui.tcl"
  MMS::loadTcl "03_estimation/modelforecast_layer_gui.tcl"
  
  MMS::loadTcl "03_estimation/submodelforecast_gui.tcl"
  MMS::loadTcl "03_estimation/submodelforecast_layer_gui.tcl"
  
  MMS::loadTcl "03_estimation/hierarchyforecast_gui.tcl"
  MMS::loadTcl "03_estimation/hierarchyforecast_layer_gui.tcl"

  MMS::loadTcl "03_estimation/parameterforecast_gui.tcl"
  MMS::loadTcl "03_estimation/parameterforecast_layer_gui.tcl"

  MMS::loadTcl "03_estimation/finterval_gui.tcl"
  MMS::loadTcl "03_estimation/finterval_layer_gui.tcl"

  MMS::loadTcl "03_estimation/fscenario_gui.tcl"
  MMS::loadTcl "03_estimation/fscenario_layer_gui.tcl"

  MMS::loadTcl "02_combination/combination_gui.tcl"
  MMS::loadTcl "02_combination/layer_gui_combination.tcl"

  MMS::loadTcl "03_fit/fit_gui.tcl"
  MMS::loadTcl "03_fit/fit_layer_gui.tcl"
}

#/////////////////////////////////////////////////////////////////////////////
proc ::MMS::MMS {} {
#/////////////////////////////////////////////////////////////////////////////
  variable mydir

  lappend ::Bitmap::path [file join $mydir .. images]

  namespace eval :: {
    ::msgcat::mcload [file join $MMS::mydir msgs]
    #project::UseTolCon
  }
}

#/////////////////////////////////////////////////////////////////////////////
proc ::MMS::AddButtonBar {} {
#/////////////////////////////////////////////////////////////////////////////
  update
  
  set defaultTF $::project::widgets(mainframe).toolbarFrame_0
  
  if {[winfo exists $defaultTF]} {
    set TFc $defaultTF
    set tb1 $TFc.tb_tbCustom
	
  } else {
    set TFc [::TolConsole::NewToolFrame 20 0]
    set tb1 [::TolConsole::NewToolBarB $TFc tb1] 

  }
  ::TolConsole::NewButtonB $tb1 "expl_mms" \
	[mc "Open MMS Interface"] "::MMSGui::ShowMMSContainer" $TFc
}

proc ::MMS::InitImageData { } {
  set images [ list ]
  image create photo connecting16 -data {
    R0lGODlhEAAQAIMAAPwCBPz+BMTCBISCBAQCBPz+/MTCxMTGxISChFxaXMzG
    zKSipAAAAAAAAAAAAAAAACH5BAEAAAAALAAAAAAQABAAAARaEMhJZbihUiz6
    0FPnfaA0iBpBVGdHEYWxToEoSHBRHHM9AgSEQRcj+AYkYAJxIPKQFUJiOdTJ
    QFIDU6dYzKKFhTCM+E5g4mXaDAyrlogEG+DGTecA7wsP8EcAACH+aENyZWF0
    ZWQgYnkgQk1QVG9HSUYgUHJvIHZlcnNpb24gMi41DQqpIERldmVsQ29yIDE5
    OTcsMTk5OC4gQWxsIHJpZ2h0cyByZXNlcnZlZC4NCmh0dHA6Ly93d3cuZGV2
    ZWxjb3IuY29tADs=
  }
  lappend images connecting16
  image create photo connectno16 -data {
    R0lGODlhEAAQAIMAAPwCBPz+BMTCBAQCBERCBPz+/MTCxMTGxISChFxaXMzG
    zKSipAAAAAAAAAAAAAAAACH5BAEAAAAALAAAAAAQABAAAARPEMhJq704axBE
    HoPUEdhQGMNYFuwxkKInDYjBniEnwMCQIIebSzXx/WwsFK+YMABZikWuYlrU
    CtZpEYv4WRPaHhb064YB41kCfJFSQBh/BAAh/mhDcmVhdGVkIGJ5IEJNUFRv
    R0lGIFBybyB2ZXJzaW9uIDIuNQ0KqSBEZXZlbENvciAxOTk3LDE5OTguIEFs
    bCByaWdodHMgcmVzZXJ2ZWQuDQpodHRwOi8vd3d3LmRldmVsY29yLmNvbQA7
  }
  lappend images connectno16
  image create photo connectyes16 -data {
    R0lGODlhEAAQAIMAAPwCBAQCBPz+/KSipDQyNMTCxMTGxISChFxaXMzGzAAA
    AAAAAAAAAAAAAAAAAAAAACH5BAEAAAAALAAAAAAQABAAAARaEMhJZxCjgsAt
    DtUlCOA1gJQ4kl/IDatAoF7xxkS6GgEBr6jAobCyBX42SQBxMOx6A8MhiGAS
    R8YDgrYsNEeJ0zaEGZY7uoH2oB6nOUwtbdLaVOeTUwo/8UcAACH+aENyZWF0
    ZWQgYnkgQk1QVG9HSUYgUHJvIHZlcnNpb24gMi41DQqpIERldmVsQ29yIDE5
    OTcsMTk5OC4gQWxsIHJpZ2h0cyByZXNlcnZlZC4NCmh0dHA6Ly93d3cuZGV2
    ZWxjb3IuY29tADs=
  }
  lappend images connectyes16
  foreach img $images {
    Bitmap::register $img
  }
}

MMS::MMS
MMS::InitImageData
MMS::AddButtonBar
