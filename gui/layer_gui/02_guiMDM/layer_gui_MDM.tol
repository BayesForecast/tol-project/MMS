//////////////////////////////////////////////////////////////////////////////
// FILE:    layer_gui_MDM.tol
// PURPOSE: Definition of the communication layer between MMS and the GUI for
//          MDM (module model definition)
//////////////////////////////////////////////////////////////////////////////

@Module MDMGui = [[

Real _void = 0;

#Embed "layer_gui_MDM_mvariable.tol";
#Embed "layer_gui_MDM_submodel.tol";
#Embed "layer_gui_MDM_transformation.tol";
#Embed "layer_gui_MDM_expterm.tol";
#Embed "layer_gui_MDM_parameter.tol";
#Embed "layer_gui_MDM_mcombination.tol";
#Embed "layer_gui_MDM_constraint.tol";
#Embed "layer_gui_MDM_prior.tol";
#Embed "layer_gui_MDM_hierarchy.tol";
#Embed "layer_gui_MDM_hierterm.tol";
#Embed "layer_gui_MDM_mequivalence.tol";

/*
  # Data of the model to edit
    # (Class @Model)
    # model_info(name)          - Name (Text _.name)
    # model_info(vers)          - Version (Text _.version)
    # model_info(dCre)          - Creation date (Date _.creationTime)
    # model_info(dMod)          - (Date _.modificationTime)
    # model_info(dNow)          - Current system date
	# model_info(saved)         - Is saved? (Real _.isSaved)
	# model_info(source)        - (Text _.source)
    #                           - (Set _.dataSet)
    # model_info(dsets)         - DataSet Identifiers (For New and Edit)
    # model_info(desc)          - Description (H: Text _.description)
    # model_info(attr)          - Attributes (H: Set _.attributes)
    # model_info(tags)          - Tags (H: Set _.tags)
    # model_info(st_tags)       - Tags as text
    #                           - (Set _.mVariables)
    #                           - (Set _.submodels)
    #                           - (Set _.parameters)
    #                           - (Set _.hierarchies)
    #                           - (Set _.mCombinations)
    #                           - (Set _.mEquivalences)
    #                           - (Set _parent_; // Empty | @Estimation | @Forecast)
*/

////////////////////////////////////////////////////////////////////////////
Set GetInfoModel(@Model mod)
////////////////////////////////////////////////////////////////////////////
{
  Text name = mod::GetName(0);
  Text vers = mod::GetVersion(0);
  Text desc = mod::GetDescription(0);
  Date datC = mod::GetCreationTime(?);
  Text dCre = FormatDate(datC, "%Y/%m/%d");
  Date datM = mod::GetModificationTime(?);
  Text dMod = FormatDate(datM, "%Y/%m/%d");
  Text source = mod::GetSource(?);
  Real saved = mod::IsSaved(?);
  Text abs_id = mod::GetAbsoluteIdentifier(?);
  
  Set attr = EvalSet(mod::GetAttributes(?), Set (Anything attribute) {
    [[Name(attribute), attribute]]
  }); 

  Set SetOfAnything(name, vers, desc, dCre, dMod, source, saved, attr, abs_id)
};
   
////////////////////////////////////////////////////////////////////////////
@Model FindModel(Set container)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = Case(
    container[1]=="MMS", {
      @Model Container::GetModel(container[2])
	},
    container[1]=="Estimation", {
      @Estimation estim = Container::GetEstimation(container[2]);
      @Model estim::GetModel(?)
	},
    container[1]=="Forecast", {
      @Forecast forecast = Container::GetForecast(container[2]);
      @Model forecast::GetModel(?)
	}
  )
};

////////////////////////////////////////////////////////////////////////////
Set GetModel(Text ident, Set container)
////////////////////////////////////////////////////////////////////////////
{
  Set model_container = If(Card(container)==1, {
    container << [[ident]]
  }, container);
  @Model mod = FindModel(model_container);

  Set GetInfoModel(mod)
};

////////////////////////////////////////////////////////////////////////////
Set GetModelsList(Text details)
////////////////////////////////////////////////////////////////////////////
{
  Set setInf = EvalSet(Container::GetModels(?), Set (@Model mod) {
    Text ident = mod::GetIdentifier(?);
    Text abs_id = mod::GetAbsoluteIdentifier(?);
    Real saved = mod::IsSaved(?);
    Set If(details == "no", {
      Set SetOfAnything(ident, abs_id, saved)
	}, {
      Text name = mod::GetName(?);
      Text vers = mod::GetVersion(?);
      Text desc = mod::GetDescription(?);
      Date datC = mod::GetCreationTime(?);
      Text dCre = FormatDate(datC, "%Y/%m/%d");
      Date datM = mod::GetModificationTime(?);
      Text dMod = FormatDate(datM, "%Y/%m/%d");
      Text source = mod::GetSource(?);
      Set SetOfAnything(ident, name, vers, desc, dCre, dMod, source, abs_id, saved)
	})
  })
};    

////////////////////////////////////////////////////////////////////////////
Text GetModelInfo(Text ident, Set container)
////////////////////////////////////////////////////////////////////////////
{
  Set model_container = If(Card(container)==1, {
    container << [[ident]]
  }, container);
  @Model mod = FindModel(model_container);
  
  Text info = If(ObjectExist("Code", "mod::_.get.autodoc.info"),
    mod::_.get.autodoc.info(?), ""
  )
};

////////////////////////////////////////////////////////////////////////////
Text GetModelHelp(Real void)
////////////////////////////////////////////////////////////////////////////
{
  Text help = If(ObjectExist("Text", "MMS::@Model::_.autodoc.help"),
    MMS::@Model::_.autodoc.help, ""
  )
};

////////////////////////////////////////////////////////////////////////////
Set GetModelDataSet(Set container)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = FindModel(container);
  @DataSet dset = mod::GetDataSet(?);
  Text ident = dset::GetIdentifier(?);
  Text abs_id = dset::GetAbsoluteIdentifier(?);
  Set SetOfAnything(ident, abs_id)
};

////////////////////////////////////////////////////////////////////////////
Set GetModelSummary(Text ident, Set container)
////////////////////////////////////////////////////////////////////////////
{
  Set model_container = If(Card(container)==1, {
    container << [[ident]]
  }, container);
  @Model mod = FindModel(model_container);

  Set mod::GetSummary(?)  
};

////////////////////////////////////////////////////////////////////////////
@MObjectPrior GetMObjectPrior(Text object, Set container)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = FindModel(container);
  
  @MObjectPrior obj = If(mod::FindParameter(object), mod::GetParameter(object), 
	If(mod::FindMCombination(object), mod::GetMCombination(object),
	  mod::GetMEquivalence(object)
	)
  )
};

////////////////////////////////////////////////////////////////////////////
Real CreateModelTest(Set info, Set dsets, Set attributes)
////////////////////////////////////////////////////////////////////////////
{
  WriteLn("CreateModelTest");
  WriteLn("name:"+info["name"]);
  WriteLn("vers:"+info["vers"]);
  WriteLn("desc:"+info["desc"]);
  WriteLn("dsets:");
  Set For( 1, Card(dsets), Real (Real i) {
    WriteLn(dsets[i]);
    Real 1
  } );
  WriteLn("Attributes:");
  Set For(1, Card(attributes), Real (Real i) {
    WriteLn(attributes[i][1]+":"+attributes[i][2]);
    Real 1
  });
  Real 1
};

////////////////////////////////////////////////////////////////////////////
Real CreateModel(Set info, Set dsets, Set attributes)
////////////////////////////////////////////////////////////////////////////
{
  Set s_attributes = For(1, Card(attributes), Anything (Real i) {
    Anything PutLocalName(attributes[i][1], attributes[i][2])
  });

  NameBlock args = [[
    Text _.name         = info["name"];
    Text _.version      = info["vers"];
    Text _.description  = info["desc"];
	Set  _.dataSets     = dsets;
    Set _.attributes    = s_attributes
  ]];
  
  @Model mod = Container::CreateModel(args);
  Real 1
};

////////////////////////////////////////////////////////////////////////////
Real CopyModelTest(Set info)
////////////////////////////////////////////////////////////////////////////
{
  WriteLn("CopyModelTest");
  WriteLn("ident:"+info["ident"]);
  WriteLn("name:"+info["name"]);
  WriteLn("vers:"+info["vers"]);
  WriteLn("desc:"+info["desc"]);
  Real 1
};

////////////////////////////////////////////////////////////////////////////
Real CopyModel(Set info)
////////////////////////////////////////////////////////////////////////////
{
  @Model prev_mod = Container::GetModel(info["ident"]);
  // Se crea un nuevo objeto con un identificador v�lido
  @Model mod = prev_mod::Duplicate(?);
  // Si estos cambios no son v�lidos se mostrar�n errores  
  Real mod::SetName(info["name"]);
  Real mod::SetVersion(info["vers"]);
  Real mod::SetDescription(info["desc"]);
  Real 1
};

////////////////////////////////////////////////////////////////////////////
Real EditModelTest(Set info, Set dsets, Set attributes)
////////////////////////////////////////////////////////////////////////////
{
  WriteLn("EditModelTest");
  WriteLn("name:"+info["name"]);
  WriteLn("vers:"+info["vers"]);
  WriteLn("desc:"+info["desc"]);
  WriteLn("dsets:");
  Set For( 1, Card(dsets), Real (Real i) {
    WriteLn(dsets[i]);
    Real 1
  } );
  WriteLn("Attributes:");
  Set For(1, Card(attributes), Real (Real i) {
    WriteLn(attributes[i][1]+":"+attributes[i][2]);
    Real 1
  });
  Real 1
};

////////////////////////////////////////////////////////////////////////////
Real EditModel(Set info, Set dsets, Set attributes)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = Container::GetModel(info["ident"]);
  Real mod::SetDescription(info["desc"]);
  Real mod::SetName(info["name"]);
  Real mod::SetVersion(info["vers"]);

  Real mod::RemoveAttributes(?);
  Set For(1, Card(attributes), Real (Real i) {
    mod::SetAttribute(attributes[i][1], attributes[i][2]);
    Real 1
  });
	
  Set datasets = EvalSet(dsets, @DataSet (Text id) {
    @DataSet Container::GetDataSet(id)
  });
  Real mod::JoinDataSets(datasets)  
}

]];
