//////////////////////////////////////////////////////////////////////////////
// FILE:    layer_gui_MDM_mequivalence.tol
// PURPOSE: Definition of the communication layer between MMS and the GUI for
//          MDM (Mequivalences)
//////////////////////////////////////////////////////////////////////////////

/*
  # MEquivalence data to be edited
   # (Class @MEquivalence)           - (: @MElement)
    # equivalence_info(name)         - Equivalence name (Text _.name)
    # equivalence_info(desc)         - Description (Text _.description)
    # equivalence_info(g_active)     - Global active (IsActive method)
    # equivalence_info(l_active)     - Local active (H: Real _.isActive)
    #                                - (Set _.parameters)(@Parameter parameter)
    # equivalence_info(terms)        - List of terms (Parameter name, Type)
    # equivalence_info(prior)        - Prior {Mean Sigma IsActive} (H: Set _.prior)
	# equivalence_info(constraint)   - Constraint {InferiorValue SuperiorValue IsActive} (H: Set _.constraint)
*/

////////////////////////////////////////////////////////////////////////////
Set GetInfoMEquivalence(@MEquivalence equiv)
////////////////////////////////////////////////////////////////////////////
{
  Text name = equiv::GetName(?);
  Text desc = equiv::GetDescription(?);
  Real l_active = equiv::GetIsActive(?);
  Real g_active = equiv::IsActive(?);
  Text abs_id = equiv::GetAbsoluteIdentifier(?);

  // Se accede a los identificadores y no a los par�metros, 
  // pues �stos pueden no estar enlazados: v�ase #604
  Set parametersId = equiv::GetParameters.Identifier(?);
  
  Set terms = For(1, Card(parametersId), Set (Real i) {
    Text ident = parametersId[i];
    Text type = @Parameter::Subclass(ident);
    Set [[ident, type]]
  });

  Text mean = "";
  Text sigma = "";
  Real p_active = 0;
  Real If(equiv::HasPrior(?), {
    @Prior pri = equiv::GetPrior(?);
    Text mean := FormatReal(pri::GetMean(?));
    Text sigma := FormatReal(pri::GetSigma(?));
    Real p_active := pri::GetIsActive(?);
	Real 1
  });
  Set prior = SetOfAnything(mean, sigma, p_active);

  Text inf = "";
  Text sup = "";
  Real c_active = 0;
  Real If(equiv::HasConstraint(?), {
    @Constraint cnst = equiv::GetConstraint(?);
    Text inf := FormatReal(cnst::GetInferiorValue(?));
    Text sup := FormatReal(cnst::GetSuperiorValue(?));
    Real c_active := cnst::GetIsActive(?);
	Real 1
  });
  Set constraint = SetOfAnything(inf, sup, c_active);

  Set SetOfAnything(name, desc, l_active, g_active,
                    terms, prior, constraint, abs_id)
};
   
////////////////////////////////////////////////////////////////////////////
Set GetMEquivalence(Text ident, Set container)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = FindModel(container);
  @MEquivalence equiv = mod::GetMEquivalence(ident);
  Set GetInfoMEquivalence(equiv)
};

////////////////////////////////////////////////////////////////////////////
Set GetMEquivalencesList(Set container, Text details)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = FindModel(container);

  Set setInf = EvalSet(mod::GetMEquivalences(?), Set (@MEquivalence equiv) {
    Text ident = equiv::GetIdentifier(?);
    Text abs_id = equiv::GetAbsoluteIdentifier(?);
	
    Set If(details == "no", {
      Set SetOfAnything(ident, abs_id)
	}, {
      Text name = equiv::GetName(?);
      Text desc = equiv::GetDescription(?);
	
      Real nterms = Card(equiv::GetParameters(?));
  
      Text mean = "";
      Text sigma = "";
      Real If(equiv::HasPrior(?), {
        @Prior pri = equiv::GetPrior(?);
        Text mean := FormatReal(pri::GetMean(?));
        Text sigma := FormatReal(pri::GetSigma(?));
	    Real 1
      });

      Text inf = "";
      Text sup = "";
      Real If(equiv::HasConstraint(?), {
        @Constraint cnst = equiv::GetConstraint(?);
        Text inf := FormatReal(cnst::GetInferiorValue(?));
        Text sup := FormatReal(cnst::GetSuperiorValue(?));
	    Real 1
      });

      Set SetOfAnything(ident, name, nterms, desc,
	                    mean, sigma, inf, sup, abs_id)
	})
  })
};    

////////////////////////////////////////////////////////////////////////////
Text GetMEquivalenceHelp(Real void)
////////////////////////////////////////////////////////////////////////////
{
  Text help = If(ObjectExist("Text", "MMS::@MEquivalence::_.autodoc.help"),
    MMS::@MEquivalence::_.autodoc.help, ""
  )
};

////////////////////////////////////////////////////////////////////////////
Real CreateMEquivalenceTest(Set info, Set terms, Set container,
                            Set prior, Set constraint)
////////////////////////////////////////////////////////////////////////////
{
  WriteLn("CreateMEquivalenceTest");
  WriteLn("name:"+info["name"]);
  WriteLn("desc:"+info["desc"]);
  WriteLn("l_active:"+info["l_active"]);
  WriteLn("terms:");
  Set For(1, Card(terms), Real (Real i) {
    WriteLn("par:"+terms[i][1]);
    WriteLn("type:"+terms[i][2]);
    Real 1
  });
  WriteLn("mean:"+prior[1]);
  WriteLn("sigma:"+prior[2]);
  WriteLn("active:"+prior[3]);
  WriteLn("inf:"+constraint[1]);
  WriteLn("sup:"+constraint[2]);
  WriteLn("active:"+constraint[3]);
  Real 1
};

////////////////////////////////////////////////////////////////////////////
Real CreateMEquivalence(Set info, Set terms, Set container,
                        Set prior, Set constraint)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = FindModel(container);

  NameBlock args = [[

    Text _.name = info["name"];
    Text _.description = info["desc"];
    Real _.isActive = MMS::Layer::Text2Real(info["l_active"]);

    Set _.parameters = EvalSet(terms, Text (Set t) {
      Text t[1]
    })
  ]];

  Text mean = prior[1];
  Text sigma = prior[2];
  Real If(And(mean!="", sigma!=""), {
    NameBlock _.prior = [[
      Real _.mean = MMS::Layer::Text2Real(mean);
      Real _.sigma = MMS::Layer::Text2Real(sigma);
      Real _.isActive = MMS::Layer::Text2Real(prior[3])
    ]];
    NameBlock args := SetToNameBlock(
      NameBlockToSet(args) << [[_.prior]]
    );
    Real 1
  });

  Text inf = constraint[1];
  Text sup = constraint[2];
  Real If(And(inf!="", sup!=""), {
    NameBlock _.constraint = [[
      Real _.inferiorValue = MMS::Layer::Text2Real(inf);
      Real _.superiorValue = MMS::Layer::Text2Real(sup);
      Real _.isActive = MMS::Layer::Text2Real(constraint[3])
    ]];
    NameBlock args := SetToNameBlock(
      NameBlockToSet(args) << [[_.constraint]]
    );
    Real 1
  });

  @MEquivalence mequivalence = mod::CreateMEquivalence(args);
  Real 1  
};

////////////////////////////////////////////////////////////////////////////
Real EditMEquivalenceTest(Set info, Set container, Set prior, Set constraint)
////////////////////////////////////////////////////////////////////////////
{
  WriteLn("EditMEquivalenceTest");
  WriteLn("name:"+info["name"]);
  WriteLn("desc:"+info["desc"]);
  WriteLn("l_active:"+info["l_active"]);
  WriteLn("terms:");
  Set For(1, Card(terms), Real (Real i) {
    WriteLn("par:"+terms[i][1]);
    WriteLn("type:"+terms[i][2]);
    Real 1
  });
  WriteLn("mean:"+prior[1]);
  WriteLn("sigma:"+prior[2]);
  WriteLn("active:"+prior[3]);
  WriteLn("inf:"+constraint[1]);
  WriteLn("sup:"+constraint[2]);
  WriteLn("active:"+constraint[3]);
  Real 1
};

////////////////////////////////////////////////////////////////////////////
Real EditMEquivalence(Set info, Set container, Set prior, Set constraint)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = FindModel(container);
  @MEquivalence equiv = mod::GetMEquivalence(info["ident"]);

  Real equiv::SetDescription(info["desc"]);
  Real equiv::SetIsActive(MMS::Layer::Text2Real(info["l_active"]));
  
  Text mean = prior[1];
  Text sigma = prior[2];
  Real If(And(mean!="", sigma!=""), {
    NameBlock args_prior = [[
      Real _.mean = MMS::Layer::Text2Real(mean);
      Real _.sigma = MMS::Layer::Text2Real(sigma);
      Real _.isActive = MMS::Layer::Text2Real(prior[3])
    ]];
    Real equiv::SetPrior(args_prior)
  }, {
    Real If(And(mean=="", sigma=="", equiv::HasPrior(?)), {
      Real equiv::RemovePrior(?) 
	}, 0)
  });
  
  Text inf = constraint[1];
  Text sup = constraint[2];
  Real If(And(inf!="", sup!=""), {
    NameBlock args_cnst = [[
      Real _.inferiorValue = MMS::Layer::Text2Real(inf);
      Real _.superiorValue = MMS::Layer::Text2Real(sup);
      Real _.isActive = MMS::Layer::Text2Real(constraint[3])
    ]];
    Real equiv::SetConstraint(args_cnst)
  }, {
  
    Real If(And(inf=="", sup=="", equiv::HasConstraint(?)), {
      Real equiv::RemoveConstraint(?) 
	}, 0)
  });
  
  Real equiv::SetName(info["name"])
};

////////////////////////////////////////////////////////////////////////////
Real RemoveMEquivalence(Text ident, Set container)
////////////////////////////////////////////////////////////////////////////
{
  @Model mod = FindModel(container);
  Real mod::RemoveMEquivalence(ident)
};
