//////////////////////////////////////////////////////////////////////////////
// FILE:    layer_gui_variable_d.tol
// PURPOSE: Definition of the communication layer between MMS and the GUI for
//          dependent variables management
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
@Module VariablesDGui = 
//////////////////////////////////////////////////////////////////////////////
[[
  /***************************************************************************
  Lista de los atributos de las variables I:
    * Text _.name
    * Text _.description
    * Set _.atributes
    * Set _.tags
    * Text _.type
    * Text _.expression
    > Set _.dependences
  ***************************************************************************/
 
  ////////////////////////////////////////////////////////////////////////////
  Set GetInfo(Text objectid)
  ////////////////////////////////////////////////////////////////////////////
  {
    @VariableD variableD = Eval(objectid);
    Set [[
      Text name = variableD::GetName(?);
      Text description = variableD::GetDescription(?);
      Set attributes = EvalSet(variableD::GetAttributes(?), 
        Set (Anything attribute) { [[Name(attribute), attribute]] });
      Set tags = variableD::GetTags(?);
      Text type = variableD::GetType(?);
      Text expression = variableD::GetExpression(?);
      Set dependences = If(variableD::HasDependences(?), {
	    variableD::GetDependences.Info(?)
      }, Empty);
      Text absoluteIdentifier = variableD::GetAbsoluteIdentifier(?)
    ]]
  };

  ////////////////////////////////////////////////////////////////////////////
  Set GetList(Text containerid, Text details)
  ////////////////////////////////////////////////////////////////////////////
  {
    @DataSet dataSet = Eval(containerid);
    Set EvalSet(dataSet::GetVariablesD(?), Set (@VariableD variableD) {
	  Text identifier = variableD::GetIdentifier(?);
      Set tags = variableD::GetTags(?);
      Text absoluteIdentifier = variableD::GetAbsoluteIdentifier(?);
      Set If(details == "no", {
        [[identifier, tags, absoluteIdentifier]]
	  }, {
        Text name = variableD::GetName(?);
        Text description = variableD::GetDescription(?);
        Text expression = variableD::GetExpression(?);
        Text type = variableD::GetType(?);
        [[identifier, name, description, type, expression, tags, 
          absoluteIdentifier]]
	  })
    })
  };   

  ////////////////////////////////////////////////////////////////////////////
  Text Create(Set info, Set tags, Set dependences,
              Set attributes, Text containerid)
  ////////////////////////////////////////////////////////////////////////////
  {
    @DataSet dataSet = Eval(containerid);
    Set s_attributes = For(1, Card(attributes), Anything (Real i) {
      Anything PutLocalName(attributes[i][1], attributes[i][2])
    });
    NameBlock args = [[
      Text _.name          = info["name"];    
      Text _.description   = info["description"];
      Set _.attributes     = s_attributes;
	  Set  _.tags          = tags;
      Text _.expression    = info["expression"];
      Set _.dependences    = dependences
    ]];
    Real If(info["type"]!="", {
      Text _.type = info["type"];
      NameBlock args := SetToNameBlock(
        NameBlockToSet(args) << [[_.type]]
      );
      Real 1
    });
    @Variable variableD = dataSet::CreateVariable(args);
    variableD::GetAbsoluteIdentifier(?)
  };

  ////////////////////////////////////////////////////////////////////////////
  Text Edit(Set info, Set tags, Set dependences,
            Set attributes, Text containerid)
  ////////////////////////////////////////////////////////////////////////////
  {
    @DataSet dataSet = Eval(containerid);
    @VariableD variableD = dataSet::GetVariable(info["identifier"]);
    Real variableD::SetName(info["name"]);
    Real variableD::SetDescription(info["description"]);
    Real variableD::RemoveAttributes(?);
    Set For(1, Card(attributes), Real (Real i) {
      variableD::SetAttribute(attributes[i][1], attributes[i][2]);
      Real 1
    });
    Real variableD::SetTags(tags);
    Real If(info["type"]!="", {
      Real variableD::SetType(info["type"])
    });
    Real variableD::SetExpression(info["expression"]);
    Real variableD::SetDependences(dependences);
    variableD::GetAbsoluteIdentifier(?)
  };

  ////////////////////////////////////////////////////////////////////////////
  Set GetType.Possibilities(Real void)
  ////////////////////////////////////////////////////////////////////////////
  { @VData::GetType.Possibilities(?) }
]];
