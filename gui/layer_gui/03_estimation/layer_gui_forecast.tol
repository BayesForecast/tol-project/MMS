//////////////////////////////////////////////////////////////////////////////
// FILE:    layer_gui_forecast.tol
// PURPOSE: Definition of the communication layer between MMS and the GUI for
//          Forecasts
//////////////////////////////////////////////////////////////////////////////

@Module ForecastsGui = [[

Real _void = 0;

#Embed "layer_gui_modelforecast.tol";
#Embed "layer_gui_submodelforecast.tol";
#Embed "layer_gui_hierarchyforecast.tol";
#Embed "layer_gui_parameterforecast.tol";
#Embed "layer_gui_finterval.tol";
#Embed "layer_gui_fscenario.tol";

/*
  # Data of the Forecast to edit
   # (Class @Forecast)            - (: @MainObject)
    # forecast_info(name)         - Name (Text _.name)
    # forecast_info(vers)         - Version (Text _.version)
    # forecast_info(dCre)         - Creation date (Date _.creationTime)
    # forecast_info(dMod)         - (Date _.modificationTime)
    # forecast_info(dNow)         - Current system date
	# forecast_info(saved)        - Is saved? (Real _.isSaved)
	# forecast_info(source)       - (Text _.source)
    #                             - Copy of the Model (Set _.model)(@Model)
	# forecast_info(model)        - Model Identifier (Only for New)
	# forecast_info(estim)        - Estimation Identifier (Only for New)
    #                             - Estimated Parameters (Set _.parameters)(SetOf{Real|RandVar::@Real.Random})
    #                             - Submodel Intervals (Set _.fIntervals)(SetOf{@FInterval})
    #                             - Variable Scenarios (Set _.fScenarios)(SetOf{@FScenario})
    #                             - (Set _.settings)(@SettingsForecast)
	# forecast_info(mode)         - Mode of Settings (Point, Sample)
    # forecast_info(desc)         - Description (H: Text _.description)
    # forecast_info(attr)         - Attributes (H: Set _.attributes)
    # forecast_info(tags)         - Tags (H: Set _.tags)
    # forecast_info(st_tags)      - Tags as text
    #                             - (Set _.forecast)(~ results)
*/

////////////////////////////////////////////////////////////////////////////
Set GetInfoForecast(@Forecast forecast)
////////////////////////////////////////////////////////////////////////////
{
  Text name = forecast::GetName(?);
  Text vers = forecast::GetVersion(?);
  Text desc = forecast::GetDescription(?);
  Date datC = forecast::GetCreationTime(?);
  Text dCre = FormatDate(datC, "%Y/%m/%d");
  Date datM = forecast::GetModificationTime(?);
  Text dMod = FormatDate(datM, "%Y/%m/%d");
  Text source = forecast::GetSource(?);
  Real saved = forecast::IsSaved(?);
  Text abs_id = forecast::GetAbsoluteIdentifier(?);
  
  Text mode = forecast::GetMode(?);
  
  Set attr = EvalSet(forecast::GetAttributes(?), Set (Anything attribute) {
    [[Name(attribute), attribute]]
  }); 

  Set SetOfAnything(name, vers, desc, dCre, dMod, source, saved,
                    mode, attr, abs_id)
};
   
////////////////////////////////////////////////////////////////////////////
Set GetForecast(Text ident)
////////////////////////////////////////////////////////////////////////////
{
  @Forecast forecast = Container::GetForecast(ident);
  Set GetInfoForecast(forecast)
};

////////////////////////////////////////////////////////////////////////////
Set GetForecastsList(Text details)
////////////////////////////////////////////////////////////////////////////
{
  Set setInf = EvalSet(Container::GetForecasts(?), Set (@Forecast forecast) {
    Text ident = forecast::GetIdentifier(?);
    Text abs_id = forecast::GetAbsoluteIdentifier(?);
    Real saved = forecast::IsSaved(?);
    Set If(details == "no", {
      Set SetOfAnything(ident, abs_id, saved)
	}, {
      Text name = forecast::GetName(?);
      Text vers = forecast::GetVersion(?);
      Text desc = forecast::GetDescription(?);
      Date datC = forecast::GetCreationTime(?);
      Text dCre = FormatDate(datC, "%Y/%m/%d");
      Date datM = forecast::GetModificationTime(?);
      Text dMod = FormatDate(datM, "%Y/%m/%d");
      Text source = forecast::GetSource(?);

	  Text mode = forecast::GetMode(?);
  
      Set SetOfAnything(ident, name, vers, desc, dCre, dMod, source, saved,
 	                    mode, abs_id)
	})
  })
};   

////////////////////////////////////////////////////////////////////////////
Text GetForecastHelp(Real void)
////////////////////////////////////////////////////////////////////////////
{
  Text help = If(ObjectExist("Text", "MMS::@Forecast::_.autodoc.help"),
    MMS::@Forecast::_.autodoc.help, ""
  )
};

////////////////////////////////////////////////////////////////////////////
Set GetForecastModel(Text ident)
////////////////////////////////////////////////////////////////////////////
{
  @Forecast forecast = Container::GetForecast(ident);
  @Model mod = forecast::GetModel(?);
  Text ident = mod::GetIdentifier(?);
  Text abs_id = mod::GetAbsoluteIdentifier(?);
  Set SetOfAnything(ident, abs_id)
};

////////////////////////////////////////////////////////////////////////////
Set GetForecastSettings(Text ident)
////////////////////////////////////////////////////////////////////////////
{
  @Forecast forecast = Container::GetForecast(ident);
  @Settings settings = forecast::GetSettings(?);
  Text subclass = settings::GetSubclass(?);
  Set SetOfAnything(subclass)
};

////////////////////////////////////////////////////////////////////////////
Set GetModelForecast(Text ident)
////////////////////////////////////////////////////////////////////////////
{
  @Forecast forecast = Container::GetForecast(ident);
  @Model.Forecast mod = forecast::GetModel.Forecast(?);
  Text ident = mod::GetIdentifier(?);
  Text abs_id = mod::GetAbsoluteIdentifier(?);
  Set SetOfAnything(ident, abs_id)
};

////////////////////////////////////////////////////////////////////////////
Real HasForecast(Text ident)
////////////////////////////////////////////////////////////////////////////
{
  @Forecast forecast = Container::GetForecast(ident);
  Real forecast::HasForecast(?)
};

////////////////////////////////////////////////////////////////////////////
Real CreateForecastTest(Set info, Set attributes)
////////////////////////////////////////////////////////////////////////////
{
  WriteLn("CreateForecastTest");
  WriteLn("name:"+info["name"]);
  WriteLn("vers:"+info["vers"]);
  WriteLn("desc:"+info["desc"]);
  WriteLn("model:"+info["model"]);
  WriteLn("estimation:"+info["estim"]);
  WriteLn("mode:"+info["mode"]);
  WriteLn("Attributes:");
  Set For(1, Card(attributes), Real (Real i) {
    WriteLn(attributes[i][1]+":"+attributes[i][2]);
    Real 1
  });
  Real 1
};

////////////////////////////////////////////////////////////////////////////
Real CreateForecast(Set info, Set attributes)
////////////////////////////////////////////////////////////////////////////
{
  Set s_attributes = For(1, Card(attributes), Anything (Real i) {
    Anything PutLocalName(attributes[i][1], attributes[i][2])
  });

  NameBlock args0 = [[
    Text _.name         = info["name"];
    Text _.version      = info["vers"];
    Text _.description  = info["desc"];
    Set _.attributes    = s_attributes;
	
	Text _.mode         = info["mode"]
  ]];

  NameBlock args1 = If(info["estim"]!="", {
    @Estimation _.estimation = Container::GetEstimation(info["estim"]);
    NameBlock SetToNameBlock(
      NameBlockToSet(args0) << [[_.estimation]]
    )}, {
	NameBlock args0
  });
  
  NameBlock args = If(info["model"]!="", {
    @Model _.model = Container::GetModel(info["model"]);
    NameBlock SetToNameBlock(
      NameBlockToSet(args1) << [[_.model]]
    )}, {
	NameBlock args1
  });
  
  @Forecast forecast = Container::CreateForecast(args);
  Real 1
};

////////////////////////////////////////////////////////////////////////////
Real CopyForecastTest(Set info)
////////////////////////////////////////////////////////////////////////////
{
  WriteLn("CopyForecastTest");
  WriteLn("ident:"+info["ident"]);
  WriteLn("name:"+info["name"]);
  WriteLn("vers:"+info["vers"]);
  WriteLn("desc:"+info["desc"]);
  Real 1
};

////////////////////////////////////////////////////////////////////////////
Real CopyForecast(Set info)
////////////////////////////////////////////////////////////////////////////
{
  @Forecast prev_fore = Container::GetForecast(info["ident"]);
  // Se crea un nuevo objeto con un identificador v�lido
  @Forecast fore = prev_fore::Duplicate(?);
  // Si estos cambios no son v�lidos se mostrar�n errores
  Real fore::SetName(info["name"]);
  Real fore::SetVersion(info["vers"]);
  Real fore::SetDescription(info["desc"]);
  Real 1
};

////////////////////////////////////////////////////////////////////////////
Real EditForecastTest(Set info, Set attributes)
////////////////////////////////////////////////////////////////////////////
{
  WriteLn("EditForecastTest");
  WriteLn("name:"+info["name"]);
  WriteLn("vers:"+info["vers"]);
  WriteLn("desc:"+info["desc"]);
  WriteLn("Attributes:");
  Set For(1, Card(attributes), Real (Real i) {
    WriteLn(attributes[i][1]+":"+attributes[i][2]);
    Real 1
  });
  Real 1
};

////////////////////////////////////////////////////////////////////////////
Real EditForecast(Set info, Set attributes)
////////////////////////////////////////////////////////////////////////////
{
  @Forecast forecast = Container::GetForecast(info["ident"]);
  Real forecast::SetName(info["name"]);
  Real forecast::SetVersion(info["vers"]);
  Real forecast::SetDescription(info["desc"]);
  
  Real forecast::RemoveAttributes(?);
  Set For(1, Card(attributes), Real (Real i) {
    forecast::SetAttribute(attributes[i][1], attributes[i][2]);
    Real 1
  });
  Real 1
}

]];
